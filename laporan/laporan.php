<?php

$tanggal_laporan=WKT(date("Y-m-d"));
$jam_laporan=date("H:i:s");
$pro="simpan";
$gambar0="avatar.jpg";
$status="Aktif";
//$PATH="ypathcss";
?>
<link type="text/css" href="<?php echo "$PATH/base/";?>ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo "$PATH/";?>jquery-1.3.2.js"></script>
<script type="text/javascript" src="<?php echo "$PATH/";?>ui/ui.core.js"></script>
<script type="text/javascript" src="<?php echo "$PATH/";?>ui/ui.datepicker.js"></script>
<script type="text/javascript" src="<?php echo "$PATH/";?>ui/i18n/ui.datepicker-id.js"></script>

  <script type="text/javascript">
      $(document).ready(function(){
        $("#tanggal_laporan").datepicker({
					dateFormat  : "dd MM yy",
          changeMonth : true,
          changeYear  : true
        });
      });
    </script>

<script type="text/javascript">
function PRINT(){
win=window.open('laporan/print.php','win','width=1000, height=400, menubar=0, scrollbars=1, resizable=0, location=0, toolbar=0, status=0'); }
</script>
<script language="JavaScript">
function buka(url) {window.open(url, 'window_baru', 'width=800,height=600,left=320,top=100,resizable=1,scrollbars=1');}
</script>

<?php
  $sql="select `id_laporan` from `$tblaporan` order by `id_laporan` desc";
  $q=mysqli_query($conn, $sql);
  $jum=mysqli_num_rows($q);
  $th=date("y");
  $bl=date("m")+0;if($bl<10){$bl="0".$bl;}

  $kd="LAP".$th.$bl;//KEG1610001
  if($jum > 0){
   $d=mysqli_fetch_array($q);
   $idmax=$d["id_laporan"];

   $bul=substr($idmax,5,2);
   $tah=substr($idmax,3,2);
    if($bul==$bl && $tah==$th){
     $urut=substr($idmax,7,3)+1;
     if($urut<10){$idmax="$kd"."00".$urut;}
     else if($urut<100){$idmax="$kd"."0".$urut;}
     else{$idmax="$kd".$urut;}
    }//==
    else{
     $idmax="$kd"."001";
     }
   }//jum>0
  else{$idmax="$kd"."001";}
  $id_laporan=$idmax;
?>
<?php
if($_GET["pro"]=="ubah"){
	$id_laporan=$_GET["kode"];
	$sql="select * from `$tblaporan` where `id_laporan`='$id_laporan'";
	$d=getField($conn,$sql);
				$id_laporan=$d["id_laporan"];
				$id_workorder=$d["id_workorder"];
                $id_pelanggan=$d["id_pelanggan"];
				$lokasi_tagging=$d["lokasi_tagging"];
				$tarif=$d["tarif"];
        $daya=$d["daya"];
        $cek_pelanggaran=$d["cek_pelanggaran"];
        $status=$d["kode_pelanggaran"];
        $jam_laporan=$d["jam_laporan"];
        $tanggal_laporan=WKT($d["tanggal_laporan"]);
				$status=$d["status"];
				$gambar=$d["gambar"];
				$gambar0=$d["gambar"];
				$pro="ubah";
}
?>

<form action="" method="post" enctype="multipart/form-data">
<table width="40%" >
<tr>
<th width="66"><label for="id_laporan">ID Laporan</label>
<th width="9">:
<th colspan="2"><b><?php echo $id_laporan;?></b></tr>

<tr>
<td><label for="id_workorder">Work Order</label>
<td>:<td width="213"><select name="id_workorder" id="id_workorder">
<?php
$sql="select id_workorder from `$tbworkorder`";// where `level`='AP2T'";
$arr=getData($conn,$sql);
	foreach($arr as $d) {
      $id_workorder0=$d["id_workorder"];
      echo"<option value='$id_workorder0' ";if($id_workorder0==$id_workorder){echo"selected";}echo">$id_workorder0</option>";
    }
 ?>
</select></td>

<td width="81" rowspan="4">
<center>
<?php
echo"<a href='#' onclick='buka(\"laporan/zoom.php?id=$id_laporan\")'>
<img src='$YPATH/$gambar0' width='77' height='80' />
</a>
";
?>
</center>
</td>
</tr>

<tr>
<td height="24"><label for="id_pelanggan">Nama Pelanggan</label>
<td>:<td colspan="2"><select name="id_pelanggan" id="id_pelanggan">
<?php
$sql="select id_pelanggan,nama from `$tbpelanggan`";// where `level`='AP2T'";
$arr=getData($conn,$sql);
	foreach($arr as $d) {
      $id_pelanggan0=$d["id_pelanggan"];
      $nama=$d["nama"];
      echo"<option value='$id_pelanggan0' ";if($id_pelanggan0==$id_pelanggan){echo"selected";}echo">$nama</option>";
    }
 ?>
</select></td>
</tr>

<tr>
<td height="24"><label for="lokasi_tagging">Lokasi</label>
<td>:<td><input name="lokasi_tagging" type="text" id="lokasi_tagging" value="<?php echo $lokasi_tagging;?>" size="15" />
</td>
</tr>

<tr>
<td height="24"><label for="tarif">Tarif</label>
<td>:<td><input name="tarif" type="text" id="tarif" value="<?php echo $tarif;?>" size="25" />
</td>
</tr>

<tr>
<td height="24"><label for="daya">Daya</label>
<td>:<td><input name="daya" type="text" id="daya" value="<?php echo $daya;?>" size="25" />
</td>
</tr>

<tr>
<td><label for="cek_pelanggaran">Cek Pelanggaran</label>
<td>:<td colspan="2">
<input type="radio" name="cek_pelanggaran" id="cek_pelanggaran"  checked="checked" value="Ada" <?php if($cek_pelanggaran=="Ada"){echo"checked";}?>/>Ada
<input type="radio" name="cek_pelanggaran" id="cek_pelanggaran" value="Tidak Ada" <?php if($cek_pelanggaran=="Tidak Ada"){echo"checked";}?>/>Tidak Ada
</td></tr>

<tr>
<td height="24"><label for="kode_pelanggaran">Kode Pelanggaran</label>
<td>:<td><input name="kode_pelanggaran" type="text" id="kode_pelanggaran" value="<?php echo $kode_pelanggaran;?>" size="25" />
</td>
</tr>

<tr>
<td height="24"><label for="jam_laporan">Jam Laporan</label>
<td>:<td><input name="jam_laporan" type="time" id="jam_laporan" value="<?php echo $jam_laporan;?>" size="25" />
</td>
</tr

<tr>
<td height="24"><label for="tanggal_laporan">Tanggal Laporan</label>
<td>:<td><input name="tanggal_laporan" type="text" id="tanggal_laporan" value="<?php echo $tanggal_laporan;?>" size="25" />
</td>
</tr>

<tr>
<td><label for="status">Status</label>
<td>:<td colspan="2">
<input type="radio" name="status" id="status"  checked="checked" value="Aktif" <?php if($status=="Aktif"){echo"checked";}?>/>Aktif
<input type="radio" name="status" id="status" value="Tidak Aktif" <?php if($status=="Tidak Aktif"){echo"checked";}?>/>Tidak Aktif
</td></tr>

<tr>
  <td height="24">gambar
    <td>:<td colspan="2"><label for="gambar"></label>
        <input name="gambar" type="file" id="gambar" size="20" />
      => <a href='#' onclick='buka("laporan/zoom.php?id=<?php echo $id_laporan;?>")'><?php echo $gambar0;?></a></td>
</tr>

<tr>
<td>
<td>
<td colspan="2"><input name="Simpan" type="submit" id="Simpan" value="Simpan" />
        <input name="pro" type="hidden" id="pro" value="<?php echo $pro;?>" />
        <input name="gambar0" type="hidden" id="gambar0" value="<?php echo $gambar0;?>" />
        <input name="id_laporan" type="hidden" id="id_laporan" value="<?php echo $id_laporan;?>" />
        <input name="id_laporan0" type="hidden" id="id_laporan0" value="<?php echo $id_laporan0;?>" />
        <a href="?mnu=laporan"><input name="Batal" type="button" id="Batal" value="Batal" /></a>
</td></tr>
</table>
</form>
<br />
Data laporan:
| <a href="laporan/pdf.php"><img src='ypathicon/pdf.png' alt='PDF'></a>
| <a href="laporan/xml.php"><img src='ypathicon/xml.png' alt='XML'></a>
| <a href="laporan/xls.php"><img src='ypathicon/xls.png' alt='XLS'></a>
| <img src='ypathicon/print.png' alt='PRINT' OnClick="PRINT()"> |
<br>

<table width="100%" border="0">
  <tr bgcolor="#036">
    <th width="3%">No</td>
    <th width="10%">id_laporan</td>
    <th width="20%">id_workorder</td>
        <th width="20%">id_pelanggan</td>
    <th width="30%">tarif</td>
    <th width="20%">lokasi_tagging</td>
      <th width="30%">daya</td>
        <th width="30%">cek_pelanggaran</td>
            <th width="30%">kode_pelanggaran</td>
                <th width="30%">jam_laporan</td>
                    <th width="30%">tanggal_laporan</td>
          <th width="10%">status</td>
    <th width="10%">gambar</td>
    <th width="15%">Aksi</td>
  </tr>
<?php
  $sql="select * from `$tblaporan` order by `id_laporan` desc";
  $jum=getJum($conn,$sql);
		if($jum > 0){
//--------------------------------------------------------------------------------------------
$batas   = 10;
$page = $_GET['page'];
if(empty($page)){$posawal  = 0;$page = 1;}
else{$posawal = ($page-1) * $batas;}

$sql2 = $sql." LIMIT $posawal,$batas";
$no = $posawal+1;
//--------------------------------------------------------------------------------------------
	$arr=getData($conn,$sql2);
		foreach($arr as $d) {
				$id_laporan=$d["id_laporan"];
				$id_workorder=$d["id_workorder"];
                $id_pelanggan=$d["id_pelanggan"];
				$lokasi_tagging=$d["lokasi_tagging"];
				$tarif=$d["tarif"];
        $daya=$d["daya"];
        $cek_pelanggaran=$d["cek_pelanggaran"];
        $kode_pelanggaran=$d["kode_pelanggaran"];
        $jam_laporan=$d["jam_laporan"];
        $tanggal_laporan=WKT($d["tanggal_laporan"]);
				$status=$d["status"];
				$gambar=$d["gambar"];
				$gambar0=$d["gambar"];
				$color="#dddddd";
					if($no %2==0){$color="#eeeeee";}
echo"<tr bgcolor='$color'>
				<td>$no</td>
				<td>$id_laporan</td>
				<td>$id_workorder</td>
                    <td>$id_pelanggan</td>
				<td>$tarif</td>
				<td>$lokasi_tagging</td>
        <td>$daya</td>
        <td>$cek_pelanggaran</td>
            <td>$kode_pelanggaran</td>
                <td>$jam_laporan</td>
                    <td>$tanggal_laporan</td>
        <td>$status</td>
				<td><div align='center'>";
echo"<a href='#' onclick='buka(\"laporan/zoom.php?id=$id_laporan\")'>
<img src='$YPATH/$gambar' width='40' height='40' /></a></div>";
				echo"</td>
				<td><div align='center'>
<a href='?mnu=laporan&pro=ubah&kode=$id_laporan'><img src='ypathicon/u.png' alt='ubah'></a>
<a href='?mnu=laporan&pro=hapus&kode=$id_laporan'><img src='ypathicon/h.png' alt='hapus'
onClick='return confirm(\"Apakah Anda benar-benar akan menghapus $nama pada data laporan ?..\")'></a></div></td>
				</tr>";

			$no++;
			}//while
		}//if
		else{echo"<tr><td colspan='6'><blink>Maaf, Data laporan belum tersedia...</blink></td></tr>";}
?>
</table>

<?php
$jmldata = $jum;
if($jmldata>0){
	if($batas<1){$batas=1;}
	$jmlhal  = ceil($jmldata/$batas);
	echo "<div class=paging>";
	if($page > 1){
		$prev=$page-1;
		echo "<span class=prevnext><a href='$_SERVER[PHP_SELF]?page=$prev&mnu=laporan'>« Prev</a></span> ";
	}
	else{echo "<span class=disabled>« Prev</span> ";}

	for($i=1;$i<=$jmlhal;$i++)
	if ($i != $page){echo "<a href='$_SERVER[PHP_SELF]?page=$i&mnu=laporan'>$i</a> ";}
	else{echo " <span class=current>$i</span> ";}

	if($page < $jmlhal){
		$next=$page+1;
		echo "<span class=prevnext><a href='$_SERVER[PHP_SELF]?page=$next&mnu=laporan'>Next »</a></span>";
	}
	else{ echo "<span class=disabled>Next »</span>";}
	echo "</div>";
	}//if jmldata

$jmldata = $jum;
echo "<p align=center>Total data <b>$jmldata</b> item</p>";
?>
<?php
if(isset($_POST["Simpan"])){
	$pro=strip_tags($_POST["pro"]);
	$id_laporan=strip_tags($_POST["id_laporan"]);
	$id_laporan0=strip_tags($_POST["id_laporan"]);
	$id_workorder=strip_tags($_POST["id_workorder"]);
    $id_pelanggan=strip_tags($_POST["id_pelanggan"]);
	$lokasi_tagging=strip_tags($_POST["lokasi_tagging"]);
	$tarif=strip_tags($_POST["tarif"]);
  $daya=strip_tags($_POST["daya"]);
  $cek_pelanggaran=strip_tags($_POST["cek_pelanggaran"]);
  $kode_pelanggaran=strip_tags($_POST["kode_pelanggaran"]);
  $jam_laporan=strip_tags($_POST["jam_laporan"]);
  $tanggal_laporan=BAL(strip_tags($_POST["tanggal_laporan"]));
	$status=strip_tags($_POST["status"]);

	$gambar0=strip_tags($_POST["gambar0"]);
	if ($_FILES["gambar"] != "") {
		@copy($_FILES["gambar"]["tmp_name"],"$YPATH/".$_FILES["gambar"]["name"]);
		$gambar=$_FILES["gambar"]["name"];
		}
	else {$gambar=$gambar0;}
	if(strlen($gambar)<1){$gambar=$gambar0;}

if($pro=="simpan"){
$sql=" INSERT INTO `$tblaporan` (
`id_laporan` ,
`id_workorder` ,
`id_pelanggan` ,
`lokasi_tagging` ,
`tarif` ,
`daya` ,
`cek_pelanggaran` ,
`kode_pelanggaran` ,
`jam_laporan` ,
`tanggal_laporan` ,
`status` ,
`gambar`
) VALUES (
'$id_laporan',
'$id_workorder',
    '$id_pelanggan',
'$lokasi_tagging',
'$tarif',
'$daya',
'$cek_pelanggaran',
'kode_pelanggaran' ,
'jam_laporan' ,
'tanggal_laporan' ,
'$status',
'$gambar'
)";

$simpan=process($conn,$sql);
	if($simpan) {echo "<script>alert('Data $id_laporan berhasil disimpan !');document.location.href='?mnu=laporan';</script>";}
		else{echo"<script>alert('Data $id_laporan gagal disimpan...');document.location.href='?mnu=laporan';</script>";}
	}
	else{
	$sql="update `$tblaporan` set `id_workorder`='$id_workorder',`gambar`='$gambar',`lokasi_tagging`='$lokasi_tagging' ,`tarif`='$tarif', `daya`='$daya',`cek_pelanggaran`='$cek_pelanggaran',`status`='$status',
	`gambar`='$gambar'  where `id_laporan`='$id_laporan0'";
	$ubah=process($conn,$sql);
		if($ubah) {echo "<script>alert('Data $id_laporan berhasil diubah !');document.location.href='?mnu=laporan';</script>";}
		else{echo"<script>alert('Data $id_laporan gagal diubah...');document.location.href='?mnu=laporan';</script>";}
	}//else simpan
}
?>

<?php
if($_GET["pro"]=="hapus"){
$id_laporan=$_GET["kode"];
$sql="delete from `$tblaporan` where `id_laporan`='$id_laporan'";
$hapus=process($conn,$sql);
	if($hapus) {echo "<script>alert('Data $id_laporan berhasil dihapus !');document.location.href='?mnu=laporan';</script>";}
	else{echo"<script>alert('Data $id_laporan gagal dihapus...');document.location.href='?mnu=laporan';</script>";}
}
?>
