<?php
header("Content-type: text/xml");

include "../konmysqli.php";
$sql = "select * from `$tblaporan`";
if(getJum($conn,$sql)>0){
	print "<laporan>\n";
		$arr=getData($conn,$sql);
		foreach($arr as $d) {
				$id_laporan=$d["id_laporan"];
				$id_workorder=$d["id_workorder"];
                $id_pelanggan=$d["id_pelanggan"];
				$lokasi_tagging=$d["lokasi_tagging"];
				$tarif=$d["tarif"];
				$daya=$d["daya"];
				$cek_pelanggaran=$d["cek_pelanggaran"];
				$status=$d["status"];
				$gambar=$d["gambar"];

				print "<record>\n";
				print "  <id_workorder>$id_workorder</id_workorder>\n";
                print "  <id_pelanggan>$id_pelanggan</id_pelanggan>\n";
				print "  <lokasi_tagging>$lokasi_tagging</lokasi_tagging>\n";
				print "  <tarif>$tarif</tarif>\n";
				print "  <daya>$daya</daya>\n";
				print "  <cek_pelanggaran>$cek_pelanggaran</cek_pelanggaran>\n";
				print "  <status>$status</status>\n";
				print "  <gambar>$gambar</gambar>\n";
				print "  <id_laporan>$id_laporan</id_laporan>\n";
				print "</record>\n";
			}
	print "</laporan>\n";
}
else{
	$null="null";
	print "<laporan>\n";
				print "<record>\n";
				print "  <id_workorder>$null</id_workorder>\n";
                print "  <id_pelanggan>$null</id_pelanggan>\n";
				print "  <lokasi_tagging>$null</lokasi_tagging>\n";
				print "  <tarif>$null</tarif>\n";
				print "  <daya>$null</daya>\n";
				print "  <cek_pelanggaran>$null</cek_pelanggaran>\n";
				print "  <status>$null</status>\n";
				print "  <gambar>$null</gambar>\n";
				print "  <id_laporan>$null</id_laporan>\n";
				print "</record>\n";
	print "</laporan>\n";

}

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

function getJum($conn,$sql){
  $rs=$conn->query($sql);
  $jum= $rs->num_rows;
	$rs->free();
	return $jum;
}

function getData($conn,$sql){
	$rs=$conn->query($sql);
	$rs->data_seek(0);
	$arr = $rs->fetch_all(MYSQLI_ASSOC);

	$rs->free();
	return $arr;
}
?>
