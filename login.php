<?php
session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>AdminLTE 2 | Log in</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="/dist/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="/plugins/iCheck/square/blue.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition login-page">

        <div class="login-box">
            <div class="login-logo">
                <a href="index.php"><b>Admin</b>P2TL</a>
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg">@SandySipayung</p>

                <form name="formLogin" method="post" action="">
                    <div class="form-group has-feedback">
                        <marquee>
                            Silakan Anda Login !
                        </marquee>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Username" name="user" id="user">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="password" name="pass" id="pass" class="form-control" placeholder="Password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>

                    <div class="form-group has-feedback" align="right">
                        <button type="Reset" name="Reset"id="Reset" class="btn btn-danger">Reset</button>
                        <button type="submit" name="Login"id="Login" class="btn btn-primary">Login</button>
<!--                                <input type="submit" name="Login" id="Login" value="Login"> -->
<!--                                <input type="Reset" name="Reset" id="Reset" value="Reset">-->
                    </div>
                </form>
                <?php
                if (isset($_POST["Login"])) {
                    $usr = $_POST["user"];
                    $pas = $_POST["pass"];

            $sql1 = "select * from `$tbadmin` where `username`='$usr' and `password`='$pas' and `status`='Aktif'";
            $sql2="select * from `$tbmasterpetugas` where `id_user`='$usr' and `password`='$pas' and `level_user`='PIC'";
            $sql3="select * from `$tbmasterpetugas` where `id_user`='$usr' and `password`='$pas' and `level_user`='PL'";

                    if (getJum($conn, $sql1) > 0) {
                        $d = getField($conn, $sql1);
                        $kode = $d["kode_admin"];
                        $nama = $d["username"];
                        $gambara = $d["gambar"];
                        $_SESSION["cid"] = $kode;
                        $_SESSION["cnama"] = $nama;
                        $_SESSION["cgambara"] = $gambara;
                        $_SESSION["clevel"] = "Administrator";
                        echo "<script>alert('Otentikasi " . $_SESSION["cnama"] . " " . $_SESSION["cnama"] . " (" . $_SESSION["cid"] . ") berhasil Login!');
		document.location.href='index.php?mnu=home';</script>";
                    }
                    elseif(getJum($conn,$sql2)>0){
 					$da = getField($conn, $sql2);
                        $kode = $da["id_user"];
                        $nama = $da["nama_user"];
                        $id_vendor = $da["id_vendor"];
                        $gambara = $da["gambar"];
                        $_SESSION["cid"] = $kode;
                        $_SESSION["cnama"] = $nama;
                        $_SESSION["cvendor"] = $id_vendor;
                        $_SESSION["cgambara"] = $gambara;
                        $_SESSION["clevel"] = "PIC";
                        echo "<script>alert('Otentikasi " . $_SESSION["cnama"] . " " . $_SESSION["cnama"] . " (" . $_SESSION["cid"] . ") berhasil Login!');
		document.location.href='index.php?mnu=home';</script>";
                    	}
                    	elseif(getJum($conn,$sql3)>0){
 					$daa = getField($conn, $sql3);
                        $kode = $daa["id_user"];
                        $nama = $daa["nama_user"];
                        $id_vendor = $daa["id_vendor"];
                        $gambara = $daa["gambar"];
                        $_SESSION["cid"] = $kode;
                        $_SESSION["cnama"] = $nama;
                        $_SESSION["cvendor"] = $id_vendor;
                        $_SESSION["cgambara"] = $gambara;
                        $_SESSION["clevel"] = "PL";
                        echo "<script>alert('Otentikasi " . $_SESSION["cnama"] . " " . $_SESSION["cnama"] . " (" . $_SESSION["cid"] . ") berhasil Login!');
		document.location.href='index.php?mnu=home';</script>";
                    	}
                    else {
                        session_destroy();
                        echo "<script>alert('Otentikasi Login GAGAL !,Silakan cek data Anda kembali...');
			document.location.href='index.php?mnu=login';</script>";
                    }
                }
                ?>

            </div>
            <!-- /.login-box-body -->
        </div>
        <!-- /.login-box -->

        <!-- jQuery 2.2.3 -->
        <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <!-- iCheck -->
        <script src="plugins/iCheck/icheck.min.js"></script>
        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>
    </body>
</html>
