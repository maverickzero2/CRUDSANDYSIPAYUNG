<?php
session_start();

// $_SESSION["clevel"]="PL";
// $_SESSION["cid"]="USR1705001"; //USR1705001

if (version_compare(phpversion(), "5.3.0", ">=") == 1)
    error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
else
    error_reporting(E_ALL & ~E_NOTICE);
?>
<?php
//error_reporting(0);
require_once"konmysqli.php";

$mnu = $_GET["mnu"];
date_default_timezone_set("Asia/Jakarta");
?>
<!DOCTYPE html>
<html>
    <head>
      <link href='img/sandy.jpg' rel='shortcut icon'>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Administrator P2TL</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
       <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
<!-- bootstrap datepicker -->
  <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
  <!-- bootstrap timepicker -->
    <link rel="stylesheet" href="plugins/timepicker/bootstrap-timepicker.css">
    <!-- bootstrap datetimepicker -->
      <link rel="stylesheet" href="bootstrap-datetimepicker-master/css/bootstrap-datetimepicker.min.css">
  <!-- bootstrap icheck -->
  <link rel="stylesheet" href="plugins/iCheck/all.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="plugins/select2/select2.min.css">
  <!-- SWEETALERT -->
  <link rel="stylesheet" href="bootstrap/css/sweetalert.css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="index.php" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><img src="img/logop2tl.png" width="100%" height="100%" align="middle"></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><img src="img/logop2tl.png" width="25%" height="17%" align="middle"><b>Admin</b>P2TL</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- Messages: style can be found in dropdown.less-->
                            <li class="dropdown messages-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-envelope-o"></i>
                                    <span class="label label-success">4</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header">You have 4 messages</li>
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu">
                                            <li><!-- start message -->
                                                <a href="#">
                                                    <div class="pull-left">
                                                        <img src="img/sandy.jpg" class="img-circle" alt="User Image">
                                                    </div>
                                                    <h4>
                                                        Support Team
                                                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                                    </h4>
                                                    <p>Why not buy a new awesome theme?</p>
                                                </a>
                                            </li>
                                            <!-- end message -->
                                        </ul>
                                    </li>
                                    <li class="footer"><a href="#">See All Messages</a></li>
                                </ul>
                            </li>
                            <!-- Notifications: style can be found in dropdown.less -->
                            <li class="dropdown notifications-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-bell-o"></i>
                                    <span class="label label-warning">10</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header">You have 10 notifications</li>
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu">
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="footer"><a href="#">View all</a></li>
                                </ul>
                            </li>
                            <!-- Tasks: style can be found in dropdown.less -->
                            <li class="dropdown tasks-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-flag-o"></i>
                                    <span class="label label-danger">9</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header">You have 9 tasks</li>
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu">
                                            <li><!-- Task item -->
                                                <a href="#">
                                                    <h3>
                                                        Design some buttons
                                                        <small class="pull-right">20%</small>
                                                    </h3>
                                                    <div class="progress xs">
                                                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">20% Complete</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <!-- end task item -->
                                        </ul>
                                    </li>
                                    <li class="footer">
                                        <a href="#">View all tasks</a>
                                    </li>
                                </ul>
                            </li>
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="img/sandy.jpg" class="user-image" alt="User Image">
                                    <span class="hidden-xs"><?php echo $_SESSION["cnama"]?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="img/sandy.jpg" class="img-circle" alt="User Image">

                                        <p>
                                            <?php echo $_SESSION["cnama"]?> - Web & Android Developer
                                            <small>Since Aug. 2013</small>
                                        </p>
                                    </li>
                                    <!-- Menu Body -->
                                    <!-- <li class="user-body">
                                        <div class="row">
                                            <div class="col-xs-4 text-center">
                                                <a href="#">Followers</a>
                                                <a href="#">999K</a>
                                            </div>
                                            <div class="col-xs-4 text-center">
                                                <a href="#">Sales</a>
                                                <a href="#">Followers</a>
                                            </div>
                                            <div class="col-xs-4 text-center">
                                                <a href="#">Friends</a>
                                            </div>
                                        </div> -->
                                        <!-- /.row -->
                                    <!-- </li> -->
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="index.php?mnu=profil" class="btn btn-default btn-flat">Profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href='index.php?mnu=logout' class="btn btn-default btn-flat">Logout</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                            <!-- <li>
                                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                            </li> -->
                        </ul>
                    </div>
                </nav>
            </header>

            <!-- =============================================== -->

            <!-- Left side column. contains the sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="img/sandy.jpg" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p><?php echo $_SESSION['cnama'] ?></p>
                            <a href="#"><i class="fa fa-circle text-success"></i> <?php echo $_SESSION['clevel'] ?> <?php echo getVendor($conn,$_SESSION["cvendor"])?></a>
                        </div>
                    </div>
                    <!-- search form -->
                    <!-- <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form> -->
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="header">NAVIGASI UTAMA</li>

  <?php
  if ($_SESSION["clevel"]=="Administrator") {
  echo"
	    <li><a href='index.php?mnu=home'><i class='fa fa-dashboard'></i> <span>Dashboard</span></a></li>
      <li><a href='index.php?mnu=admin'><i class='fa fa-user'></i><span>Administrator</span></a></li>
      <li><a href='index.php?mnu=mastervendor'> <i class='fa fa-university'></i><span>Master Vendor</span></a></li>
      <li><a href='index.php?mnu=petugas'><i class='fa fa-users'></i><span>Master Petugas Lapangan</span></a></li>
      <li><a href='index.php?mnu=pelanggan'><i class='fa fa-user-secret'></i><span>Master Pelanggan</span></a></li>
      <li><a href='index.php?mnu=masterstatus'><i class='fa fa-mouse-pointer'></i><span>Master Status Pelaksanaan</span></a></li>
      <li><a href='index.php?mnu=transdata_p2tl'><i class='fa fa-paper-plane'></i><span>Transaksi Data P2TL</span></a></li>
      <li><a href='index.php?mnu=targetoperasi'><i class='fa fa-paper-plane'></i><span>Target Operasi</span></a></li>
      <li><a href='index.php?mnu=logout'><i class='fa fa-close'></i><span>Logout</span></a></li>";
      }
else   if ($_SESSION["clevel"]=="PIC") {
  echo"
	    <li><a href='index.php?mnu=home'><i class='fa fa-dashboard'></i> <span>Dashboard</span></a></li>
      <li><a href='index.php?mnu=profil'><i class='fa fa-user'></i><span>Profil</span></a></li>
      <li><a href='index.php?mnu=ppetugas'><i class='fa fa-users'></i><span>Petugas Lapangan</span></a></li>
      <li><a href='index.php?mnu=ppelanggan'><i class='fa fa-user-secret'></i><span>Pelanggan</span></a></li>
      <!-- <li><a href='index.php?mnu=pstatus'><i class='fa fa-mouse-pointer'></i><span>Status Pelaksanaan</span></a></li> -->
      <li><a href='index.php?mnu=ptransdata_p2tlwo'><i class='fa fa-paper-plane'></i><span>Menu Work Order</span></a></li>
      <li><a href='index.php?mnu=ptransdata_p2tlfinal'><i class='fa fa-paper-plane'></i><span>Menu Laporan</span></a></li>
      <li><a href='index.php?mnu=logout'><i class='fa fa-close'></i><span>Logout</span></a></li>";
      }
      else   if ($_SESSION["clevel"]=="PL") {
        echo"
      	    <li><a href='index.php?mnu=home'><i class='fa fa-dashboard'></i> <span>Dashboard</span></a></li>
            <li><a href='index.php?mnu=profil'><i class='fa fa-user'></i><span>Profil</span></a></li>
            <li><a href='index.php?mnu=ppelanggan'><i class='fa fa-user-secret'></i><span>Pelanggan</span></a></li>
            <li><a href='index.php?mnu=pltransdata_p2tl'><i class='fa fa-paper-plane'></i><span>Daftar Work Order</span></a></li>
            <li><a href='index.php?mnu=pllaporan'><i class='fa fa-mouse-pointer'></i><span>Laporan</span></a></li>
            <li><a href='index.php?mnu=logout'><i class='fa fa-close'></i><span>Logout</span></a></li>";
            }
else {
                          ?>
                          <li class="active treeview"><a href='index.php?mnu=home'><i class='fa fa-dashboard'></i><span>Dashboard</span></a></li>
                          <li class="treeview"><a href='index.php?mnu=login'><i class='fa fa-tv'></i><span>Login</span></a></li>
                          <?php
                        }
                        ?>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- =============================================== -->

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <!-- <section class="content-header">
                    <h1>
                        Blank page
                        <small>it all starts here</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#">Examples</a></li>
                        <li class="active">Blank page</li>
                    </ol>
                </section> -->

                <!-- Main content -->
                <section class="content">

                    <!-- Default box -->
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tampilan Menu <?php echo "$mnu"?></h3>

                            <!-- <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i class="fa fa-times"></i></button>
                            </div> -->
                        </div>
                        <div class="box-body">

<?php
if ($mnu == "admin") {  require_once"admin/admin.php";}
else if ($mnu == "petugas") {require_once"masterpetugas/masterpetugas.php";}
else if ($mnu == "mastervendor") {require_once"mastervendor/mastervendor.php";}
else if ($mnu == "transdata_p2tl") {require_once"transdata_p2tl/transdata_p2tl.php";}
else if ($mnu == "masterstatus") {require_once"masterstatus/masterstatus.php";}
else if ($mnu == "pelanggan") {require_once"pelanggan/pelanggan.php";}

else if ($mnu == "ppetugas") {require_once"masterpetugas/ppetugas.php";}

else if ($mnu == "profil") {require_once"masterpetugas/profil.php";}
else if ($mnu == "profil2") {require_once"masterpetugas/profil2.php";}
else if ($mnu == "pvendor") {require_once"mastervendor/pvendor.php";}
else if ($mnu == "ptransdata_p2tlfinal") {require_once"transdata_p2tl/ptransdata_p2tlfinal.php";}
else if ($mnu == "ptransdata_p2tlwo") {require_once"transdata_p2tl/ptransdata_p2tlwo.php";}
else if ($mnu == "ptransdata_p2tlwo2") {require_once"transdata_p2tl/ptransdata_p2tlwo2.php";}
else if ($mnu == "pltransdata_p2tl") {require_once"transdata_p2tl/pltransdata_p2tl.php";}
else if ($mnu == "pltransdata_p2tlstatus") {require_once"transdata_p2tl/pltransdata_p2tlstatus.php";}
else if ($mnu == "pltransdata_p2tlstatus2") {require_once"transdata_p2tl/pltransdata_p2tlstatus2.php";}
else if ($mnu == "pltransdata_p2tllaporan") {require_once"transdata_p2tl/pltransdata_p2tllaporan.php";}
else if ($mnu == "pllaporan") {require_once"transdata_p2tl/pllaporan.php";}
else if ($mnu == "masterstatus") {require_once"masterstatus/masterstatus.php";}

else if ($mnu == "pelanggan") {require_once"pelanggan/pelanggan.php";}
else if ($mnu == "ppelanggan") {require_once"pelanggan/ppelanggan.php";}

else if ($mnu == "login") {require_once"login.php";}
else if ($mnu == "logout") {require_once"logout.php";}

else if ($mnu == "targetoperasi") {require_once"transdata_p2tl/targetoperasi.php";}

else {require_once"home.php";}


?>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                        </div>
                        <!-- /.box-footer-->
                    </div>
                    <!-- /.box -->
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> V.01
                </div>
                <strong>Copyright &copy; 2017 <a href="http://sandysipayung.wordpress.com">Glory Efrat Sandy Sipayung</a>.</strong> All rights
                reserved.
            </footer>

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Create the tabs -->
                <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                    <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>

                    <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <!-- Home tab content -->
                    <div class="tab-pane" id="control-sidebar-home-tab">
                        <h3 class="control-sidebar-heading">Recent Activity</h3>
                        <ul class="control-sidebar-menu">
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                                        <p>Will be 23 on April 24th</p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="menu-icon fa fa-user bg-yellow"></i>

                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                                        <p>New phone +1(800)555-1234</p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                                        <p>nora@example.com</p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="menu-icon fa fa-file-code-o bg-green"></i>

                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                                        <p>Execution time 5 seconds</p>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <!-- /.control-sidebar-menu -->

                        <h3 class="control-sidebar-heading">Tasks Progress</h3>
                        <ul class="control-sidebar-menu">
                            <li>
                                <a href="javascript:void(0)">
                                    <h4 class="control-sidebar-subheading">
                                        Custom Template Design
                                        <span class="label label-danger pull-right">70%</span>
                                    </h4>

                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <h4 class="control-sidebar-subheading">
                                        Update Resume
                                        <span class="label label-success pull-right">95%</span>
                                    </h4>

                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-success" style="width: 95%"></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <h4 class="control-sidebar-subheading">
                                        Laravel Integration
                                        <span class="label label-warning pull-right">50%</span>
                                    </h4>

                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <h4 class="control-sidebar-subheading">
                                        Back End Framework
                                        <span class="label label-primary pull-right">68%</span>
                                    </h4>

                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <!-- /.control-sidebar-menu -->

                    </div>
                    <!-- /.tab-pane -->
                    <!-- Stats tab content -->
                    <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
                    <!-- /.tab-pane -->
                    <!-- Settings tab content -->
                    <div class="tab-pane" id="control-sidebar-settings-tab">
                        <form method="post">
                            <h3 class="control-sidebar-heading">General Settings</h3>

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Report panel usage
                                    <input type="checkbox" class="pull-right" checked>
                                </label>

                                <p>
                                    Some information about this general settings option
                                </p>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Allow mail redirect
                                    <input type="checkbox" class="pull-right" checked>
                                </label>

                                <p>
                                    Other sets of options are available
                                </p>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Expose author name in posts
                                    <input type="checkbox" class="pull-right" checked>
                                </label>

                                <p>
                                    Allow the user to show his name in blog posts
                                </p>
                            </div>
                            <!-- /.form-group -->

                            <h3 class="control-sidebar-heading">Chat Settings</h3>

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Show me as online
                                    <input type="checkbox" class="pull-right" checked>
                                </label>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Turn off notifications
                                    <input type="checkbox" class="pull-right">
                                </label>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Delete chat history
                                    <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
                                </label>
                            </div>
                            <!-- /.form-group -->
                        </form>
                    </div>
                    <!-- /.tab-pane -->
                </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->

        <!-- jQuery 2.2.3 -->
        <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <!-- SlimScroll -->
        <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="plugins/fastclick/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="dist/js/app.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="dist/js/demo.js"></script>
        <script src="plugins/select2/select2.full.min.js"></script>
        <!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- bootstrap datepicker -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- bootstrap timepicker -->
<script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- bootstrap datetimepicker -->
<script src="bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.js"></script>
<!-- bootstrap icheck -->
<script src="plugins/iCheck/icheck.js"></script>
<!-- bootstrap icheck -->
<script src="bootstrap/js/sweetalert.min.js"></script>

<script>
  $(function () {
      //Initialize Select2 Elements
    $(".select2").select2();

    $("#tabeladmin").DataTable();
    $('#tabeluser').DataTable();
    $('#tabelvendor').DataTable();
    $('#tabelpelanggan').DataTable();
    $('#tabeltransdata_p2tl').DataTable();
    $('#tabelupdatestatus').DataTable();
   /* $('#tabeluser').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });*/
  });

  //Date picker
    $('#tanggal_transdata_p2tl').datepicker({
      autoclose: true
    });
    $('#tanggal_updatestatus').datepicker({
      autoclose: true
    });

    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });
</script>

    </body>
</html>
<?php

function RP($rupiah) {
    return number_format($rupiah, "2", ",", ".");
} ?>
<?php

function WKT($sekarang) {
    $tanggal = substr($sekarang, 8, 2) + 0;
    $bulan = substr($sekarang, 5, 2);
    $tahun = substr($sekarang, 0, 4);

    $judul_bln = array(1 => "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
    $wk = $tanggal . " " . $judul_bln[(int) $bulan] . " " . $tahun;
    return $wk;
}
?>
<?php

function WKTP($sekarang) {
    $tanggal = substr($sekarang, 8, 2) + 0;
    $bulan = substr($sekarang, 5, 2);
    $tahun = substr($sekarang, 2, 2);

    $judul_bln = array(1 => "Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des");
    $wk = $tanggal . " " . $judul_bln[(int) $bulan] . "'" . $tahun;
    return $wk;
}
?>
<?php

function BAL($tanggal) {
    $arr = split(" ", $tanggal);
    if ($arr[1] == "Januari") {
        $bul = "01";
    } else if ($arr[1] == "Februari") {
        $bul = "02";
    } else if ($arr[1] == "Maret") {
        $bul = "03";
    } else if ($arr[1] == "April") {
        $bul = "04";
    } else if ($arr[1] == "Mei") {
        $bul = "05";
    } else if ($arr[1] == "Juni") {
        $bul = "06";
    } else if ($arr[1] == "Juli") {
        $bul = "07";
    } else if ($arr[1] == "Agustus") {
        $bul = "08";
    } else if ($arr[1] == "September") {
        $bul = "09";
    } else if ($arr[1] == "Oktober") {
        $bul = "10";
    } else if ($arr[1] == "November") {
        $bul = "11";
    } else if ($arr[1] == "Nopember") {
        $bul = "11";
    } else if ($arr[1] == "Desember") {
        $bul = "12";
    }
    return "$arr[2]-$bul-$arr[0]";
}
?>

<?php

function BALP($tanggal) {
    $arr = split(" ", $tanggal);
    if ($arr[1] == "Jan") {
        $bul = "01";
    } else if ($arr[1] == "Feb") {
        $bul = "02";
    } else if ($arr[1] == "Mar") {
        $bul = "03";
    } else if ($arr[1] == "Apr") {
        $bul = "04";
    } else if ($arr[1] == "Mei") {
        $bul = "05";
    } else if ($arr[1] == "Jun") {
        $bul = "06";
    } else if ($arr[1] == "Jul") {
        $bul = "07";
    } else if ($arr[1] == "Agu") {
        $bul = "08";
    } else if ($arr[1] == "Sep") {
        $bul = "09";
    } else if ($arr[1] == "Okt") {
        $bul = "10";
    } else if ($arr[1] == "Nov") {
        $bul = "11";
    } else if ($arr[1] == "Nop") {
        $bul = "11";
    } else if ($arr[1] == "Des") {
        $bul = "12";
    }
    return "$arr[2]-$bul-$arr[0]";
}
?>


<?php

function process($conn, $sql) {
    $s = false;
    $conn->autocommit(FALSE);
    try {
        $rs = $conn->query($sql);
        if ($rs) {
            $conn->commit();
            $last_inserted_id = $conn->insert_id;
            $affected_rows = $conn->affected_rows;
            $s = true;
        }
    } catch (Exception $e) {
        echo 'fail: ' . $e->getMessage();
        $conn->rollback();
    }
    $conn->autocommit(TRUE);
    return $s;
}

function getJum($conn, $sql) {
    $rs = $conn->query($sql);
    $jum = $rs->num_rows;
    $rs->free();
    return $jum;
}

function getField($conn, $sql) {
    $rs = $conn->query($sql);
    $rs->data_seek(0);
    $d = $rs->fetch_assoc();
    $rs->free();
    return $d;
}

function getData($conn, $sql) {
    $rs = $conn->query($sql);
    $rs->data_seek(0);
    $arr = $rs->fetch_all(MYSQLI_ASSOC);
    //foreach($arr as $row) {
    //  echo $row['nama_kelas'] . '*<br>';
    //}

    $rs->free();
    return $arr;
}

function getPelanggan($conn, $kode) {
    $field = "nama_pelanggan";
    $sql = "SELECT `$field` FROM `tabel_pelanggan` where `id_pelanggan`='$kode'";
    $rs = $conn->query($sql);
    $rs->data_seek(0);
    $row = $rs->fetch_assoc();
    $rs->free();
    return $row[$field];
}

function getUser($conn, $kode) {
    $field = "nama_user";
    $sql = "SELECT `$field` FROM `tabel_masterpetugas` where `id_user`='$kode'";
    $rs = $conn->query($sql);
    $rs->data_seek(0);
    $row = $rs->fetch_assoc();
    $rs->free();
    return $row[$field];
}

function getVendor($conn, $kode) {
    $field = "nama_vendor";
    $sql = "SELECT `$field` FROM `tabel_mastervendor` where `id_vendor`='$kode'";
    $rs = $conn->query($sql);
    $rs->data_seek(0);
    $row = $rs->fetch_assoc();
    $rs->free();
    return $row[$field];
}

function getStatus($conn, $kode) {
    $field = "nama_status";
    $sql = "SELECT `$field` FROM `tabel_masterstatus` where `id_status_pelaksanaan`='$kode'";
    $rs = $conn->query($sql);
    $rs->data_seek(0);
    $row = $rs->fetch_assoc();
    $rs->free();
    return $row[$field];
}
?>
