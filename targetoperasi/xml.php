<?php
header("Content-type: text/xml");

include "../konmysqli.php";
$sql = "select * from `$tbtargetoperasi`";
if(getJum($conn,$sql)>0){
	print "<to>\n";
		$arr=getData($conn,$sql);
		foreach($arr as $d) {
				$id_targetoperasi=$d["id_targetoperasi"];
				$id_ap2t=$d["id_ap2t"];
				$id_pelanggan=$d["id_pelanggan"];
				$judul=$d["judul"];
				$uraian=$d["uraian"];
				$lokasi=$d["lokasi"];
				$jam_targetoperasi=$d["jam_targetoperasi"];
				$tanggal_targetoperasi=$d["tanggal_targetoperasi"];
			    $keterangan=$d["keterangan"];
				$status=$d["status"];

				print "<record>\n";
				print "  <id_ap2t>$id_ap2t</id_ap2t>\n";
				print "  <id_pelanggan>$id_pelanggan</id_pelanggan>\n";
				print "  <judul>$judul</judul>\n";
				print "  <uraian>$uraian</uraian>\n";
				print "  <lokasi>$lokasi</lokasi>\n";
				print "  <jam_targetoperasi>$jam_targetoperasi</jam_targetoperasi>\n";
				print "  <tanggal_targetoperasi>$tanggal_targetoperasi</tanggal_targetoperasi>\n";
				print "  <keterangan>$keterangan</keterangan>\n";
				print "  <status>$status</status>\n";
				print "  <id_targetoperasi>$id_targetoperasi</id_targetoperasi>\n";
				print "</record>\n";
			}
	print "</to>\n";
}
else{
	$null="null";
	print "<to>\n";
		print "<record>\n";
				print "  <id_ap2t>$null</id_ap2t>\n";
				print "  <id_pelanggan>$null</id_pelanggan>\n";
				print "  <judul>$null</judul>\n";
				print "  <uraian>$uraian</uraian>\n";
				print "  <lokasi>$lokasi</lokasi>\n";
				print "  <jam_targetoperasi>$jam_targetoperasi</jam_targetoperasi>\n";
				print "  <tanggal_targetoperasi>$tanggal_targetoperasi</tanggal_targetoperasi>\n";
				print "  <keterangan>$null</keterangan>\n";
				print "  <status>$null</status>\n";
				print "  <id_targetoperasi>$null</id_targetoperasi>\n";
		print "</record>\n";
	print "</to>\n";
	}
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

function getJum($conn,$sql){
  $rs=$conn->query($sql);
  $jum= $rs->num_rows;
	$rs->free();
	return $jum;
}

function getData($conn,$sql){
	$rs=$conn->query($sql);
	$rs->data_seek(0);
	$arr = $rs->fetch_all(MYSQLI_ASSOC);

	$rs->free();
	return $arr;
}
?>
