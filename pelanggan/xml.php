<?php
header("Content-type: text/xml");

include "../konmysqli.php";
$sql = "select * from `$tbpelanggan`";
if(getJum($conn,$sql)>0){
	print "<to>\n";
		$arr=getData($conn,$sql);
		foreach($arr as $d) {
				$id_pelanggan=$d["id_pelanggan"];
				$nama_pelanggan=$d["nama_pelanggan"];
				$alamat_pelanggan=$d["alamat_pelanggan"];
				$tarif=$d["tarif"];
				$daya=$d["daya"];
			    $telepon_pelanggan=$d["telepon_pelanggan"];
				$status=$d["status"];

				print "<record>\n";
				print "  <nama_pelanggan>$nama_pelanggan</nama_pelanggan>\n";
				print "  <alamat_pelanggan>$alamat_pelanggan</alamat_pelanggan>\n";
				print "  <tarif>$tarif</tarif>\n";
				print "  <daya>$daya</daya>\n";
				print "  <telepon_pelanggan>$telepon_pelanggan</telepon_pelanggan>\n";
				print "  <status>$status</status>\n";
				print "  <id_pelanggan>$id_pelanggan</id_pelanggan>\n";
				print "</record>\n";
			}
	print "</to>\n";
}
else{
	$null="null";
	print "<to>\n";
		print "<record>\n";
				print "  <nama_pelanggan>$null</nama_pelanggan>\n";
				print "  <alamat_pelanggan>$null</alamat_pelanggan>\n";
				print "  <tarif>$tarif</tarif>\n";
				print "  <daya>$daya</daya>\n";
				print "  <telepon_pelanggan>$null</telepon_pelanggan>\n";
				print "  <status>$null</status>\n";
				print "  <id_pelanggan>$null</id_pelanggan>\n";
		print "</record>\n";
	print "</to>\n";
	}
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

function getJum($conn,$sql){
  $rs=$conn->query($sql);
  $jum= $rs->num_rows;
	$rs->free();
	return $jum;
}

function getData($conn,$sql){
	$rs=$conn->query($sql);
	$rs->data_seek(0);
	$arr = $rs->fetch_all(MYSQLI_ASSOC);

	$rs->free();
	return $arr;
}
?>
