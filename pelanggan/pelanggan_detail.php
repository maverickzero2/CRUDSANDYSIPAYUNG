<?php
require_once"../konmysqli.php";
$respon = array();

if (isset($_GET["id_pelanggan"])) {
    $id_pelanggan = $_GET['id_pelanggan'];
	$sql="SELECT * FROM `$tbpelanggan` WHERE `id_pelanggan` = '$id_pelanggan'";
    $jum=getJum($conn,$sql);
    if ($jum>0) {
            $d=getField($conn,$sql);
		    $record = array();
            $record["id_pelanggan"] = $d["id_pelanggan"];
			$record["nama_pelanggan"] = $d["nama_pelanggan"];
			$record["alamat_pelanggan"] = $d["alamat_pelanggan"];
			$record["tarif"] = $d["tarif"];
      $record["daya"] = $d["daya"];
			$record["telepon_pelanggan"] = $d["telepon_pelanggan"];
			$record["status"] = $d["status"];
      $record["latitude"] = $d["latitude"];
			$record["longitude"] = $d["longitude"];

            $respon["sukses"] = 1;
            $respon["record"] = array();

            array_push($respon["record"], $record);
             $respon["pesan"] = "$jum record";
			echo json_encode($respon);
        } else {
            $respon["sukses"] = 0;
            $respon["pesan"] = "0 record";
            echo json_encode($respon);
        }

} else {
    $respon["sukses"] = 0;
    $respon["pesan"] = "? lengkapi data";
    echo json_encode($respon);
}
?>

<?php

function getJum($conn,$sql){
  $rs=$conn->query($sql);
  $jum= $rs->num_rows;
	$rs->free();
	return $jum;
}

function getField($conn,$sql){
	$rs=$conn->query($sql);
	$rs->data_seek(0);
	$d= $rs->fetch_assoc();
	$rs->free();
	return $d;
}
?>
