<?php
$pro = "simpan";
$tanggal = WKT(date("Y-m-d"));

if(!isset($_SESSION["cid"])){
    die("<script>location.href='index.php'</script>");
}
?>
<link type="text/css" href="<?php echo "$PATH/base/"; ?>ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo "$PATH/"; ?>jquery-1.3.2.js"></script>
<script type="text/javascript" src="<?php echo "$PATH/"; ?>ui/ui.core.js"></script>
<script type="text/javascript" src="<?php echo "$PATH/"; ?>ui/ui.datepicker.js"></script>
<script type="text/javascript" src="<?php echo "$PATH/"; ?>ui/i18n/ui.datepicker-id.js"></script>

<script type="text/javascript">
$(document).ready(function () {
    $("#tanggal").datepicker({
        dateFormat: "dd MM yy",
        changeMonth: true,
        changeYear: true
    });
});
</script>

<script type="text/javascript">
function PRINT() {
    win = window.open('pelanggan/print.php', 'win', 'width=1000, height=400, menubar=0, scrollbars=1, resizable=0, location=0, toolbar=0, status=0');
}
</script>
<script language="JavaScript">
function buka(url) {
    window.open(url, 'window_baru', 'width=800,height=600,left=320,top=100,resizable=1,scrollbars=1');
}
</script>

<?php
$sql = "select `id_pelanggan` from `$tbpelanggan` order by `id_pelanggan` desc";
$q = mysqli_query($conn, $sql);
$jum = mysqli_num_rows($q);
$th = date("y");
$bl = date("m") + 0;
if ($bl < 10) {
    $bl = "0" . $bl;
}

$kd = "PEL" . $th . $bl; //KEG1610001
if ($jum > 0) {
    $d = mysqli_fetch_array($q);
    $idmax = $d["id_pelanggan"];

    $bul = substr($idmax, 5, 2);
    $tah = substr($idmax, 3, 2);
    if ($bul == $bl && $tah == $th) {
        $urut = substr($idmax, 7, 3) + 1;
        if ($urut < 10) {
            $idmax = "$kd" . "00" . $urut;
        } else if ($urut < 100) {
            $idmax = "$kd" . "0" . $urut;
        } else {
            $idmax = "$kd" . $urut;
        }
    }//==
    else {
        $idmax = "$kd" . "001";
    }
}//jum>0
else {
    $idmax = "$kd" . "001";
}
$id_pelanggan = $idmax;
?>

<?php
if ($_GET["pro"] == "ubah") {
    $id_pelanggan = $_GET["kode"];
    $sql = "select * from `$tbpelanggan` where `id_pelanggan`='$id_pelanggan'";
    $d = getField($conn, $sql);
    $id_pelanggan = $d["id_pelanggan"];
    $id_pelanggan0 = $d["id_pelanggan"];
    $nama_pelanggan = $d["nama_pelanggan"];
    $alamat_pelanggan = $d["alamat_pelanggan"];
    $tarif = $d["tarif"];
    $daya = $d["daya"];
    $telepon_pelanggan = $d["telepon_pelanggan"];
    $status = $d["status"];
    $pro = "ubah";
}
?>
<div class="panel-group" id="accordion">
<div class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title">
      <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
      Data Pelanggan</a>
    </h4>
  </div>
  <div id="collapse1" class="panel-collapse collapse in">
    <div class="panel-body">
Data pelanggan:
| <a href="pelanggan/pdf.php"><img src='ypathicon/pdf.png' alt='PDF'></a>
| <a href="pelanggan/xls.php"><img src='ypathicon/xls.png' alt='XLS'></a>
| <a href="pelanggan/xml.php"><img src='ypathicon/xml.png' alt='XML'></a>
| <img src='ypathicon/print.png' alt='PRINT' OnClick="PRINT()"> |
<br>
<div class="row">
    <div class="table-responsive">
        <table id="tabelpelanggan" class="table table-bordered table-striped dataTable">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Kode</th>
                    <th>NAMA</th>
                    <th>ALAMAT</th>
                    <th>TARIF</th>
                    <th>DAYA</th>
                    <th>TELEPON</th>
                    <th>STATUS</th>
                </tr>
            </thead>
            <?php
            $sql = "select * from `$tbpelanggan` order by `id_pelanggan` desc";
            $jum = getJum($conn, $sql);
            if ($jum > 0) {
                //--------------------------------------------------------------------------------------------
                $batas = 10;
                $page = $_GET['page'];
                if (empty($page)) {
                    $posawal = 0;
                    $page = 1;
                } else {
                    $posawal = ($page - 1) * $batas;
                }

                $sql2 = $sql . " LIMIT $posawal,$batas";
                $no = $posawal + 1;
                //--------------------------------------------------------------------------------------------
                $arr = getData($conn, $sql2);
                foreach ($arr as $d) {
                    $id_pelanggan = $d["id_pelanggan"];
                    $nama_pelanggan = $d["nama_pelanggan"];
                    $alamat_pelanggan = $d["alamat_pelanggan"];
                    $tarif = $d["tarif"];
                    $daya = $d["daya"];
                    $telepon_pelanggan = $d["telepon_pelanggan"];
                    $status = $d["status"];
                    $color = "#dddddd";
                    if ($no % 2 == 0) {
                        $color = "#eeeeee";
                    }
                    echo"<tr bgcolor='$color'>
                    <td>$no</td>
                    <td>$id_pelanggan</td>
                    <td>$nama_pelanggan</td>
                    <td>$alamat_pelanggan</td>
                    <td>$tarif</td>
                    <td>$daya</td>
                    <td>$telepon_pelanggan</td>
                    <td align='center'>$status</td>

                    </tr>";

                    $no++;
                }//while
            }//if
            else {
                echo"<tr><td colspan='7'><blink>Maaf, Data pelanggan belum tersedia...</blink></td></tr>";
            }
            ?>
        </table>
    </div>
</div>
</div></div></div></div>  
<?php
if (isset($_POST["Simpan"])) {
    $pro = strip_tags($_POST["pro"]);
    $id_pelanggan = strip_tags($_POST["id_pelanggan"]);
    $id_pelanggan0 = strip_tags($_POST["id_pelanggan0"]);
    $nama_pelanggan = strip_tags($_POST["nama_pelanggan"]);
    $alamat_pelanggan = strip_tags($_POST["alamat_pelanggan"]);
    $tarif = strip_tags($_POST["tarif"]);
    $daya = strip_tags($_POST["daya"]);
    $telepon_pelanggan = strip_tags($_POST["telepon_pelanggan"]);
    $status = strip_tags($_POST["status"]);

    if ($pro == "simpan") {
        $sql = " INSERT INTO `$tbpelanggan` (
            `id_pelanggan` ,
            `nama_pelanggan` ,
            `alamat_pelanggan` ,
            `tarif` ,
            `daya` ,
            `telepon_pelanggan` ,
            `status`
        ) VALUES (
            '$id_pelanggan',
            '$nama_pelanggan',
            '$alamat_pelanggan',
            '$tarif',
            '$daya',
            '$telepon_pelanggan',
            '$status'
        )";

        $simpan = process($conn, $sql);
        if ($simpan) {
            echo "<script>alert('Data $id_pelanggan berhasil disimpan !');document.location.href='?mnu=pelanggan';</script>";
        } else {
            echo"<script>alert('Data $id_pelanggan gagal disimpan...');document.location.href='?mnu=pelanggan';</script>";
        }
    } else {
        $sql = "update `$tbpelanggan` set
        `nama_pelanggan`='$nama_pelanggan' ,
        `alamat_pelanggan`='$alamat_pelanggan',
        `tarif`='$tarif',
        `daya`='$daya',
        `status`='$status',
        `telepon_pelanggan`='$telepon_pelanggan'
        where `id_pelanggan`='$id_pelanggan0'";
        $ubah = process($conn, $sql);
        if ($ubah) {
            echo "<script>alert('Data $id_pelanggan berhasil diubah !');document.location.href='?mnu=pelanggan';</script>";
        } else {
            echo"<script>alert('Data $id_pelanggan gagal diubah...');document.location.href='?mnu=pelanggan';</script>";
        }
    }//else simpan
}
?>

<?php
if ($_GET["pro"] == "hapus") {
    $id_pelanggan = $_GET["kode"];
    $sql = "delete from `$tbpelanggan` where `id_pelanggan`='$id_pelanggan'";
    $hapus = process($conn, $sql);
    if ($hapus) {
        echo "<script>alert('Data pelanggan $id_pelanggan berhasil dihapus !');document.location.href='?mnu=pelanggan';</script>";
    } else {
        echo"<script>alert('Data pelanggan $id_pelanggan gagal dihapus...');document.location.href='?mnu=pelanggan';</script>";
    }
}
?>
