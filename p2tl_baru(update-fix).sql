/*
Navicat MySQL Data Transfer

Source Server         : localhost#XAMPP
Source Server Version : 100119
Source Host           : localhost:3306
Source Database       : p2tl_baru

Target Server Type    : MYSQL
Target Server Version : 100119
File Encoding         : 65001

Date: 2017-05-31 10:47:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for activity_login
-- ----------------------------
DROP TABLE IF EXISTS `activity_login`;
CREATE TABLE `activity_login` (
  `id_session` varchar(255) NOT NULL,
  `id_user` varchar(255) DEFAULT NULL,
  `no_hp` varchar(255) DEFAULT NULL,
  `no_imei` varchar(255) DEFAULT NULL,
  `time_login` datetime DEFAULT NULL,
  `login_latitude` varchar(255) DEFAULT NULL,
  `login_longitude` varchar(255) DEFAULT NULL,
  `time_logout` datetime DEFAULT NULL,
  `logout_latitude` varchar(255) DEFAULT NULL,
  `logout_longitude` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_session`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of activity_login
-- ----------------------------
INSERT INTO `activity_login` VALUES ('WOR1704005', 'TOP1704003', 'VEN1704008', 'USR1704004', '0000-00-00 00:00:00', '15:16:27', '2017-04-18', '0000-00-00 00:00:00', '', null);
INSERT INTO `activity_login` VALUES ('WOR1704006', 'TOP1704002', 'VEN1704008', 'USR1704004', '0000-00-00 00:00:00', '15:17:54', '2017-04-18', '0000-00-00 00:00:00', '', null);
INSERT INTO `activity_login` VALUES ('WOR1704007', 'TOP1704002', 'VEN1704008', 'USR1704004', '0000-00-00 00:00:00', '19:19:34', '2017-04-18', '0000-00-00 00:00:00', 'fad', null);
INSERT INTO `activity_login` VALUES ('WOR1704008', 'TOP1704002', 'VEN1704008', 'USR1704004', '0000-00-00 00:00:00', '13:44:19', '2017-04-19', '0000-00-00 00:00:00', 'OKE OCE', null);
INSERT INTO `activity_login` VALUES ('WOR1704009', 'TOP1704002', 'VEN1704008', 'USR1704004', '0000-00-00 00:00:00', '21:06:53', '2017-04-18', '0000-00-00 00:00:00', '', null);
INSERT INTO `activity_login` VALUES ('WOR1704010', 'TOP1704002', 'VEN1704008', 'USR1704004', '0000-00-00 00:00:00', '21:08:00', '2017-04-19', '0000-00-00 00:00:00', '', null);
INSERT INTO `activity_login` VALUES ('WOR1704011', 'TOP1704002', 'VEN1704008', 'USR1704004', '0000-00-00 00:00:00', '21:27:59', '2017-04-07', '0000-00-00 00:00:00', '', null);
INSERT INTO `activity_login` VALUES ('WOR1704012', 'TOP1704002', 'VEN1704008', 'USR1704004', '0000-00-00 00:00:00', '21:28:04', '2017-04-22', '0000-00-00 00:00:00', '', null);
INSERT INTO `activity_login` VALUES ('WOR1704013', 'TOP1704002', 'VEN1704008', 'USR1704006', '0000-00-00 00:00:00', '11:18:51', '2017-04-28', '0000-00-00 00:00:00', 'DES', null);

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `kode_admin` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `username` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `password` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `telepon` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `gambar` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `status` varchar(15) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`kode_admin`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('ADM02', 'a', 'a', '0234567845678', 'admin@yahoo.com', 'wifi.png', 'Aktif');
INSERT INTO `admin` VALUES ('ADM03', 'array', 'array', '02345678923456', 'array@a.com', 'keys.jpg', 'Aktif');
INSERT INTO `admin` VALUES ('ADM01', 'jokowi', 'jokowi', '123', 'presidenri@gmail.com', 'key.jpg', 'Aktif');
INSERT INTO `admin` VALUES ('ADM17040', 'sandysipayung', '123', '23', 'as', '764467.jpg', 'Aktif');
INSERT INTO `admin` VALUES ('ADM1704001', 'duia', 'asd', '8967887', 'gatot@gatot.co', 'avatar.jpg', 'NonAktif');
INSERT INTO `admin` VALUES ('ADM1705001', 'mohbennyirwansyah02@gmail', 'admin123', '312321', 'sipayung.sandy@gmail.com', 'avatar.jpg', 'NonAktif');
INSERT INTO `admin` VALUES ('ADM1705002', 'mohbennyirwansyah02@gmail', 'admin123', '32432', 'rrrr#tttt', 'avatar.jpg', 'Aktif');

-- ----------------------------
-- Table structure for statistik
-- ----------------------------
DROP TABLE IF EXISTS `statistik`;
CREATE TABLE `statistik` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `ip` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `tanggal` date NOT NULL,
  `hits` int(8) NOT NULL,
  `online` varchar(20) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of statistik
-- ----------------------------
INSERT INTO `statistik` VALUES ('1', '127.0.0.1', '2014-03-17', '63', '1395034465');
INSERT INTO `statistik` VALUES ('2', '127.0.0.1', '2014-03-18', '53', '1395129935');
INSERT INTO `statistik` VALUES ('3', '127.0.0.1', '2014-03-22', '122', '1395493770');
INSERT INTO `statistik` VALUES ('4', '127.0.0.1', '2014-04-17', '50', '1397733464');
INSERT INTO `statistik` VALUES ('5', '127.0.0.1', '2014-04-18', '55', '1397839756');
INSERT INTO `statistik` VALUES ('6', '127.0.0.1', '2014-04-19', '26', '1397883619');
INSERT INTO `statistik` VALUES ('7', '127.0.0.1', '2014-05-28', '9', '1401282009');
INSERT INTO `statistik` VALUES ('8', '127.0.0.1', '2014-05-31', '39', '1401531874');
INSERT INTO `statistik` VALUES ('9', '127.0.0.1', '2014-06-03', '30', '1401783305');
INSERT INTO `statistik` VALUES ('10', '127.0.0.1', '2014-06-09', '12', '1402299670');
INSERT INTO `statistik` VALUES ('11', '127.0.0.1', '2014-06-18', '8', '1403092882');
INSERT INTO `statistik` VALUES ('12', '127.0.0.1', '2014-06-20', '1954', '1403269933');
INSERT INTO `statistik` VALUES ('13', '127.0.0.1', '2014-10-15', '86', '1413374159');
INSERT INTO `statistik` VALUES ('14', '127.0.0.1', '2014-10-22', '107', '1413951013');
INSERT INTO `statistik` VALUES ('15', '127.0.0.1', '2014-10-24', '3', '1414149898');
INSERT INTO `statistik` VALUES ('16', '127.0.0.1', '2014-11-04', '20', '1415070918');
INSERT INTO `statistik` VALUES ('17', '127.0.0.1', '2014-11-05', '46', '1415154829');
INSERT INTO `statistik` VALUES ('18', '127.0.0.1', '2014-11-11', '24', '1415666142');
INSERT INTO `statistik` VALUES ('19', '127.0.0.1', '2014-11-23', '35', '1416719646');
INSERT INTO `statistik` VALUES ('20', '127.0.0.1', '2015-01-02', '196', '1420215747');
INSERT INTO `statistik` VALUES ('21', '127.0.0.1', '2015-01-03', '24', '1420264639');
INSERT INTO `statistik` VALUES ('22', '127.0.0.1', '2015-01-06', '45', '1420511116');
INSERT INTO `statistik` VALUES ('23', '127.0.0.1', '2015-02-01', '271', '1422770430');
INSERT INTO `statistik` VALUES ('24', '::1', '2017-04-11', '103', '1491901414');
INSERT INTO `statistik` VALUES ('25', '::1', '2017-04-15', '16', '1492234998');

-- ----------------------------
-- Table structure for tabel_masterpetugas
-- ----------------------------
DROP TABLE IF EXISTS `tabel_masterpetugas`;
CREATE TABLE `tabel_masterpetugas` (
  `id_user` varchar(255) NOT NULL,
  `id_vendor` varchar(255) DEFAULT NULL,
  `nama_user` varchar(255) DEFAULT NULL,
  `alamat_user` varchar(255) DEFAULT NULL,
  `telepon_user` varchar(255) DEFAULT NULL,
  `nomor_imei` varchar(255) DEFAULT NULL,
  `email_user` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `level_user` varchar(255) DEFAULT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tabel_masterpetugas
-- ----------------------------
INSERT INTO `tabel_masterpetugas` VALUES ('USR001', 'VEN1705001', 'Anggy Rafael Sipayung', 'Jl KOAI', '098765', '0987654', 'rafael@rafa.co', '12345', 'PIC', '_MG_8585.jpg');
INSERT INTO `tabel_masterpetugas` VALUES ('USR002', 'VEN1705002', 'Sandy Sipayung', 'Jl KIAI TAPA', '0987655', '98765444', 'sandy@sandy.co', '12345', 'PIC', '_MG_8585.jpg');
INSERT INTO `tabel_masterpetugas` VALUES ('USR1705001', 'VEN1705001', 'Tygres Enjel Elio Enai Sipayung', 'Pematangsiantar', '08776736467', '', 'tygres@tygres.com', '12345', 'PL', 'TYGRES.jpg');
INSERT INTO `tabel_masterpetugas` VALUES ('USR1705002', 'VEN1705002', 'Reinhard A', 'Pematangsiantar', '08776736467', '', 'tygres@tygres.com', '12345', 'PL', 'creative-cartoon-phone-holder-ring-buckle-stand-smartphone (1).jpg');

-- ----------------------------
-- Table structure for tabel_masterstatus
-- ----------------------------
DROP TABLE IF EXISTS `tabel_masterstatus`;
CREATE TABLE `tabel_masterstatus` (
  `id_status_pelaksanaan` varchar(255) NOT NULL,
  `nama_status` varchar(255) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_status_pelaksanaan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tabel_masterstatus
-- ----------------------------
INSERT INTO `tabel_masterstatus` VALUES ('STA1704001', 'Berangkat', 'Berangkat ke stasiun untuk pelaksanaan');
INSERT INTO `tabel_masterstatus` VALUES ('STA1704002', 'Sampai', 'Tiba dilokasi');
INSERT INTO `tabel_masterstatus` VALUES ('STA1704003', 'Penertiban', 'Sedang dilakukan penertiban');
INSERT INTO `tabel_masterstatus` VALUES ('STA1704004', 'Penertiban Kosong', 'Dikembalikan ke PIC');

-- ----------------------------
-- Table structure for tabel_mastervendor
-- ----------------------------
DROP TABLE IF EXISTS `tabel_mastervendor`;
CREATE TABLE `tabel_mastervendor` (
  `id_vendor` varchar(255) NOT NULL,
  `nama_vendor` varchar(255) DEFAULT NULL,
  `alamat_vendor` varchar(255) DEFAULT NULL,
  `telepon_vendor` varchar(255) DEFAULT NULL,
  `email_vendor` varchar(255) DEFAULT NULL,
  `pic_vendor` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_vendor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tabel_mastervendor
-- ----------------------------
INSERT INTO `tabel_mastervendor` VALUES ('VEN1705001', 'PT DEI INDONESIA', 'JL H U TIGARAS', '09896545565', 'dei.utama@dei.com', 'USR001');
INSERT INTO `tabel_mastervendor` VALUES ('VEN1705002', 'PT LISTRIK RAKYAT', 'JL KANTIN', '0954565867', 'lis@listrik.com', 'USR002');

-- ----------------------------
-- Table structure for tabel_pelanggan
-- ----------------------------
DROP TABLE IF EXISTS `tabel_pelanggan`;
CREATE TABLE `tabel_pelanggan` (
  `id_pelanggan` varchar(255) NOT NULL,
  `nama_pelanggan` varchar(255) DEFAULT NULL,
  `alamat_pelanggan` text,
  `tarif` varchar(65) DEFAULT NULL,
  `daya` varchar(255) DEFAULT NULL,
  `telepon_pelanggan` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_pelanggan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tabel_pelanggan
-- ----------------------------
INSERT INTO `tabel_pelanggan` VALUES ('PEL1704006', 'Payuung', 'Medan', 'R3', '5000', 'Aktif', 'Aktif');
INSERT INTO `tabel_pelanggan` VALUES ('PEL1704008', 'GLORY SANDY', 'Jakarta', 'R4', '50000', '0863728928', 'Aktif');
INSERT INTO `tabel_pelanggan` VALUES ('PEL1704009', 'AHOK', 'JL PANCUR BATU', 'R5', '700', '0899999999', 'NonAktif');
INSERT INTO `tabel_pelanggan` VALUES ('PEL1705001', 'KUMUCI', 'JL H I', 'R4', '2300', '0783863875', 'Aktif');
INSERT INTO `tabel_pelanggan` VALUES ('PEL1705002', 'GOGO', 'JALAN KISMAH', 'R4', '900', '9876543', 'Aktif');

-- ----------------------------
-- Table structure for tabel_transdata_p2tl
-- ----------------------------
DROP TABLE IF EXISTS `tabel_transdata_p2tl`;
CREATE TABLE `tabel_transdata_p2tl` (
  `id_transdata_p2tl` varchar(255) NOT NULL,
  `nomor_targetoperasi` varchar(255) NOT NULL,
  `tanggal_targetoperasi` datetime DEFAULT NULL,
  `id_user` varchar(255) DEFAULT NULL,
  `id_pelanggan` varchar(255) DEFAULT NULL,
  `id_vendor` varchar(255) DEFAULT NULL,
  `nomor_workorder` varchar(255) DEFAULT NULL,
  `tanggal_workorder` datetime DEFAULT NULL,
  `tanggal_respon_workorder` varchar(255) DEFAULT NULL,
  `petugas_penerima` varchar(255) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `foto_1` varchar(255) DEFAULT NULL,
  `foto_2` varchar(255) DEFAULT NULL,
  `foto_3` varchar(255) DEFAULT NULL,
  `foto_4` varchar(255) DEFAULT NULL,
  `foto_5` varchar(255) DEFAULT NULL,
  `foto_6` varchar(255) DEFAULT NULL,
  `petugas_lapangan` varchar(255) DEFAULT NULL,
  `cek_pelanggaran` varchar(255) DEFAULT NULL,
  `kode_pelanggaran` varchar(255) DEFAULT NULL,
  `status_pelaksanaan` varchar(255) DEFAULT NULL,
  `tanggal_berangkat` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `tanggal_sampai` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `tanggal_penertiban` datetime DEFAULT NULL,
  PRIMARY KEY (`id_transdata_p2tl`,`nomor_targetoperasi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tabel_transdata_p2tl
-- ----------------------------
INSERT INTO `tabel_transdata_p2tl` VALUES ('TDP1705002', 'TO001', '2017-05-17 00:00:00', 'USR002', 'PEL1704006', 'VEN1705002', 'WO002', '2017-05-17 00:00:00', '', '', 'LA97873847', 'LO0834676', 'iseng sandy.png', 'iseng sandy.png', 'iseng sandy.png', 'iseng sandy.png', 'iseng sandy.png', 'iseng sandy.png', '', 'ya', 'KP011', 'STA1704003', '2017-05-26 00:37:52', '2017-05-26 00:37:52', '2017-05-23 00:00:00');
INSERT INTO `tabel_transdata_p2tl` VALUES ('TDP1705003', 'TO002', '0000-00-00 00:00:00', 'USR001', 'Bbbbbbbbbbb', 'VEN1705001', 'Bbbb', '0000-00-00 00:00:00', 'Bbb', 'Bbbb', 'Bbb', 'Bbbbbbbbbbb', 'Bbb', 'Bb', 'Bbb', 'Bbbb', 'Bbbbb', 'Bbbb', '', 'tidak', 'Bbb', 'Bn', '2017-05-26 04:14:41', '2017-05-26 04:14:41', '0000-00-00 00:00:00');
INSERT INTO `tabel_transdata_p2tl` VALUES ('TOP1705004', 'TO003', '2017-05-08 14:00:55', 'USR002', 'PEL1704008', 'VEN1705002', 'WO001', '2017-05-16 00:00:00', '2017-05-23 19:45:21', 'USR1705001', 'GSPKI', 'PKIGS', '764467.jpg', '1468855123242.jpg', '1468855125022.jpg', '20161210_071733.jpg', '2014032it.jpg', 'combo-portable-bluetooth-speaker-radio-clock-with-tf-card-slot-kd-66-black-1.jpg', 'USR1705002', 'tidak', '-', 'STA1704001', '2017-05-26 01:10:31', '2017-05-26 01:10:31', '1900-12-08 03:10:00');
INSERT INTO `tabel_transdata_p2tl` VALUES ('TOP1705005', 'TO0011', '2017-05-08 14:00:55', 'USR001', 'PEL1704008', 'VEN1705001', 'WO001', '2017-05-16 00:00:00', '2017-05-23 19:45:21', 'USR1705001', 'GSPKI', 'PKIGS', 'combo-portable-bluetooth-speaker-radio-clock-with-tf-card-slot-kd-66-black-1.jpg', '1468855123242.jpg', '1468855123242.jpg', '1468855123242.jpg', '1468855123242.jpg', '1468855123242.jpg', 'USR1705001', 'ya', '134', 'STA1704001', '2017-05-26 00:32:24', '2017-05-26 00:32:24', '2000-12-08 03:10:00');
