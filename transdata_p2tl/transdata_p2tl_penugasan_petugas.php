<?php
require_once"../konmysqli.php";
date_default_timezone_set("Asia/Jakarta");
$respon = array();

$sql = "select `nomor_workorder` from `$tbtransdata_p2tl` order by `nomor_workorder` desc";
$q = mysqli_query($conn, $sql);
$jum = mysqli_num_rows($q);
$th = date("y");
$bl = date("m") + 0;
if ($bl < 10) {
		$bl = "0" . $bl;
}

$kd = "WOR" . $th . $bl; //KEG1610001
if ($jum > 0) {
		$d = mysqli_fetch_array($q);
		$idmax = $d["nomor_workorder"];

		$bul = substr($idmax, 5, 2);
		$tah = substr($idmax, 3, 2);
		if ($bul == $bl && $tah == $th) {
				$urut = substr($idmax, 7, 3) + 1;
				if ($urut < 10) {
						$idmax = "$kd" . "00" . $urut;
				} else if ($urut < 100) {
						$idmax = "$kd" . "0" . $urut;
				} else {
						$idmax = "$kd" . $urut;
				}
		}//==
		else {
				$idmax = "$kd" . date("his");
		}
}//jum>0
else {
		$idmax = "$kd" . date("his");
}
$nomor_workorder = $idmax;

if (isset($_POST['nama_petugas_lapangan']) ) {
		$id_transdata_p2tl0= $_POST['id_transdata_p2tl0'];
		$nomor_targetoperasi = $_POST['nomor_targetoperasi'];
		$tanggal_targetoperasi = $_POST['tanggal_targetoperasi'];
		$id_user = $_POST['id_user'];
		$id_pelanggan = $_POST['id_pelanggan'];
		// $nama_pelanggan = $_POST['nama_pelanggan'];
		// $alamat_pelanggan = $_POST['alamat_pelanggan'];
		// $tarif = $_POST['tarif'];
		// $daya = $_POST['daya'];
    $id_vendor = $_POST['id_vendor'];

    $tanggal_workorder = $_POST['tanggal_workorder'];
    $tanggal_respon_workorder = $_POST['tanggal_respon_workorder'];
    $petugas_penerima = strip_tags($_POST["petugas_penerima"]);
      $latitude = strip_tags($_POST["latitude"]);
      $longitude = strip_tags($_POST["longitude"]);
      $foto_1 = strip_tags($_POST["foto_1"]);
      $foto_2 = strip_tags($_POST["foto_2"]);
      $foto_3 = strip_tags($_POST["foto_3"]);
      $foto_4 = strip_tags($_POST["foto_4"]);
      $foto_5 = strip_tags($_POST["foto_5"]);
      $foto_6 = strip_tags($_POST["foto_6"]);

      $nama_petugas_lapangan = $_POST['nama_petugas_lapangan'];
	$sql="select id_user from `$tbmasterpetugas` where `nama_user` LIKE '$nama_petugas_lapangan'";
	$d=getField($conn,$sql);
	$petugas_lapangan=$d["id_user"];

      // $petugas_lapangan = strip_tags($_POST["petugas_lapangan"]);
      $cek_pelanggaran = strip_tags($_POST["cek_pelanggaran"]);
      $kode_pelanggaran = strip_tags($_POST["kode_pelanggaran"]);
      // $tanggal_berangkat = strip_tags($_POST["tanggal_berangkat"]);
        // $tanggal_sampai = strip_tags($_POST["tanggal_sampai"]);
          $tanggal_penertiban = strip_tags($_POST["tanggal_penertiban"]);
		$status_pelaksanaan = $_POST['status_pelaksanaan'];
		$tanggal_workorder = (date("Y-m-d H:i:s"));

	$sql="UPDATE `$tbtransdata_p2tl` SET
	-- `nomor_targetoperasi` = '$nomor_targetoperasi',
	-- `tanggal_targetoperasi` = '$tanggal_targetoperasi',
	-- `id_user` = '$id_user',
	-- `id_pelanggan` = '$id_pelanggan',
  -- -- `nama_pelanggan` = '$nama_pelanggan',
  -- -- `alamat_pelanggan` = '$alamat_pelanggan',
  -- -- `tarif` = '$tarif',
  -- -- `daya` = '$daya',
  -- `id_vendor` = '$id_vendor',
  `nomor_workorder` = '$nomor_workorder',
  `tanggal_workorder` = '$tanggal_workorder',
  -- `tanggal_respon_workorder` = '$tanggal_respon_workorder',
  -- `petugas_penerima`='$petugas_penerima',
  -- `latitude`='$latitude',
  -- `longitude`='$longitude',
  -- `foto_1`='$foto_1',
  -- `foto_2`='$foto_2',
  -- `foto_3`='$foto_3',
  -- `foto_4`='$foto_4',
  -- `foto_5`='$foto_5',
  -- `foto_6`='$foto_6',
  `petugas_lapangan`='$petugas_lapangan'
  -- `cek_pelanggaran`='$cek_pelanggaran',
  -- `kode_pelanggaran`='$kode_pelanggaran',
  -- `tanggal_berangkat`='$tanggal_berangkat',
  -- `tanggal_sampai`='$tanggal_sampai',
  -- `tanggal_penertiban`='$tanggal_penertiban',
	-- `status_pelaksanaan` = '$status_pelaksanaan'
	WHERE `id_transdata_p2tl` = '$id_transdata_p2tl0'";
    $ubah=process($conn,$sql);

    if ($ubah) {
        $respon["sukses"] = 1;
        $respon["pesan"] = "1 sukses update.";
        echo json_encode($respon);
    } else {
        $respon["sukses"] = 0;
        $respon["pesan"] = "Gagal update data.";
        echo json_encode($respon);

    }
} else {
    $respon["sukses"] = 0;
    $respon["pesan"] = "data belum terset/terisi";
    echo json_encode($respon);
}
?>



<?php

function getField($conn,$sql){
  $rs=$conn->query($sql);
  $rs->data_seek(0);
  $d= $rs->fetch_assoc();
  $rs->free();
  return $d;
}

function process($conn,$sql){
$s=false;
$conn->autocommit(FALSE);
try {
  $rs = $conn->query($sql);
  if($rs){
	   $conn->commit();
	    $last_inserted_id = $conn->insert_id;
 		$affected_rows = $conn->affected_rows;
  		$s=true;
  }
}
catch (Exception $e) {
	echo 'fail: ' . $e->getMessage();
  	$conn->rollback();
}
$conn->autocommit(TRUE);
return $s;
}
?>
