<?php
require_once"../konmysqli.php";
$respon = array();

if (isset($_GET["id_transdata_p2tl"])) {
  $id_transdata_p2tl = $_GET['id_transdata_p2tl'];
  $sql="SELECT * FROM `$tbtransdata_p2tl` a LEFT JOIN `$tbpelanggan` b ON a.`id_pelanggan`=b.`id_pelanggan` WHERE `id_transdata_p2tl` = '$id_transdata_p2tl'";
  $jum=getJum($conn,$sql);
  if ($jum>0) {
    $d=getField($conn,$sql);
    $record = array();
    $record["id_transdata_p2tl"] = $d["id_transdata_p2tl"];
    $record["nomor_targetoperasi"] = $d["nomor_targetoperasi"];
    $record["tanggal_targetoperasi"] = $d["tanggal_targetoperasi"];
    $record["id_user"] = getUser($conn,$d["id_user"]);
    $record["id_pelanggan"] = $d["id_pelanggan"];
    $record["nama_pelanggan"] = $d["nama_pelanggan"];
    $record["alamat_pelanggan"] = $d["alamat_pelanggan"];
    $record["tarif"] = $d["tarif"];
    $record["daya"] = $d["daya"];
    $record["id_vendor"] = getVendor($conn, $d["id_vendor"]);
    $record["nomor_workorder"] = $d["nomor_workorder"];
    $record["tanggal_workorder"] = $d["tanggal_workorder"];
    $record["tanggal_respon_workorder"] = $d["tanggal_respon_workorder"];
    $record["petugas_penerima"] = getUser($conn,$d["petugas_penerima"]);
    $record["latitude"] = $d["latitude"];
    $record["longitude"] = $d["longitude"];
    $record["foto_1"] = $d["foto_1"];
    $record["foto_2"] = $d["foto_2"];
    $record["foto_3"] = $d["foto_3"];
    $record["foto_4"] = $d["foto_4"];
    $record["foto_5"] = $d["foto_5"];
    $record["foto_6"] = $d["foto_6"];
    $record["petugas_lapangan"] = getUser($conn,$d["petugas_lapangan"]);
    $record["cek_pelanggaran"] = $d["cek_pelanggaran"];
    $record["kode_pelanggaran"] = $d["kode_pelanggaran"];
    $record["tanggal_berangkat"] = $d["tanggal_berangkat"];
    $record["tanggal_sampai"] = $d["tanggal_sampai"];
    $record["tanggal_penertiban"] = $d["tanggal_penertiban"];
    $record["status_pelaksanaan"] = $d["status_pelaksanaan"];
    $record["alasan_tolak"] = $d["alasan_tolak"];


    $respon["sukses"] = 1;
    $respon["record"] = array();

    array_push($respon["record"], $record);
    $respon["pesan"] = "$jum record";
    echo json_encode($respon);
  } else {
    $respon["sukses"] = 0;
    $respon["pesan"] = "0 record";
    echo json_encode($respon);
  }

} else {
  $respon["sukses"] = 0;
  $respon["pesan"] = "? lengkapi data";
  echo json_encode($respon);
}
?>

<?php

function getJum($conn,$sql){
  $rs=$conn->query($sql);
  $jum= $rs->num_rows;
  $rs->free();
  return $jum;
}

function getField($conn,$sql){
  $rs=$conn->query($sql);
  $rs->data_seek(0);
  $d= $rs->fetch_assoc();
  $rs->free();
  return $d;
}
function getVendor($conn, $kode) {
    $field = "nama_vendor";
    $sql = "SELECT `$field` FROM `tabel_mastervendor` where `id_vendor`='$kode'";
    $rs = $conn->query($sql);
    $rs->data_seek(0);
    $row = $rs->fetch_assoc();
    $rs->free();
    return $row[$field];
}

function getUser($conn, $kode) {
    $field = "nama_user";
    $sql = "SELECT `$field` FROM `tabel_masterpetugas` where `id_user`='$kode'";
    $rs = $conn->query($sql);
    $rs->data_seek(0);
    $row = $rs->fetch_assoc();
    $rs->free();
    return $row[$field];
}
?>
