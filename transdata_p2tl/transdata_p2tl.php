<?php
$pro = "simpan";
$tanggal_workorder = (date("Y-m-d H:i:s"));
$tanggal_targetoperasi = (date("Y-m-d H:i:s"));
$tanggal_respon_workorder = (date("Y-m-d H:i:s"));
$tanggal_penertiban = (date("Y-m-d H:i:s"));
$tanggal_berangkat = (date("Y-m-d H:i:s"));
$tanggal_sampai = (date("Y-m-d H:i:s"));
$foto_10 = "avatar.jpg";
$foto_20 = "avatar.jpg";
$foto_30 = "avatar.jpg";
$foto_40 = "avatar.jpg";
$foto_50 = "avatar.jpg";
$foto_60 = "avatar.jpg";

if(!isset($_SESSION["cid"])){
    die("<script>location.href='index.php'</script>");
}
?>
<link type="text/css" href="<?php echo "$PATH/base/"; ?>ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo "$PATH/"; ?>jquery-1.3.2.js"></script>
<script type="text/javascript" src="<?php echo "$PATH/"; ?>ui/ui.core.js"></script>
<script type="text/javascript" src="<?php echo "$PATH/"; ?>ui/ui.datepicker.js"></script>
<script type="text/javascript" src="<?php echo "$PATH/"; ?>ui/i18n/ui.datepicker-id.js"></script>

<script type="text/javascript">
$(document).ready(function () {
$('.tanggal_workorder').datetimepicker({
      format: 'yyyy-mm-dd hh:ii:ss',
       weekStart: 1,
       todayBtn:  1,
       autoclose: 1,
       todayHighlight: 1,
       startView: 2,
       forceParse: 0,
       showMeridian: 1
   });
});

$(document).ready(function () {
$('.tanggal_respon_workorder').datetimepicker({
      format: 'yyyy-mm-dd hh:ii:ss',
       weekStart: 1,
       todayBtn:  1,
       autoclose: 1,
       todayHighlight: 1,
       startView: 2,
       forceParse: 0,
       showMeridian: 1
   });
});

$(document).ready(function () {
$('.tanggal_targetoperasi').datetimepicker({
      format: 'yyyy-mm-dd hh:ii:ss',
       weekStart: 1,
       todayBtn:  1,
       autoclose: 1,
       todayHighlight: 1,
       startView: 2,
       forceParse: 0,
       showMeridian: 1
   });
});

$(document).ready(function () {
$('.tanggal_penertiban').datetimepicker({
      format: 'yyyy-mm-dd hh:ii:ss',
       weekStart: 1,
       todayBtn:  1,
       autoclose: 1,
       todayHighlight: 1,
       startView: 2,
       forceParse: 0,
       showMeridian: 1
   });
});
$(document).ready(function () {
$('.tanggal_berangkat').datetimepicker({
      format: 'yyyy-mm-dd hh:ii:ss',
       weekStart: 1,
       todayBtn:  1,
       autoclose: 1,
       todayHighlight: 1,
       startView: 2,
       forceParse: 0,
       showMeridian: 1
   });
});
$(document).ready(function () {
$('.tanggal_sampai').datetimepicker({
      format: 'yyyy-mm-dd hh:ii:ss',
       weekStart: 1,
       todayBtn:  1,
       autoclose: 1,
       todayHighlight: 1,
       startView: 2,
       forceParse: 0,
       showMeridian: 1
   });
});
</script>

<script type="text/javascript">
function PRINT() {
    win = window.open('transdata_p2tl/print.php', 'win', 'width=1000, height=400, menubar=0, scrollbars=1, resizable=0, location=0, toolbar=0, status_pelaksanaan=0');
}
</script>
<script language="JavaScript">
function buka(url) {
    window.open(url, 'window_baru', 'width=800,height=600,left=320,top=100,resizable=1,scrollbars=1');
}
</script>

<?php
$sql = "select `id_transdata_p2tl` from `$tbtransdata_p2tl` order by `id_transdata_p2tl` desc";
$q = mysqli_query($conn, $sql);
$jum = mysqli_num_rows($q);
$th = date("y");
$bl = date("m") + 0;
if ($bl < 10) {
    $bl = "0" . $bl;
}

$kd = "TDP" . $th . $bl; //KEG1610001
if ($jum > 0) {
    $d = mysqli_fetch_array($q);
    $idmax = $d["id_transdata_p2tl"];

    $bul = substr($idmax, 5, 2);
    $tah = substr($idmax, 3, 2);
    if ($bul == $bl && $tah == $th) {
        $urut = substr($idmax, 7, 3) + 1;
        if ($urut < 10) {
            $idmax = "$kd" . "00" . $urut;
        } else if ($urut < 100) {
            $idmax = "$kd" . "0" . $urut;
        } else {
            $idmax = "$kd" . $urut;
        }
    }//==
    else {
        $idmax = "$kd" . "001";
    }
}//jum>0
else {
    $idmax = "$kd" . "001";
}
$id_transdata_p2tl = $idmax;
?>

<?php
if ($_GET["pro"] == "ubah") {
    $id_transdata_p2tl = $_GET["kode"];
    $sql = "select * from `$tbtransdata_p2tl` where `id_transdata_p2tl`='$id_transdata_p2tl'";
    $d = getField($conn, $sql);
    $id_transdata_p2tl = $d["id_transdata_p2tl"];
    $id_transdata_p2tl0 = $d["id_transdata_p2tl"];
    $nomor_targetoperasi = $d["nomor_targetoperasi"];
    $tanggal_targetoperasi = $d["tanggal_targetoperasi"];
    $id_user = $d["id_user"];
    $id_pelanggan = $d["id_pelanggan"];
    $id_vendor = $d["id_vendor"];
    $nomor_workorder = ($d["nomor_workorder"]);
    $tanggal_workorder = ($d["tanggal_workorder"]);
    $tanggal_respon_workorder = $d["tanggal_respon_workorder"];
    $petugas_penerima = ($d["petugas_penerima"]);
    $latitude = ($d["latitude"]);
    $longitude = ($d["longitude"]);
    $foto_1 = ($d["foto_1"]);
    $foto_10 = ($d["foto_1"]);
    $foto_2 = ($d["foto_2"]);
    $foto_20 = ($d["foto_2"]);
    $foto_3 = ($d["foto_3"]);
    $foto_30 = ($d["foto_3"]);
    $foto_4 = ($d["foto_4"]);
    $foto_40 = ($d["foto_4"]);
    $foto_5 = ($d["foto_5"]);
    $foto_50 = ($d["foto_5"]);
    $foto_6 = ($d["foto_6"]);
    $foto_60 = ($d["foto_6"]);
    $petugas_lapangan = ($d["petugas_lapangan"]);
    $cek_pelanggaran = ($d["cek_pelanggaran"]);
    $kode_pelanggaran = ($d["kode_pelanggaran"]);
    $tanggal_berangkat = ($d["tanggal_berangkat"]);
    $tanggal_sampai = ($d["tanggal_sampai"]);
    $tanggal_penertiban = ($d["tanggal_penertiban"]);
    $status_pelaksanaan = $d["status_pelaksanaan"];
    $pro = "ubah";
}
?>

<div class="panel-group" id="accordion">
<div class="panel box box-success panel-default">
  <div class="panel-heading">
    <h4 class="panel-title">
      <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
      Input Vendor</a>
    </h4>
  </div>
  <div id="collapse1" class="panel-collapse collapse in">
    <div class="panel-body">

<form action="" method="post" enctype="multipart/form-data">
    <div class="box-body row">
        <div class="form-group col-sm-3" >
    <center>
            <?php
            echo"<a href='#' onclick='buka(\"transdata_p2tl/zoom.php?id=$id_transdata_p2tl\")'>
    <img src='$YPATH/$foto_10' width='77' height='80' />
    </a>
    ";
            echo"<a href='#' onclick='buka(\"transdata_p2tl/zoom2.php?id=$id_transdata_p2tl\")'>
        <img src='$YPATH/$foto_20' width='77' height='80' />
        </a>
        ";
            echo"<a href='#' onclick='buka(\"transdata_p2tl/zoom3.php?id=$id_transdata_p2tl\")'>
        <img src='$YPATH/$foto_30' width='77' height='80' />
        </a>
        ";
            echo"<a href='#' onclick='buka(\"transdata_p2tl/zoom4.php?id=$id_transdata_p2tl\")'>
        <img src='$YPATH/$foto_40' width='77' height='80' />
        </a>
        ";
            echo"<a href='#' onclick='buka(\"transdata_p2tl/zoom5.php?id=$id_transdata_p2tl\")'>
        <img src='$YPATH/$foto_50' width='77' height='80' />
        </a>
        ";
                echo"<a href='#' onclick='buka(\"transdata_p2tl/zoom6.php?id=$id_transdata_p2tl\")'>
        <img src='$YPATH/$foto_60' width='77' height='80' />
        </a>
        ";
            ?>
    </center>
        </div>
        <div class="form-group col-sm-1" >
        </div>
        <div class="form-group col-sm-4" >

            <div class="form-group">
                <label for="id_transdata_p2tl">ID Transaksi Data</label>
                <input disabled="disabled" class="form-control" value="<?php echo $id_transdata_p2tl; ?>"/>
            </div>
            <div class="form-group">
                <label for="nomor_targetoperasi">Nomor Target Operasi</label>
                <input class="form-control" name="nomor_targetoperasi" required="required" type="text" id="nomor_targetoperasi" value="<?php echo $nomor_targetoperasi; ?>" size="30" />
            </div>
            <div class="form-group">
                <label for="tanggal_targetoperasi">Tanggal Target Operasi</label>
                <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input class="form-control date tanggal_workorder" data-date-format="yyyy-mm-dd HH:ii:ss" name="tanggal_targetoperasi" required="required" type="text" id="tanggal_targetoperasi" value="<?php echo $tanggal_targetoperasi; ?>" />
                </div>
            </div>
            <div class="form-group">
                <label for="id_user">Nama PIC</label>
                <select class="form-control" name="id_user" id="id_user">
                  <?php
                            $sql = "select id_user,nama_user from `$tbmasterpetugas`";// where `level_user`='PIC'";
                            $arr = getData($conn, $sql);
                            foreach ($arr as $d) {
                                $id_user0 = $d["id_user"];
                                $nama_user = $d["nama_user"];
                                echo"<option value='$id_user0' ";
                                if ($id_user0 == $id_user) {
                                    echo"selected";
                                }echo">$nama_user</option>";
                            }
                            ?>
                </select>
            </div>
            <div class="form-group">
                <label for="id_pelanggan">ID Pelanggan</label>
                <select class="form-control" name="id_pelanggan" id="id_pelanggan">
                    <?php
                    $sql = "select id_pelanggan,nama_pelanggan from `$tbpelanggan`";//" where `level_user`='PIC'";
                    $arr = getData($conn, $sql);
                    foreach ($arr as $d) {
                        $id_pelanggan0 = $d["id_pelanggan"];
                        $nama_pelanggan = $d["nama_pelanggan"];
                        echo"<option value='$id_pelanggan0' ";
                        if ($id_pelanggan0 == $id_pelanggan) {
                            echo"selected";
                        }echo">$id_pelanggan0</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="id_vendor">Nama Vendor</label>
                <select class="form-control" name="id_vendor" id="id_vendor">
                    <?php
                    $sql = "select id_vendor, nama_vendor from `$tbmastervendor`"; // where `level`='AP2T'";
                    $arr = getData($conn, $sql);
                    foreach ($arr as $d) {
                        $id_vendor0 = $d["id_vendor"];
                        $nama_vendor = $d["nama_vendor"];
                        echo"<option value='$id_vendor0' ";
                        if ($id_vendor0 == $id_vendor) {
                            echo"selected";
                        }echo">$nama_vendor</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="nomor_workorder">Nomor Work Order</label>
                <input class="form-control" name="nomor_workorder" required="required" type="text" id="nomor_workorder" value="<?php echo $nomor_workorder; ?>" size="30" />
            </div>
            <div class="form-group">
                <label for="tanggal_workorder">Tanggal Work Order</label>
                <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input class="form-control date tanggal_workorder" data-date-format="yyyy-mm-dd HH:ii:ss" name="tanggal_workorder" required="required" type="text" id="tanggal_workorder" value="<?php echo $tanggal_workorder; ?>" />
                </div>
            </div>
            <div class="form-group">
                <label for="tanggal_respon_workorder">Tanggal Respon Work Order</label>
                <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input class="form-control date tanggal_workorder" data-date-format="yyyy-mm-dd HH:ii:ss" name="tanggal_respon_workorder" required="required" type="text" id="tanggal_respon_workorder" value="<?php echo $tanggal_respon_workorder; ?>" />
                </div>
            </div>
            <div class="form-group">
                <label for="petugas_penerima">Petugas Penerima</label>
                <select class="form-control" name="petugas_penerima" id="petugas_penerima">
                    <?php
                              $sql = "select id_user,nama_user from `$tbmasterpetugas` where `level_user`='PL'";
                              $arr = getData($conn, $sql);
                              foreach ($arr as $d) {
                                  $id_user0 = $d["id_user"];
                                  $nama_user = $d["nama_user"];
                                  echo"<option value='$id_user0' ";
                                  if ($id_user0 == $id_user) {
                                      echo"selected";
                                  }echo">$nama_user</option>";
                              }
                              ?>
                </select>
            </div>
            <div class="form-group">
                <label for="latitude">Latitude</label>
                <input class="form-control" name="latitude" required="required" type="text" id="latitude" value="<?php echo $latitude; ?>" size="30" />
            </div>
            <div class="form-group">
                <label for="longitude">Longitude</label>
                <input class="form-control" name="longitude" required="required" type="text" id="longitude" value="<?php echo $longitude; ?>" size="30" />
            </div>
        </div>
        <div class="form-group col-sm-4" >
            <div class="form-group">
                <label for="foto_1">Foto 1</label>
                <input class="form-control" name="foto_1" type="file" id="foto_1" size="30" />
                <i class="fa fa-image"></i> <a href='#' onclick='buka("transdata_p2tl/zoom.php?id=<?php echo $id_transdata_p2tl; ?>")'><?php echo $foto_10; ?></a>
            </div>
            <div class="form-group">
                <label for="foto_1">Foto 2</label>
                <input class="form-control" name="foto_2" type="file" id="foto_2" size="30" />
                <i class="fa fa-image"></i> <a href='#' onclick='buka("transdata_p2tl/zoom2.php?id=<?php echo $id_transdata_p2tl; ?>")'><?php echo $foto_20; ?></a>
            </div>
            <div class="form-group">
                <label for="foto_3">Foto 3</label>
                <input class="form-control" name="foto_3"  type="file" id="foto_3" size="30" />
                <i class="fa fa-image"></i> <a href='#' onclick='buka("transdata_p2tl/zoom3.php?id=<?php echo $id_transdata_p2tl; ?>")'><?php echo $foto_30; ?></a>
            </div>
            <div class="form-group">
                <label for="foto_4">Foto 4</label>
                <input class="form-control" name="foto_4" type="file" id="foto_4" size="30" />
                <i class="fa fa-image"></i> <a href='#' onclick='buka("transdata_p2tl/zoom4.php?id=<?php echo $id_transdata_p2tl; ?>")'><?php echo $foto_40; ?></a>
            </div>
            <div class="form-group">
                <label for="foto_5">Foto 5</label>
                <input class="form-control" name="foto_5" type="file" id="foto_5" size="30" />
                <i class="fa fa-image"></i> <a href='#' onclick='buka("transdata_p2tl/zoom5.php?id=<?php echo $id_transdata_p2tl; ?>")'><?php echo $foto_50; ?></a>
            </div>
            <div class="form-group">
                <label for="foto_6">Foto 6</label>
                <input class="form-control" name="foto_6" type="file" id="foto_6" size="30" />
                <i class="fa fa-image"></i> <a href='#' onclick='buka("transdata_p2tl/zoom6.php?id=<?php echo $id_transdata_p2tl; ?>")'><?php echo $foto_60; ?></a>
            </div>
            <div class="form-group">
                <label for="petugas_lapangan">Petugas Lapangan</label>
                <select class="form-control" name="petugas_lapangan" id="petugas_lapangan">
                    <?php
                              $sql = "select id_user,nama_user from `$tbmasterpetugas` where `level_user`='PL'";
                              $arr = getData($conn, $sql);
                              foreach ($arr as $d) {
                                  $id_user0 = $d["id_user"];
                                  $nama_user = $d["nama_user"];
                                  echo"<option value='$id_user0' ";
                                  if ($id_user0 == $id_user) {
                                      echo"selected";
                                  }echo">$nama_user</option>";
                              }
                              ?>
                </select>
            </div>
            <div class="form-group">
                <label for="cek_pelanggaran">Cek Pelanggaran</label>
                <select class="form-control" name="cek_pelanggaran" id="cek_pelanggaran">
                    <option value="">Silahkan Pilih</option>
                    <option value="ya">Ya</option>
                    <option value="tidak">Tidak</option>
                </select>
            </div>
            <div class="form-group">
                <label for="kode_pelanggaran">Kode Pelanggaran</label>
                <input class="form-control" name="kode_pelanggaran" required="required" type="text" id="kode_pelanggaran" value="<?php echo $kode_pelanggaran; ?>" size="30" />
            </div>
            <div class="form-group">
                <label for="tanggal_berangkat">Tanggal Berangkat</label>
                <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input class="form-control date tanggal_berangkat" data-date-format="yyyy-mm-dd HH:ii:ss" name="tanggal_berangkat" required="required" type="text" id="tanggal_berangkat" value="<?php echo $tanggal_berangkat; ?>" />
                </div>
            </div>
            <div class="form-group">
                <label for="tanggal_sampai">Tanggal Sampai</label>
                <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input class="form-control date tanggal_sampai" data-date-format="yyyy-mm-dd HH:ii:ss" name="tanggal_sampai" required="required" type="text" id="tanggal_sampai" value="<?php echo $tanggal_sampai; ?>" />
                </div>
            </div>
            <div class="form-group">
                <label for="tanggal_penertiban">Tanggal Penertiban</label>
                <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input class="form-control date tanggal_penertiban" data-date-format="yyyy-mm-dd HH:ii:ss" name="tanggal_penertiban" required="required" type="text" id="tanggal_penertiban" value="<?php echo $tanggal_penertiban; ?>" />
                </div>
            </div>
            <div class="form-group">
                <label for="status_pelaksanaan">Status Pelaksanaan</label>
                <select class="form-control" name="status_pelaksanaan" id="status_pelaksanaan">
                    <?php
                              $sql = "select id_status_pelaksanaan,nama_status from `$tbmasterstatus`";//where `level_user`='PL'";
                              $arr = getData($conn, $sql);
                              foreach ($arr as $d) {
                                  $id_status_pelaksanaan0 = $d["id_status_pelaksanaan"];
                                  $nama_status = $d["nama_status"];
                                  echo"<option value='$id_status_pelaksanaan0' ";
                                  if ($id_status_pelaksanaan0 == $id_status_pelaksanaan) {
                                      echo"selected";
                                  }echo">$nama_status</option>";
                              }
                              ?>
                </select>  </div>
            <div class="form-group" align="right">
                <button type="submit" name="Simpan"id="Simpan" class="btn btn-primary">Simpan</button>
                <input name="pro" type="hidden" id="pro" value="<?php echo $pro; ?>" />
                <input name="foto_10" type="hidden" id="foto_10" value="<?php echo $foto_10; ?>" />
                <input name="foto_20" type="hidden" id="foto_20" value="<?php echo $foto_20; ?>" />
                <input name="foto_30" type="hidden" id="foto_30" value="<?php echo $foto_30; ?>" />
                <input name="foto_40" type="hidden" id="foto_40" value="<?php echo $foto_40; ?>" />
                <input name="foto_50" type="hidden" id="foto_50" value="<?php echo $foto_50; ?>" />
                <input name="foto_60" type="hidden" id="foto_60" value="<?php echo $foto_60; ?>" />
                <input name="id_transdata_p2tl" type="hidden" id="id_transdata_p2tl" value="<?php echo $id_transdata_p2tl; ?>" />
                <input name="id_transdata_p2tl0" type="hidden" id="id_transdata_p2tl0" value="<?php echo $id_transdata_p2tl0; ?>" />
                <a href="?mnu=transdata_p2tl"><button type="button" name="Batal" id="Batal" class="btn btn-danger">Batal</button></a>
            </div>
        </div>
    </div>
</form>
</div>
    </div>
  </div>

  <?php
  $sqlq="select distinct(cek_pelanggaran) from `$tbtransdata_p2tl` order by `id_transdata_p2tl` desc";
  $jumq=getJum($conn,$sqlq);
        if($jumq <1){
            echo"<h1>Maaf data belum tersedia...</h1>";
            }
    $arrq=getData($conn,$sqlq);
        foreach($arrq as $dq) {
                $id_transdata_p2tl=$dq["id_transdata_p2tl"];
                        $cek_pelanggaran=$dq["cek_pelanggaran"];

?>

  <div class="panel box box-primary panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordions" href="#collapse2<?php echo $cek_pelanggaran ?>">
        Data Pelanggaran <?php echo $cek_pelanggaran ?></a>
      </h4>
    </div>
    <div id="collapse2<?php echo $cek_pelanggaran ?>" class="panel-collapse collapse">
      <div class="panel-body">
Data transdata_p2tl:
| <a href="transdata_p2tl/pdf.php"><img src='ypathicon/pdf.png' alt='PDF'></a>
| <a href="transdata_p2tl/xls.php"><img src='ypathicon/xls.png' alt='XLS'></a>
| <a href="transdata_p2tl/xml.php"><img src='ypathicon/xml.png' alt='XML'></a>
| <img src='ypathicon/print.png' alt='PRINT' OnClick="PRINT()"> |
<br>
<div class="row">
    <div class="table-responsive">
        <table id="tabeltransdata_p2tl" class="table table-bordered table-striped dataTable" width="100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Kode</th>
                    <th>Nomor TO</th>
                    <th>Tanggal TO</th>
                    <th>Vendor</th>
                    <th>Nama PIC</th>
                    <th>Nomor WO</th>
                    <th>Tanggal WO</th>
                    <th>Tanggal Respon WO</th>
                    <th>ID Pelanggan</th>
                    <th>Nama Pelanggan</th>
                    <th>Alamat Pelanggan</th>
                    <th>Tarif/Daya</th>
                    <th>Petugas Penerima</th>
                    <th>Latitude</th>
                    <th>Longitude</th>
                    <th>Foto 1</th>
                    <th>Foto 2</th>
                    <th>Foto 3</th>
                    <th>Foto 4</th>
                    <th>Foto 5</th>
                    <th>Foto 6</th>
                    <th>Petugas Lapangan</th>
                    <th>Cek Pelanggaran</th>
                    <th>Kode Pelanggaran</th>
                    <th>Tanggal Berangkat</th>
                    <th>Tanggal Sampai</th>
                    <th>Tanggal Penertiban</th>
                    <th>Status Pelaksanaan</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <?php
            $sql = "select * from `$tbtransdata_p2tl` a LEFT JOIN `$tbpelanggan` b ON a.`id_pelanggan`=b.`id_pelanggan` where cek_pelanggaran='$cek_pelanggaran' order by `id_transdata_p2tl` desc";
            $jum = getJum($conn, $sql);
            if ($jum > 0) {
                //--------------------------------------------------------------------------------------------
                $batas = 10;
                $page = $_GET['page'];
                if (empty($page)) {
                    $posawal = 0;
                    $page = 1;
                } else {
                    $posawal = ($page - 1) * $batas;
                }

                $sql2 = $sql . " LIMIT $posawal,$batas";
                $no = $posawal + 1;
                //--------------------------------------------------------------------------------------------
                $arr = getData($conn, $sql2);
                foreach ($arr as $d) {
                    $id_transdata_p2tl = $d["id_transdata_p2tl"];
                    $nomor_targetoperasi = $d["nomor_targetoperasi"];
                    $tanggal_targetoperasi = ($d["tanggal_targetoperasi"]);
                    $id_user = getUser($conn, $d["id_user"]);
                    $id_pelanggan = $d["id_pelanggan"];
                     $nama_pelanggan = $d["nama_pelanggan"];
                      $alamat_pelanggan = $d["alamat_pelanggan"];
                       $tarif = $d["tarif"];
                        $daya = $d["daya"];
                    $id_vendor = getVendor($conn, $d["id_vendor"]);
                    $nomor_workorder = $d["nomor_workorder"];
                    $tanggal_workorder = ($d["tanggal_workorder"]);
                    $tanggal_respon_workorder = ($d["tanggal_respon_workorder"]);
                    $petugas_penerima = getUser($conn, $d["petugas_penerima"]);
                    $latitude = $d["latitude"];
                    $longitude = $d["longitude"];
                    $foto_1 = $d["foto_1"];
                    $foto_10 = ($d["foto_1"]);
                    $foto_2 = ($d["foto_2"]);
                    $foto_20 = ($d["foto_2"]);
                    $foto_3 = ($d["foto_3"]);
                    $foto_30 = ($d["foto_3"]);
                    $foto_4 = ($d["foto_4"]);
                    $foto_40 = ($d["foto_4"]);
                    $foto_5 = ($d["foto_5"]);
                    $foto_50 = ($d["foto_5"]);
                    $foto_6 = ($d["foto_6"]);
                    $foto_60 = ($d["foto_6"]);
                    $petugas_lapangan = getUser($conn, $d["petugas_lapangan"]);
                    $cek_pelanggaran = ($d["cek_pelanggaran"]);
                    $kode_pelanggaran = ($d["kode_pelanggaran"]);
                    $tanggal_berangkat = ($d["tanggal_berangkat"]);
                    $tanggal_sampai = ($d["tanggal_sampai"]);
                    $tanggal_penertiban = ($d["tanggal_penertiban"]);
                    $status_pelaksanaan = getStatus($conn, $d["status_pelaksanaan"]);
                    $color = "#dddddd";
                    if ($no % 2 == 0) {
                        $color = "#eeeeee";
                    }
                    echo"<tr bgcolor='$color'>
                    <td>$no</td>
                    <td>$id_transdata_p2tl</td>
                    <td>$nomor_targetoperasi</td>
                    <td>$tanggal_targetoperasi</td>
                    <td>$id_vendor</td>
                    <td>$id_user</td>
                    <td>$nomor_workorder</td>
                    <td>$tanggal_workorder</td>
                    <td>$tanggal_respon_workorder</td>
                    <td>$id_pelanggan</td>
                    <td>$nama_pelanggan</td>
                    <td>$alamat_pelanggan</td>
                    <td>$tarif / $daya</td>
                    <td>$petugas_penerima</td>
                    <td>$latitude</td>
                    <td>$longitude</td>
                    <td><div align='center'>";
                echo"<a href='#' onclick='buka(\"transdata_p2tl/zoom.php?id=$id_transdata_p2tl\")'>
    <img src='data:image/jpg;base64,$foto_1' width='40' height='40' /></a></div>";
                echo"</td>
                    <td><div align='center'>";
                echo"<a href='#' onclick='buka(\"transdata_p2tl/zoom2.php?id=$id_transdata_p2tl\")'>
    <img src='data:image/jpg;base64,$foto_2' width='40' height='40' /></a></div>";
                echo"</td>
                <td><div align='center'>";
                echo"<a href='#' onclick='buka(\"transdata_p2tl/zoom3.php?id=$id_transdata_p2tl\")'>
    <img src='data:image/jpg;base64,$foto_3' width='40' height='40' /></a></div>";
                echo"</td>
                <td><div align='center'>";
            echo"<a href='#' onclick='buka(\"transdata_p2tl/zoom4.php?id=$id_transdata_p2tl\")'>
    <img src='data:image/jpg;base64,$foto_4' width='40' height='40' /></a></div>";
            echo"</td>
                <td><div align='center'>";
            echo"<a href='#' onclick='buka(\"transdata_p2tl/zoom5.php?id=$id_transdata_p2tl\")'>
    <img src='data:image/jpg;base64,$foto_5' width='40' height='40' /></a></div>";
            echo"</td>
            <td><div align='center'>";
        echo"<a href='#' onclick='buka(\"transdata_p2tl/zoom6.php?id=$id_transdata_p2tl\")'>
    <img src='data:image/jpg;base64,$foto_6' width='40' height='40' /></a></div>";
   //   <img src='$YPATH/$foto_6' width='40' height='40' /></a></div>";
        echo"</td>
                    <td>$petugas_lapangan</td>
                    <td>$cek_pelanggaran</td>
                    <td>$kode_pelanggaran</td>
                    <td>$tanggal_berangkat</td>
                    <td>$tanggal_sampai</td>
                    <td>$tanggal_penertiban</td>
                    <td>$status_pelaksanaan</td>
                    <td><div align='center'>
                    <a href='?mnu=transdata_p2tl&pro=ubah&kode=$id_transdata_p2tl'><img src='ypathicon/u.png' alt='ubah'></a>
                    <a href='?mnu=transdata_p2tl&pro=hapus&kode=$id_transdata_p2tl'><img src='ypathicon/h.png' alt='hapus'
                    onClick='return confirm(\"Apakah Anda benar-benar akan menghapus $nomor_targetoperasi pada data transdata_p2tl ?..\")'></a></div></td>
                    </tr>";

                    $no++;
                }//while
            }//if
            else {
                echo"<tr><td colspan='7'><blink>Maaf, Data transdata_p2tl belum tersedia...</blink></td></tr>";
            }
            ?>
        </table>
    </div>
</div>
<?php
//Langkah 3: Hitung total data dan page
$jmldata = $jum;
if ($jmldata > 0) {
    if ($batas < 1) {
        $batas = 1;
    }
    $jmlhal = ceil($jmldata / $batas);
    echo "<div class=paging>";
    if ($page > 1) {
        $prev = $page - 1;
        echo "<span class=prevnext><a href='$_SERVER[PHP_SELF]?page=$prev&mnu=transdata_p2tl'>« Prev</a></span> ";
    } else {
        echo "<span class=disabled>« Prev</span> ";
    }

    // Tampilkan link page 1,2,3 ...
    for ($i = 1; $i <= $jmlhal; $i++)
    if ($i != $page) {
        echo "<a href='$_SERVER[PHP_SELF]?page=$i&mnu=transdata_p2tl'>$i</a> ";
    } else {
        echo " <span class=current>$i</span> ";
    }

    // Link kepage berikutnya (Next)
    if ($page < $jmlhal) {
        $next = $page + 1;
        echo "<span class=prevnext><a href='$_SERVER[PHP_SELF]?page=$next&mnu=transdata_p2tl'>Next »</a></span>";
    } else {
        echo "<span class=disabled>Next »</span>";
    }
    echo "</div>";
}//if jmldata

$jmldata = $jum;
echo "<p align=center>Total Data <b>$jmldata</b> Item</p>";
?>
</div>
    </div>
  </div>
  <?php } ?>
</div>
<?php
if (isset($_POST["Simpan"])) {
    $pro = strip_tags($_POST["pro"]);
    $id_transdata_p2tl = strip_tags($_POST["id_transdata_p2tl"]);
    $id_transdata_p2tl0 = strip_tags($_POST["id_transdata_p2tl0"]);
    $nomor_targetoperasi = strip_tags($_POST["nomor_targetoperasi"]);
    $tanggal_targetoperasi = strip_tags($_POST["tanggal_targetoperasi"]);
    $id_user = strip_tags($_POST["id_user"]);
    $id_pelanggan = strip_tags($_POST["id_pelanggan"]);
    $id_vendor = strip_tags($_POST["id_vendor"]);
    $nomor_workorder = strip_tags($_POST["nomor_workorder"]);
    $tanggal_workorder = (strip_tags($_POST["tanggal_workorder"]));
    $tanggal_respon_workorder = strip_tags($_POST["tanggal_respon_workorder"]);
    $petugas_penerima = strip_tags($_POST["petugas_penerima"]);
    $latitude = strip_tags($_POST["latitude"]);
    $longitude = strip_tags($_POST["longitude"]);

    $foto_10 = strip_tags($_POST["foto_10"]);
    if ($_FILES["foto_1"] != "") {
        @copy($_FILES["foto_1"]["tmp_name"], "$YPATH/" . $_FILES["foto_1"]["name"]);
        $foto_1 = $_FILES["foto_1"]["name"];
    } else {
        $foto_1 = $foto_10;
    }
    if (strlen($foto_1) < 1) {
        $foto_1 = $foto_10;
    }

    $foto_20 = strip_tags($_POST["foto_20"]);
    if ($_FILES["foto_2"] != "") {
    @copy($_FILES["foto_2"]["tmp_name"], "$YPATH/" . $_FILES["foto_2"]["name"]);
    $foto_2 = $_FILES["foto_2"]["name"];
    } else {
    $foto_2 = $foto_20;
    }
    if (strlen($foto_2) < 1) {
    $foto_2 = $foto_20;
    }

        $foto_30 = strip_tags($_POST["foto_30"]);
    if ($_FILES["foto_3"] != "") {
        @copy($_FILES["foto_3"]["tmp_name"], "$YPATH/" . $_FILES["foto_3"]["name"]);
        $foto_3 = $_FILES["foto_3"]["name"];
    } else {
        $foto_3 = $foto_30;
    }
    if (strlen($foto_3) < 1) {
        $foto_3 = $foto_30;
    }

        $foto_40 = strip_tags($_POST["foto_40"]);
    if ($_FILES["foto_4"] != "") {
        @copy($_FILES["foto_4"]["tmp_name"], "$YPATH/" . $_FILES["foto_4"]["name"]);
        $foto_4 = $_FILES["foto_4"]["name"];
    } else {
        $foto_4 = $foto_40;
    }
    if (strlen($foto_4) < 1) {
        $foto_4 = $foto_40;
    }

        $foto_50 = strip_tags($_POST["foto_50"]);
    if ($_FILES["foto_5"] != "") {
        @copy($_FILES["foto_5"]["tmp_name"], "$YPATH/" . $_FILES["foto_5"]["name"]);
        $foto_5 = $_FILES["foto_5"]["name"];
    } else {
        $foto_5 = $foto_50;
    }
    if (strlen($foto_5) < 1) {
        $foto_5 = $foto_50;
    }

        $foto_60 = strip_tags($_POST["foto_60"]);
    if ($_FILES["foto_6"] != "") {
        @copy($_FILES["foto_6"]["tmp_name"], "$YPATH/" . $_FILES["foto_6"]["name"]);
        $foto_6 = $_FILES["foto_6"]["name"];
    } else {
        $foto_6 = $foto_60;
    }
    if (strlen($foto_6) < 1) {
        $foto_6 = $foto_60;
    }

    $petugas_lapangan = strip_tags($_POST["petugas_lapangan"]);
    $cek_pelanggaran = strip_tags($_POST["cek_pelanggaran"]);
    $kode_pelanggaran = strip_tags($_POST["kode_pelanggaran"]);
    $tanggal_berangkat = strip_tags($_POST["tanggal_berangkat"]);
    $tanggal_sampai = strip_tags($_POST["tanggal_sampai"]);
    $tanggal_penertiban = strip_tags($_POST["tanggal_penertiban"]);
    $status_pelaksanaan = strip_tags($_POST["status_pelaksanaan"]);

    if ($pro == "simpan") {
        $sql = " INSERT INTO `$tbtransdata_p2tl` (
            `id_transdata_p2tl` ,
            `nomor_targetoperasi` ,
            `tanggal_targetoperasi` ,
            `id_user` ,
            `id_pelanggan` ,
            `id_vendor` ,
            `nomor_workorder` ,
            `tanggal_workorder` ,
            `tanggal_respon_workorder` ,
            `petugas_penerima` ,
            `latitude` ,
            `longitude` ,
            `foto_1` ,
            `foto_2` ,
            `foto_3` ,
            `foto_4` ,
            `foto_5` ,
            `foto_6` ,
            `petugas_lapangan` ,
            `cek_pelanggaran` ,
            `kode_pelanggaran` ,
            `tanggal_berangkat` ,
            `tanggal_sampai` ,
            `tanggal_penertiban` ,
            `status_pelaksanaan`
        ) VALUES (
            '$id_transdata_p2tl',
            '$nomor_targetoperasi',
            '$tanggal_targetoperasi',
            '$id_user',
            '$id_pelanggan',
            '$id_vendor',
            '$nomor_workorder',
            '$tanggal_workorder',
            '$tanggal_respon_workorder',
            '$petugas_penerima',
            '$latitude',
            '$longitude',
            '$foto_1',
            '$foto_2',
            '$foto_3',
            '$foto_4',
            '$foto_5',
            '$foto_6',
            '$petugas_lapangan',
            '$cek_pelanggaran',
            '$kode_pelanggaran',
            '$tanggal_berangkat',
            '$tanggal_sampai',
            '$tanggal_penertiban',
            '$status_pelaksanaan'
        )";

        $simpan = process($conn, $sql);
        if ($simpan) {
            echo "<script>alert('Data $id_transdata_p2tl berhasil disimpan !');document.location.href='?mnu=transdata_p2tl';</script>";
        } else {
            echo"<script>alert('Data $id_transdata_p2tl gagal disimpan...');document.location.href='?mnu=transdata_p2tl';</script>";
        }
    } else {
        $sql = "update `$tbtransdata_p2tl` set
        `nomor_targetoperasi`='$nomor_targetoperasi',
        `tanggal_targetoperasi`='$tanggal_targetoperasi' ,
        `id_user`='$id_user',
        `id_pelanggan`='$id_pelanggan',
        `id_vendor`='$id_vendor',
        `nomor_workorder`='$nomor_workorder',
        `tanggal_workorder`='$tanggal_workorder',
        `tanggal_respon_workorder`='$tanggal_respon_workorder',
        `petugas_penerima`='$petugas_penerima',
        `latitude`='$latitude',
        `longitude`='$longitude',
        `foto_1`='$foto_1',
        `foto_2`='$foto_2',
        `foto_3`='$foto_3',
        `foto_4`='$foto_4',
        `foto_5`='$foto_5',
        `foto_6`='$foto_6',
        `petugas_lapangan`='$petugas_lapangan',
        `cek_pelanggaran`='$cek_pelanggaran',
        `kode_pelanggaran`='$kode_pelanggaran',
        `tanggal_berangkat`='$tanggal_berangkat',
        `tanggal_sampai`='$tanggal_sampai',
        `tanggal_penertiban`='$tanggal_penertiban',
        `status_pelaksanaan`='$status_pelaksanaan'
        where `id_transdata_p2tl`='$id_transdata_p2tl0'";
        $ubah = process($conn, $sql);
        if ($ubah) {
            echo "<script>alert('Data $id_transdata_p2tl berhasil diubah !');document.location.href='?mnu=transdata_p2tl';</script>";
        } else {
            echo"<script>alert('Data $id_transdata_p2tl gagal diubah...');document.location.href='?mnu=transdata_p2tl';</script>";
        }
    }//else simpan
}
?>

<?php
if ($_GET["pro"] == "hapus") {
    $id_transdata_p2tl = $_GET["kode"];
    $sql = "delete from `$tbtransdata_p2tl` where `id_transdata_p2tl`='$id_transdata_p2tl'";
    $hapus = process($conn, $sql);
    if ($hapus) {
        echo "<script>alert('Data to $id_transdata_p2tl berhasil dihapus !');document.location.href='?mnu=transdata_p2tl';</script>";
    } else {
        echo"<script>alert('Data to $id_transdata_p2tl gagal dihapus...');document.location.href='?mnu=transdata_p2tl';</script>";
    }
}
?>
