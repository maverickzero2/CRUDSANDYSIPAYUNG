<?php
header("Content-type: text/xml");

include "../konmysqli.php";
$sql = "select * from `$tbtransdata_p2tl`";
if(getJum($conn,$sql)>0){
	print "<to>\n";
		$arr=getData($conn,$sql);
		foreach($arr as $d) {
				$id_transdata_p2tl=$d["id_transdata_p2tl"];
				$nomor_targetoperasi=$d["nomor_targetoperasi"];
				$tanggal_targetoperasi=$d["tanggal_targetoperasi"];
				$id_user=$d["id_user"];
				$id_pelanggan=$d["id_pelanggan"];
				$nama_pelanggan=$d["nama_pelanggan"];
				$alamat_pelanggan=$d["alamat_pelanggan"];
				$tarif=$d["tarif"];
				$daya=$d["daya"];
				$id_vendor=$d["id_vendor"];
				$nomor_workorder=$d["nomor_workorder"];
				$tanggal_workorder=$d["tanggal_workorder"];
		    $tanggal_respon_workorder=$d["tanggal_respon_workorder"];
				$petugas_penerima=$d["petugas_penerima"];
				$latitude = $d["latitude"];
				$longitude = $d["longitude"];
        $foto_1 = $d["foto_1"];
        $foto_2 = ($d["foto_2"]);
        $foto_3 = ($d["foto_3"]);
        $foto_4 = ($d["foto_4"]);
        $foto_5 = ($d["foto_5"]);
        $foto_6 = ($d["foto_6"]);
        $petugas_lapangan = ($d["petugas_lapangan"]);
        $cek_pelanggaran = ($d["cek_pelanggaran"]);
        $kode_pelanggaran = ($d["kode_pelanggaran"]);
        $tanggal_berangkat = $d["tanggal_berangkat"];
				$tanggal_sampai = $d["tanggal_sampai"];
				$tanggal_penertiban = $d["tanggal_penertiban"];
				$status_pelaksanaan=$d["status_pelaksanaan"];

				print "<record>\n";
				print "  <nomor_targetoperasi>$nomor_targetoperasi</nomor_targetoperasi>\n";
				print "  <tanggal_targetoperasi>$tanggal_targetoperasi</tanggal_targetoperasi>\n";
				print "  <id_user>$id_user</id_user>\n";
				print "  <id_pelanggan>$id_pelanggan</id_pelanggan>\n";
				print "  <nama_pelanggan>$inama_pelanggan</nama_pelanggan>\n";
				print "  <alamat_pelanggan>$alamat_pelanggan</alamat_pelanggan>\n";
				print "  <tarif>$tarif</tarif>\n";
				print "  <daya>$daya</daya>\n";
				print "  <id_vendor>$id_vendor</id_vendor>\n";
				print "  <nomor_workorder>$nomor_workorder</nomor_workorder>\n";
				print "  <tanggal_workorder>$tanggal_workorder</tanggal_workorder>\n";
				print "  <tanggal_respon_workorder>$tanggal_respon_workorder</tanggal_respon_workorder>\n";
				print "  <petugas_penerima>$petugas_penerima</petugas_penerima>\n";
				print "  <latitude>$latitude</latitude>\n";
				print "  <longitude>$longitude</longitude>\n";
				print "  <foto_1>$foto_1</foto_1>\n";
				print "  <foto_2>$foto_2</foto_2>\n";
				print "  <foto_3>$foto_3</foto_3>\n";
				print "  <foto_4>$foto_4</foto_4>\n";
				print "  <foto_5>$foto_5</foto_5>\n";
				print "  <foto_6>$foto_6</foto_6>\n";
				print "  <petugas_lapangan>$petugas_lapangan</petugas_lapangan>\n";
				print "  <cek_pelanggaran>$cek_pelanggaran</cek_pelanggaran>\n";
				print "  <kode_pelanggaran>$kode_pelanggaran</kode_pelanggaran>\n";
				print "  <tanggal_berangkat>$tanggal_berangkat</tanggal_berangkat>\n";
				print "  <tanggal_sampai>$tanggal_sampai</tanggal_sampai>\n";
				print "  <tanggal_penertiban>$tanggal_penertiban</tanggal_penertiban>\n";
				print "  <status_pelaksanaan>$status_pelaksanaan</status_pelaksanaan>\n";
				print "  <id_transdata_p2tl>$id_transdata_p2tl</id_transdata_p2tl>\n";
				print "</record>\n";
			}
	print "</to>\n";
}
else{
	$null="null";
	print "<to>\n";
		print "<record>\n";
				print "  <nomor_targetoperasi>$null</nomor_targetoperasi>\n";
				print "  <tanggal_targetoperasi>$null</tanggal_targetoperasi>\n";
				print "  <id_user>$null</id_user>\n";
				print "  <id_pelanggan>$null</id_pelanggan>\n";
				print "  <nama_pelanggan>$null</nama_pelanggan>\n";
				print "  <alamat_pelanggan>$null</alamat_pelanggan>\n";
				print "  <tarif>$null</tarif>\n";
				print "  <daya>$null</daya>\n";
				print "  <id_vendor>$null</id_vendor>\n";
				print "  <nomor_workorder>$null</nomor_workorder>\n";
				print "  <tanggal_workorder>$null</tanggal_workorder>\n";
				print "  <tanggal_respon_workorder>$null</tanggal_respon_workorder>\n";
				print "  <petugas_penerima>$null</petugas_penerima>\n";
				print "  <latitude>$null</latitude>\n";
				print "  <longitude>$null</longitude>\n";
				print "  <foto_1>$null</foto_1>\n";
				print "  <foto_2>$null</foto_2>\n";
				print "  <foto_3>$null</foto_3>\n";
				print "  <foto_4>$null</foto_4>\n";
				print "  <foto_5>$null</foto_5>\n";
				print "  <foto_6>$null</foto_6>\n";
				print "  <petugas_lapangan>$null</petugas_lapangan>\n";
				print "  <cek_pelanggaran>$null</cek_pelanggaran>\n";
				print "  <kode_pelanggaran>$null</kode_pelanggaran>\n";
				print "  <tanggal_berangkat>$null</tanggal_berangkat>\n";
				print "  <tanggal_sampai>$null</tanggal_sampai>\n";
				print "  <tanggal_penertiban>$null</tanggal_penertiban>\n";
				print "  <status_pelaksanaan>$null</status_pelaksanaan>\n";
				print "  <id_transdata_p2tl>$null</id_transdata_p2tl>\n";
		print "</record>\n";
	print "</to>\n";
	}
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

function getJum($conn,$sql){
  $rs=$conn->query($sql);
  $jum= $rs->num_rows;
	$rs->free();
	return $jum;
}

function getData($conn,$sql){
	$rs=$conn->query($sql);
	$rs->data_seek(0);
	$arr = $rs->fetch_all(MYSQLI_ASSOC);

	$rs->free();
	return $arr;
}
?>
