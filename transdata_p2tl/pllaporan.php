<?php
$pro = "simpan";
$tanggal_workorder = (date("Y-m-d H:i:s"));
$tanggal_targetoperasi = (date("Y-m-d H:i:s"));
$tanggal_respon_workorder = (date("Y-m-d H:i:s"));
$tanggal_penertiban = (date("Y-m-d H:i:s"));
$tanggal_berangkat = (date("Y-m-d H:i:s"));
$tanggal_sampai = (date("Y-m-d H:i:s"));
$foto_10 = "avatar.jpg";
$foto_20 = "avatar.jpg";
$foto_30 = "avatar.jpg";
$foto_40 = "avatar.jpg";
$foto_50 = "avatar.jpg";
$foto_60 = "avatar.jpg";
$pic_vendor=$_SESSION["cid"];

if(!isset($_SESSION["cid"])){
    die("<script>location.href='index.php'</script>");
}
?>
<link type="text/css" href="<?php echo "$PATH/base/"; ?>ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo "$PATH/"; ?>jquery-1.3.2.js"></script>
<script type="text/javascript" src="<?php echo "$PATH/"; ?>ui/ui.core.js"></script>
<script type="text/javascript" src="<?php echo "$PATH/"; ?>ui/ui.datepicker.js"></script>
<script type="text/javascript" src="<?php echo "$PATH/"; ?>ui/i18n/ui.datepicker-id.js"></script>

<script type="text/javascript">
$(document).ready(function () {
    $("#tanggal_workorder").datepicker({
        dateFormat: "Y-m-d",
        changeMonth: true,
        changeYear: true
    });
});
$(document).ready(function () {
    $("#tanggal_respon_workorder").datepicker({
        dateFormat: "Y-m-d",
        changeMonth: true,
        changeYear: true
    });
});
$(document).ready(function () {
    $("#tanggal_targetoperasi").datepicker({
        dateFormat: "Y-m-d",
        changeMonth: true,
        changeYear: true
    });
});
$(document).ready(function () {
    $("#tanggal_penertiban").datepicker({
        dateFormat: "Y-m-d",
        changeMonth: true,
        changeYear: true
    });
});
</script>

<script type="text/javascript">
function PRINT() {
    win = window.open('transdata_p2tl/print.php', 'win', 'width=1000, height=400, menubar=0, scrollbars=1, resizable=0, location=0, toolbar=0, status_pelaksanaan=0');
}
</script>
<script language="JavaScript">
function buka(url) {
    window.open(url, 'window_baru', 'width=800,height=600,left=320,top=100,resizable=1,scrollbars=1');
}
</script>

<?php
$sql = "select `id_transdata_p2tl` from `$tbtransdata_p2tl` order by `id_transdata_p2tl` desc";
$q = mysqli_query($conn, $sql);
$jum = mysqli_num_rows($q);
$th = date("y");
$bl = date("m") + 0;
if ($bl < 10) {
    $bl = "0" . $bl;
}

$kd = "TDP" . $th . $bl; //KEG1610001
if ($jum > 0) {
    $d = mysqli_fetch_array($q);
    $idmax = $d["id_transdata_p2tl"];

    $bul = substr($idmax, 5, 2);
    $tah = substr($idmax, 3, 2);
    if ($bul == $bl && $tah == $th) {
        $urut = substr($idmax, 7, 3) + 1;
        if ($urut < 10) {
            $idmax = "$kd" . "00" . $urut;
        } else if ($urut < 100) {
            $idmax = "$kd" . "0" . $urut;
        } else {
            $idmax = "$kd" . $urut;
        }
    }//==
    else {
        $idmax = "$kd" . "001";
    }
}//jum>0
else {
    $idmax = "$kd" . "001";
}
$id_transdata_p2tl = $idmax;
?>

<?php
if ($_GET["pro"] == "ubah") {
    $id_transdata_p2tl = $_GET["kode"];
    $sql = "select * from `$tbtransdata_p2tl` where `id_transdata_p2tl`='$id_transdata_p2tl'";
    $d = getField($conn, $sql);
    $id_transdata_p2tl = $d["id_transdata_p2tl"];
    $id_transdata_p2tl0 = $d["id_transdata_p2tl"];
    $nomor_targetoperasi = $d["nomor_targetoperasi"];
    $tanggal_targetoperasi = $d["tanggal_targetoperasi"];
    $id_user = $d["id_user"];
    $id_pelanggan = $d["id_pelanggan"];
    $id_vendor = $d["id_vendor"];
    $nomor_workorder = ($d["nomor_workorder"]);
    $tanggal_workorder = ($d["tanggal_workorder"]);
    $tanggal_respon_workorder = $d["tanggal_respon_workorder"];
    $petugas_penerima = ($d["petugas_penerima"]);
    $latitude = ($d["latitude"]);
    $longitude = ($d["longitude"]);
    $foto_1 = ($d["foto_1"]);
    $foto_10 = ($d["foto_1"]);
    $foto_2 = ($d["foto_2"]);
    $foto_20 = ($d["foto_2"]);
    $foto_3 = ($d["foto_3"]);
    $foto_30 = ($d["foto_3"]);
    $foto_4 = ($d["foto_4"]);
    $foto_40 = ($d["foto_4"]);
    $foto_5 = ($d["foto_5"]);
    $foto_50 = ($d["foto_5"]);
    $foto_6 = ($d["foto_6"]);
    $foto_60 = ($d["foto_6"]);
    $petugas_lapangan = ($d["petugas_lapangan"]);
    $cek_pelanggaran = ($d["cek_pelanggaran"]);
    $kode_pelanggaran = ($d["kode_pelanggaran"]);
    $tanggal_berangkat = ($d["tanggal_berangkat"]);
    $tanggal_sampai = ($d["tanggal_sampai"]);
    $tanggal_penertiban = ($d["tanggal_penertiban"]);
    $status_pelaksanaan = $d["status_pelaksanaan"];
    $pro = "ubah";
}
?>

<div class="panel-group" id="accordion">
<div class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title">
      <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
      Data Laporan</a>
    </h4>
  </div>
  <div id="collapse1" class="panel-collapse collapse in">
    <div class="panel-body">

Data transdata_p2tl:
| <a href="transdata_p2tl/pdf.php"><img src='ypathicon/pdf.png' alt='PDF'></a>
| <a href="transdata_p2tl/xls.php"><img src='ypathicon/xls.png' alt='XLS'></a>
| <a href="transdata_p2tl/xml.php"><img src='ypathicon/xml.png' alt='XML'></a>
| <img src='ypathicon/print.png' alt='PRINT' OnClick="PRINT()"> |
<br>
<div class="row">
    <div class="table-responsive">
        <table id="tabeltransdata_p2tl" class="table table-bordered table-striped dataTable">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Kode</th>
                    <th>Nomor TO</th>
                    <th>Tanggal TO</th>
                    <th>Vendor</th>
                    <th>Nama PIC</th>
                    <th>Nomor WO</th>
                    <th>Tanggal WO</th>
                    <th>Tanggal Respon WO</th>
                    <th>ID Pelanggan</th>
                    <th>Nama Pelanggan</th>
                    <th>Alamat Pelanggan</th>
                    <th>Tarif/Daya</th>
                    <th>Petugas Penerima</th>
                    <th>Latitude</th>
                    <th>Longitude</th>
                    <th>Foto 1</th>
                    <th>Foto 2</th>
                    <th>Foto 3</th>
                    <th>Foto 4</th>
                    <th>Foto 5</th>
                    <th>Foto 6</th>
                    <th>Petugas Lapangan</th>
                    <th>Cek Pelanggaran</th>
                    <th>Kode Pelanggaran</th>
                    <th>Tanggal Berangkat</th>
                    <th>Tanggal Sampai</th>
                    <th>Tanggal Penertiban</th>
                    <th>Status Pelaksanaan</th>

                </tr>
            </thead>
            <?php
            $sql = "select * from `$tbtransdata_p2tl` a LEFT JOIN `$tbpelanggan` b ON a.`id_pelanggan`=b.`id_pelanggan` order by `id_transdata_p2tl` desc";
            $jum = getJum($conn, $sql);
            if ($jum > 0) {
                //--------------------------------------------------------------------------------------------
                $batas = 10;
                $page = $_GET['page'];
                if (empty($page)) {
                    $posawal = 0;
                    $page = 1;
                } else {
                    $posawal = ($page - 1) * $batas;
                }

                $sql2 = $sql . " LIMIT $posawal,$batas";
                $no = $posawal + 1;
                //--------------------------------------------------------------------------------------------
                $arr = getData($conn, $sql2);
                foreach ($arr as $d) {
                    $id_transdata_p2tl = $d["id_transdata_p2tl"];
                    $nomor_targetoperasi = $d["nomor_targetoperasi"];
                    $tanggal_targetoperasi = ($d["tanggal_targetoperasi"]);
                    $id_user = getUser($conn, $d["id_user"]);
                    $id_pelanggan = $d["id_pelanggan"];
                    $nama_pelanggan = $d["nama_pelanggan"];
                    $alamat_pelanggan = $d["alamat_pelanggan"];
                    $tarif = $d["tarif"];
                    $daya = $d["daya"];
                    $id_vendor = getVendor($conn, $d["id_vendor"]);
                    $nomor_workorder = $d["nomor_workorder"];
                    $tanggal_workorder = ($d["tanggal_workorder"]);
                    $tanggal_respon_workorder = WKT($d["tanggal_respon_workorder"]);
                    $petugas_penerima = getUser($conn, $d["petugas_penerima"]);
                    $latitude = $d["latitude"];
                    $longitude = $d["longitude"];
                    $foto_1 = $d["foto_1"];
                    $foto_10 = ($d["foto_1"]);
                    $foto_2 = ($d["foto_2"]);
                    $foto_20 = ($d["foto_2"]);
                    $foto_3 = ($d["foto_3"]);
                    $foto_30 = ($d["foto_3"]);
                    $foto_4 = ($d["foto_4"]);
                    $foto_40 = ($d["foto_4"]);
                    $foto_5 = ($d["foto_5"]);
                    $foto_50 = ($d["foto_5"]);
                    $foto_6 = ($d["foto_6"]);
                    $foto_60 = ($d["foto_6"]);
                    $petugas_lapangan = getUser($conn, $d["petugas_lapangan"]);
                    $cek_pelanggaran = ($d["cek_pelanggaran"]);
                    $kode_pelanggaran = ($d["kode_pelanggaran"]);
                    $tanggal_berangkat = ($d["tanggal_berangkat"]);
                    $tanggal_sampai = ($d["tanggal_sampai"]);
                    $tanggal_penertiban = ($d["tanggal_penertiban"]);
                    $status_pelaksanaan = getStatus($conn, $d["status_pelaksanaan"]);
                    $color = "#dddddd";
                    if ($no % 2 == 0) {
                        $color = "#eeeeee";
                    }
                    echo"<tr bgcolor='$color'>
                    <td>$no</td>
                    <td>$id_transdata_p2tl</td>
                    <td>$nomor_targetoperasi</td>
                    <td>$tanggal_targetoperasi</td>
                    <td>$id_vendor</td>
                    <td>$id_user</td>
                    <td>$nomor_workorder</td>
                    <td>$tanggal_workorder</td>
                    <td>$tanggal_respon_workorder</td>
                    <td>$id_pelanggan</td>
                    <td>$nama_pelanggan</td>
                    <td>$alamat_pelanggan</td>
                    <td>$tarif / $daya</td>
                    <td>$petugas_penerima</td>
                    <td>$latitude</td>
                    <td>$longitude</td>
                    <td><div align='center'>";
                echo"<a href='#' onclick='buka(\"transdata_p2tl/zoom.php?id=$id_transdata_p2tl\")'>
    <img src='$YPATH/$foto_1' width='40' height='40' /></a></div>";
                echo"</td>
                    <td><div align='center'>";
                echo"<a href='#' onclick='buka(\"transdata_p2tl/zoom2.php?id=$id_transdata_p2tl\")'>
    <img src='$YPATH/$foto_2' width='40' height='40' /></a></div>";
                echo"</td>
                <td><div align='center'>";
                echo"<a href='#' onclick='buka(\"transdata_p2tl/zoom3.php?id=$id_transdata_p2tl\")'>
    <img src='$YPATH/$foto_3' width='40' height='40' /></a></div>";
                echo"</td>
                <td><div align='center'>";
            echo"<a href='#' onclick='buka(\"transdata_p2tl/zoom4.php?id=$id_transdata_p2tl\")'>
    <img src='$YPATH/$foto_4' width='40' height='40' /></a></div>";
            echo"</td>
                <td><div align='center'>";
            echo"<a href='#' onclick='buka(\"transdata_p2tl/zoom5.php?id=$id_transdata_p2tl\")'>
    <img src='$YPATH/$foto_5' width='40' height='40' /></a></div>";
            echo"</td>
            <td><div align='center'>";
        echo"<a href='#' onclick='buka(\"transdata_p2tl/zoom6.php?id=$id_transdata_p2tl\")'>
    <img src='$YPATH/$foto_6' width='40' height='40' /></a></div>";
        echo"</td>
                    <td>$petugas_lapangan</td>
                    <td>$cek_pelanggaran</td>
                    <td>$kode_pelanggaran</td>
                    <td>$tanggal_berangkat</td>
                    <td>$tanggal_sampai</td>
                    <td>$tanggal_penertiban</td>
                    <td>$status_pelaksanaan</td>
                    </tr>";

                    $no++;
                }//while
            }//if
            else {
                echo"<tr><td colspan='7'><blink>Maaf, Data transdata_p2tl belum tersedia...</blink></td></tr>";
            }
            ?>
        </table>
    </div>
</div>
</div></div></div></div>
<?php
if (isset($_POST["Simpan"])) {
    $pro = strip_tags($_POST["pro"]);
    $id_transdata_p2tl = strip_tags($_POST["id_transdata_p2tl"]);
    $id_transdata_p2tl0 = strip_tags($_POST["id_transdata_p2tl0"]);
    $nomor_targetoperasi = strip_tags($_POST["nomor_targetoperasi"]);
    $tanggal_targetoperasi = strip_tags($_POST["tanggal_targetoperasi"]);
    $id_user = strip_tags($_POST["id_user"]);
    $id_pelanggan = strip_tags($_POST["id_pelanggan"]);
    $id_vendor = strip_tags($_POST["id_vendor"]);
    $nomor_workorder = strip_tags($_POST["nomor_workorder"]);
    $tanggal_workorder = (strip_tags($_POST["tanggal_workorder"]));
    $tanggal_respon_workorder = strip_tags($_POST["tanggal_respon_workorder"]);
    $petugas_penerima = strip_tags($_POST["petugas_penerima"]);
    $latitude = strip_tags($_POST["latitude"]);
    $longitude = strip_tags($_POST["longitude"]);

    $foto_10 = strip_tags($_POST["foto_10"]);
    if ($_FILES["foto_1"] != "") {
        @copy($_FILES["foto_1"]["tmp_name"], "$YPATH/" . $_FILES["foto_1"]["name"]);
        $foto_1 = $_FILES["foto_1"]["name"];
    } else {
        $foto_1 = $foto_10;
    }
    if (strlen($foto_1) < 1) {
        $foto_1 = $foto_10;
    }

    $foto_20 = strip_tags($_POST["foto_20"]);
    if ($_FILES["foto_2"] != "") {
    @copy($_FILES["foto_2"]["tmp_name"], "$YPATH/" . $_FILES["foto_2"]["name"]);
    $foto_2 = $_FILES["foto_2"]["name"];
    } else {
    $foto_2 = $foto_20;
    }
    if (strlen($foto_2) < 1) {
    $foto_2 = $foto_20;
    }

        $foto_30 = strip_tags($_POST["foto_30"]);
    if ($_FILES["foto_3"] != "") {
        @copy($_FILES["foto_3"]["tmp_name"], "$YPATH/" . $_FILES["foto_3"]["name"]);
        $foto_3 = $_FILES["foto_3"]["name"];
    } else {
        $foto_3 = $foto_30;
    }
    if (strlen($foto_3) < 1) {
        $foto_3 = $foto_30;
    }

        $foto_40 = strip_tags($_POST["foto_40"]);
    if ($_FILES["foto_4"] != "") {
        @copy($_FILES["foto_4"]["tmp_name"], "$YPATH/" . $_FILES["foto_4"]["name"]);
        $foto_4 = $_FILES["foto_4"]["name"];
    } else {
        $foto_4 = $foto_40;
    }
    if (strlen($foto_4) < 1) {
        $foto_4 = $foto_40;
    }

        $foto_50 = strip_tags($_POST["foto_50"]);
    if ($_FILES["foto_5"] != "") {
        @copy($_FILES["foto_5"]["tmp_name"], "$YPATH/" . $_FILES["foto_5"]["name"]);
        $foto_5 = $_FILES["foto_5"]["name"];
    } else {
        $foto_5 = $foto_50;
    }
    if (strlen($foto_5) < 1) {
        $foto_5 = $foto_50;
    }

        $foto_60 = strip_tags($_POST["foto_60"]);
    if ($_FILES["foto_6"] != "") {
        @copy($_FILES["foto_6"]["tmp_name"], "$YPATH/" . $_FILES["foto_6"]["name"]);
        $foto_6 = $_FILES["foto_6"]["name"];
    } else {
        $foto_6 = $foto_60;
    }
    if (strlen($foto_6) < 1) {
        $foto_6 = $foto_60;
    }

    $petugas_lapangan = strip_tags($_POST["petugas_lapangan"]);
    $cek_pelanggaran = strip_tags($_POST["cek_pelanggaran"]);
    $kode_pelanggaran = strip_tags($_POST["kode_pelanggaran"]);
    $tanggal_penertiban = strip_tags($_POST["tanggal_penertiban"]);
    $status_pelaksanaan = strip_tags($_POST["status_pelaksanaan"]);

    if ($pro == "simpan") {
        $sql = " INSERT INTO `$tbtransdata_p2tl` (
            `latitude` ,
            `longitude` ,
            `foto_1` ,
            `foto_2` ,
            `foto_3` ,
            `foto_4` ,
            `foto_5` ,
            `foto_6` ,
            `cek_pelanggaran` ,
            `kode_pelanggaran` ,
            `tanggal_penertiban` ,
            `status_pelaksanaan`
        ) VALUES (
            '$latitude',
            '$longitude',
            '$foto_1',
            '$foto_2',
            '$foto_3',
            '$foto_4',
            '$foto_5',
            '$foto_6',
            '$cek_pelanggaran',
            '$kode_pelanggaran',
            '$tanggal_penertiban',
            '$status_pelaksanaan'
        )";

        $simpan = process($conn, $sql);
        if ($simpan) {
            echo "<script>alert('Data $id_transdata_p2tl berhasil disimpan !');document.location.href='?mnu=pltransdata_p2tl';</script>";
        } else {
            echo"<script>alert('Data $id_transdata_p2tl gagal disimpan...');document.location.href='?mnu=pltransdata_p2tl';</script>";
        }
    } else {
        $sql = "update `$tbtransdata_p2tl` set
        `latitude`='$latitude',
        `longitude`='$longitude',
        `foto_1`='$foto_1',
        `foto_2`='$foto_2',
        `foto_3`='$foto_3',
        `foto_4`='$foto_4',
        `foto_5`='$foto_5',
        `foto_6`='$foto_6',
        `cek_pelanggaran`='$cek_pelanggaran',
        `kode_pelanggaran`='$kode_pelanggaran',
        `tanggal_penertiban`='$tanggal_penertiban',
        `status_pelaksanaan`='$status_pelaksanaan'
        where `id_transdata_p2tl`='$id_transdata_p2tl0'";
        $ubah = process($conn, $sql);
        if ($ubah) {
            echo "<script>alert('Data $id_transdata_p2tl berhasil diubah !');document.location.href='?mnu=pltransdata_p2tl';</script>";
        } else {
            echo"<script>alert('Data $id_transdata_p2tl gagal diubah...');document.location.href='?mnu=pltransdata_p2tl';</script>";
        }
    }//else simpan
}
?>

<?php
if ($_GET["pro"] == "hapus") {
    $id_transdata_p2tl = $_GET["kode"];
    $sql = "delete from `$tbtransdata_p2tl` where `id_transdata_p2tl`='$id_transdata_p2tl'";
    $hapus = process($conn, $sql);
    if ($hapus) {
        echo "<script>alert('Data to $id_transdata_p2tl berhasil dihapus !');document.location.href='?mnu=pltransdata_p2tl';</script>";
    } else {
        echo"<script>alert('Data to $id_transdata_p2tl gagal dihapus...');document.location.href='?mnu=pltransdata_p2tl';</script>";
    }
}
?>
