<?php
require_once"../konmysqli.php";
$respon = array();
$sql="SELECT * FROM `$tbmasterstatus` order by `id_status_pelaksanaan` desc";
$jum=getJum($conn,$sql);
if ($jum> 0) {
    $respon["record"] = array();
	$arr=getData($conn,$sql);
		foreach($arr as $d) {
        $record = array();
        $record["id_status_pelaksanaan"] = $d["id_status_pelaksanaan"];
        $record["nama_status"] = $d["nama_status"];
        $record["keterangan"] = $d["keterangan"];

        array_push($respon["record"], $record);       //tambahkan array 'record' pada array final 'respon'
    }
    // sukses
    $respon["sukses"] = 1;
	$respon["pesan"] = "$jum record";
    echo json_encode($respon);
} else {
    // jika data kosong
	$respon["record"]="";
    $respon["sukses"] = 0;
    $respon["pesan"] = "0 record";
    echo json_encode($respon);
}
?>


<?php

function getJum($conn,$sql){
  $rs=$conn->query($sql);
  $jum= $rs->num_rows;
	$rs->free();
	return $jum;
}

function getData($conn,$sql){
	$rs=$conn->query($sql);
	$rs->data_seek(0);
	$arr = $rs->fetch_all(MYSQLI_ASSOC);
	$rs->free();
	return $arr;
}
?>
