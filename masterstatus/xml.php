<?php
header("Content-type: text/xml");

include "../konmysqli.php";
$sql = "select * from `$tbmasterstatus`";
if(getJum($conn,$sql)>0){
	print "<to>\n";
		$arr=getData($conn,$sql);
		foreach($arr as $d) {
				$id_status_pelaksanaan=$d["id_status_pelaksanaan"];
				$nama_status=$d["nama_status"];
				$keterangan=$d["keterangan"];
				$uraian=$d["uraian"];
				$jam_updatestatus=$d["jam_updatestatus"];
				$tanggal_updatestatus=$d["tanggal_updatestatus"];

				print "<record>\n";
				print "  <nama_status>$nama_status</nama_status>\n";
				print "  <keterangan>$keterangan</keterangan>\n";
				print "  <uraian>$uraian</uraian>\n";
				print "  <jam_updatestatus>$jam_updatestatus</jam_updatestatus>\n";
				print "  <tanggal_updatestatus>$tanggal_updatestatus</tanggal_updatestatus>\n";
				print "  <id_status_pelaksanaan>$id_status_pelaksanaan</id_status_pelaksanaan>\n";
				print "</record>\n";
			}
	print "</to>\n";
}
else{
	$null="null";
	print "<to>\n";
		print "<record>\n";
				print "  <nama_status>$null</nama_status>\n";
				print "  <keterangan>$null</keterangan>\n";
				print "  <uraian>$null</uraian>\n";
				print "  <jam_updatestatus>$jam_updatestatus</jam_updatestatus>\n";
				print "  <tanggal_updatestatus>$tanggal_updatestatus</tanggal_updatestatus>\n";
				print "  <id_status_pelaksanaan>$null</id_status_pelaksanaan>\n";
		print "</record>\n";
	print "</to>\n";
	}
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

function getJum($conn,$sql){
  $rs=$conn->query($sql);
  $jum= $rs->num_rows;
	$rs->free();
	return $jum;
}

function getData($conn,$sql){
	$rs=$conn->query($sql);
	$rs->data_seek(0);
	$arr = $rs->fetch_all(MYSQLI_ASSOC);

	$rs->free();
	return $arr;
}
?>
