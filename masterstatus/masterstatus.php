<?php
$pro = "simpan";
$tanggal_masterstatus = (date("Y-m-d"));
$jam_masterstatus = date("H:i:s");

if(!isset($_SESSION["cid"])){
	die("<script>location.href='index.php'</script>");
	}
?>
<link type="text/css" href="<?php echo "$PATH/base/"; ?>ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo "$PATH/"; ?>jquery-1.3.2.js"></script>
<script type="text/javascript" src="<?php echo "$PATH/"; ?>ui/ui.core.js"></script>
<script type="text/javascript" src="<?php echo "$PATH/"; ?>ui/ui.datepicker.js"></script>
<script type="text/javascript" src="<?php echo "$PATH/"; ?>ui/i18n/ui.datepicker-id.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#tanggal_masterstatus").datepicker({
            dateFormat: "dd MM yy",
            changeMonth: true,
            changeYear: true
        });
    });
</script>

<script type="text/javascript">
    function PRINT() {
        win = window.open('masterstatus/print.php', 'win', 'width=1000, height=400, menubar=0, scrollbars=1, resizable=0, location=0, toolbar=0, status=0');
    }
</script>
<script language="JavaScript">
    function buka(url) {
        window.open(url, 'window_baru', 'width=800,height=600,left=320,top=100,resizable=1,scrollbars=1');
    }
</script>

<?php
$sql = "select `id_status_pelaksanaan` from `$tbmasterstatus` order by `id_status_pelaksanaan` desc";
$q = mysqli_query($conn, $sql);
$jum = mysqli_num_rows($q);
$th = date("y");
$bl = date("m") + 0;
if ($bl < 10) {
    $bl = "0" . $bl;
}

$kd = "STA" . $th . $bl; //KEG1610001
if ($jum > 0) {
    $d = mysqli_fetch_array($q);
    $idmax = $d["id_status_pelaksanaan"];

    $bul = substr($idmax, 5, 2);
    $tah = substr($idmax, 3, 2);
    if ($bul == $bl && $tah == $th) {
        $urut = substr($idmax, 7, 3) + 1;
        if ($urut < 10) {
            $idmax = "$kd" . "00" . $urut;
        } else if ($urut < 100) {
            $idmax = "$kd" . "0" . $urut;
        } else {
            $idmax = "$kd" . $urut;
        }
    }//==
    else {
        $idmax = "$kd" . "001";
    }
}//jum>0
else {
    $idmax = "$kd" . "001";
}
$id_status_pelaksanaan = $idmax;
?>

<?php
if ($_GET["pro"] == "ubah") {
    $id_status_pelaksanaan = $_GET["kode"];
    $sql = "select * from `$tbmasterstatus` where `id_status_pelaksanaan`='$id_status_pelaksanaan'";
    $d = getField($conn, $sql);
    $id_status_pelaksanaan = $d["id_status_pelaksanaan"];
    $id_status_pelaksanaan0 = $d["id_status_pelaksanaan"];
    $nama_status = $d["nama_status"];
    $keterangan = $d["keterangan"];
    $pro = "ubah";
}
?>


<form action="" method="post" enctype="multipart/form-data">

    <div class="box-body row">
        <div class="col-sm-3">
        </div>
        <div class="form-group col-sm-6" >
            <div class=form-group>
            <label for="id_status_pelaksanaan">ID Status</label>
            <input disabled="disabled" class="form-control" value="<?php echo $id_status_pelaksanaan; ?>"/>
        </div>
        <div class=form-group>
            <label for="nama_status">Nama Status</label>
            <input class="form-control" name="nama_status" type="text" id="nama_status" required="required" value="<?php echo $nama_status; ?>" size="30" />
        </div>
        <div class=form-group>
            <label for="keterangan">Keterangan</label>
            <input class="form-control" name="keterangan" type="text" id="keterangan" required="required" value="<?php echo $keterangan; ?>" size="30" />
        </div>

            <div class="form-group" align="right">
                <button type="submit" name="Simpan"id="Simpan" class="btn btn-primary">Simpan</button>
                <input name="pro" type="hidden" id="pro" value="<?php echo $pro; ?>" />
                <input name="id_status_pelaksanaan" type="hidden" id="id_status_pelaksanaan" value="<?php echo $id_status_pelaksanaan; ?>" />
                <input name="id_status_pelaksanaan0" type="hidden" id="id_status_pelaksanaan0" value="<?php echo $id_status_pelaksanaan0; ?>" />
                <a href="?mnu=masterstatus"><button type="button" name="Batal" id="Batal" class="btn btn-danger">Batal</button></a>
            </div>
        </div>
        <div class="col-sm-3">
        </div>
    </div>
</form>

Data masterstatus:
| <a href="masterstatus/pdf.php"><img src='ypathicon/pdf.png' alt='PDF'></a>
| <a href="masterstatus/xls.php"><img src='ypathicon/xls.png' alt='XLS'></a>
| <a href="masterstatus/xml.php"><img src='ypathicon/xml.png' alt='XML'></a>
| <img src='ypathicon/print.png' alt='PRINT' OnClick="PRINT()"> |
<br>
<div class="row">
    <div class="table-responsive">
        <table id="tabelmasterstatus" class="table table-bordered table-striped dataTable">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Kode</th>
                    <th>Nama Status</th>
                    <th>Keterangan</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <?php
            $sql = "select * from `$tbmasterstatus` order by `id_status_pelaksanaan` desc";
            $jum = getJum($conn, $sql);
            if ($jum > 0) {
                //--------------------------------------------------------------------------------------------
                $batas = 10;
                $page = $_GET['page'];
                if (empty($page)) {
                    $posawal = 0;
                    $page = 1;
                } else {
                    $posawal = ($page - 1) * $batas;
                }

                $sql2 = $sql . " LIMIT $posawal,$batas";
                $no = $posawal + 1;
                //--------------------------------------------------------------------------------------------
                $arr = getData($conn, $sql2);
                foreach ($arr as $d) {
                    $id_status_pelaksanaan = $d["id_status_pelaksanaan"];
                    $nama_status = $d["nama_status"];
                    $keterangan = $d["keterangan"];
                    $color = "#dddddd";
                    if ($no % 2 == 0) {
                        $color = "#eeeeee";
                    }
                    echo"<tr bgcolor='$color'>
				<td>$no</td>
				<td>$id_status_pelaksanaan</td>
				<td>$nama_status</td>
				<td>$keterangan</td>
				<td align='center'>
<a href='?mnu=masterstatus&pro=ubah&kode=$id_status_pelaksanaan'><img src='ypathicon/u.png' alt='ubah'></a>
<a href='?mnu=masterstatus&pro=hapus&kode=$id_status_pelaksanaan'><img src='ypathicon/h.png' alt='hapus'
onClick='return confirm(\"Apakah Anda benar-benar akan menghapus $nama_status pada data masterstatus ?..\")'></a></td>
				</tr>";

                    $no++;
                }//while
            }//if
            else {
                echo"<tr><td colspan='7'><blink>Maaf, Data masterstatus belum tersedia...</blink></td></tr>";
            }
            ?>
        </table>
    </div>
</div>
<?php
//Langkah 3: Hitung total data dan page
$jmldata = $jum;
if ($jmldata > 0) {
    if ($batas < 1) {
        $batas = 1;
    }
    $jmlhal = ceil($jmldata / $batas);
    echo "<div class=paging>";
    if ($page > 1) {
        $prev = $page - 1;
        echo "<span class=prevnext><a href='$_SERVER[PHP_SELF]?page=$prev&mnu=masterstatus'>« Prev</a></span> ";
    } else {
        echo "<span class=disabled>« Prev</span> ";
    }

    // Tampilkan link page 1,2,3 ...
    for ($i = 1; $i <= $jmlhal; $i++)
        if ($i != $page) {
            echo "<a href='$_SERVER[PHP_SELF]?page=$i&mnu=masterstatus'>$i</a> ";
        } else {
            echo " <span class=current>$i</span> ";
        }

    // Link kepage berikutnya (Next)
    if ($page < $jmlhal) {
        $next = $page + 1;
        echo "<span class=prevnext><a href='$_SERVER[PHP_SELF]?page=$next&mnu=masterstatus'>Next »</a></span>";
    } else {
        echo "<span class=disabled>Next »</span>";
    }
    echo "</div>";
}//if jmldata

$jmldata = $jum;
echo "<p align=center>Total Data <b>$jmldata</b> Item</p>";
?>

<?php
if (isset($_POST["Simpan"])) {
    $pro = strip_tags($_POST["pro"]);
    $id_status_pelaksanaan = strip_tags($_POST["id_status_pelaksanaan"]);
    $id_status_pelaksanaan0 = strip_tags($_POST["id_status_pelaksanaan0"]);
    $nama_status = strip_tags($_POST["nama_status"]);
    $keterangan = strip_tags($_POST["keterangan"]);

    if ($pro == "simpan") {
        $sql = " INSERT INTO `$tbmasterstatus` (
`id_status_pelaksanaan` ,
`nama_status` ,
`keterangan` ,
`uraian` ,
`jam_masterstatus` ,
`tanggal_masterstatus`
) VALUES (
'$id_status_pelaksanaan',
'$nama_status',
'$keterangan'
)";

        $simpan = process($conn, $sql);
        if ($simpan) {
            echo "<script>alert('Data $id_status_pelaksanaan berhasil disimpan !');document.location.href='?mnu=masterstatus';</script>";
        } else {
            echo"<script>alert('Data $id_status_pelaksanaan gagal disimpan...');document.location.href='?mnu=masterstatus';</script>";
        }
    } else {
        $sql = "update `$tbmasterstatus` set
`nama_status`='$nama_status',
`keterangan`='$keterangan'
where `id_status_pelaksanaan`='$id_status_pelaksanaan0'";
        $ubah = process($conn, $sql);
        if ($ubah) {
            echo "<script>alert('Data $id_status_pelaksanaan berhasil diubah !');document.location.href='?mnu=masterstatus';</script>";
        } else {
            echo"<script>alert('Data $id_status_pelaksanaan gagal diubah...');document.location.href='?mnu=masterstatus';</script>";
        }
    }//else simpan
}
?>

<?php
if ($_GET["pro"] == "hapus") {
    $id_status_pelaksanaan = $_GET["kode"];
    $sql = "delete from `$tbmasterstatus` where `id_status_pelaksanaan`='$id_status_pelaksanaan'";
    $hapus = process($conn, $sql);
    if ($hapus) {
        echo "<script>alert('Data masterstatus $id_status_pelaksanaan berhasil dihapus !');document.location.href='?mnu=masterstatus';</script>";
    } else {
        echo"<script>alert('Data masterstatus $id_status_pelaksanaan gagal dihapus...');document.location.href='?mnu=masterstatus';</script>";
    }
}
?>
