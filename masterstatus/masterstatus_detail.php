<?php
require_once"../konmysqli.php";
$respon = array();

if (isset($_GET["id_status_pelaksanaan"])) {
    $id_status_pelaksanaan = $_GET['id_status_pelaksanaan'];
	$sql="SELECT * FROM `$tbmasterstatus` WHERE `id_status_pelaksanaan` = '$id_status_pelaksanaan'";
    $jum=getJum($conn,$sql);
    if ($jum>0) {
            $d=getField($conn,$sql);
		    $record = array();
            $record["id_status_pelaksanaan"] = $d["id_status_pelaksanaan"];
			$record["nama_status"] = $d["nama_status"];
			$record["keterangan"] = $d["keterangan"];
            $respon["sukses"] = 1;
            $respon["record"] = array();

            array_push($respon["record"], $record);
             $respon["pesan"] = "$jum record";
			echo json_encode($respon);
        } else {
            $respon["sukses"] = 0;
            $respon["pesan"] = "0 record";
            echo json_encode($respon);
        }

} else {
    $respon["sukses"] = 0;
    $respon["pesan"] = "? lengkapi data";
    echo json_encode($respon);
}
?>

<?php

function getJum($conn,$sql){
  $rs=$conn->query($sql);
  $jum= $rs->num_rows;
	$rs->free();
	return $jum;
}

function getField($conn,$sql){
	$rs=$conn->query($sql);
	$rs->data_seek(0);
	$d= $rs->fetch_assoc();
	$rs->free();
	return $d;
}
?>
