<?php
header("Content-type: text/xml");

include "../konmysqli.php";
$sql = "select * from `$tbmasterpetugas`";
if(getJum($conn,$sql)>0){
	print "<user>\n";
		$arr=getData($conn,$sql);
		foreach($arr as $d) {
				$id_user=$d["id_user"];
				$id_vendor=$d["id_vendor"];
				$nama_user=$d["nama_user"];
				$alamat_user=$d["alamat_user"];
				$telepon_user=$d["telepon_user"];
				$nomor_imei=$d["nomor_imei"];
				$email_user=$d["email_user"];
				$password=$d["password"];
				$level_user=$d["level_user"];
				$gambar=$d["gambar"];

				print "<record>\n";
				print "  <id_vendor>$id_vendor</id_vendor>\n";
				print "  <nama_user>$nama_user</nama_user>\n";
				print "  <alamat_user>$alamat_user</alamat_user>\n";
				print "  <telepon_user>$telepon_user</telepon_user>\n";
				print "  <nomor_imei>$nomor_imei</nomor_imei>\n";
				print "  <email_user>$email_user</email_user>\n";
				print "  <password>$password</password>\n";
				print "  <level_user>$level_user</level_user>\n";
				print "  <gambar>$gambar</gambar>\n";
				print "  <id_user>$id_user</id_user>\n";
				print "</record>\n";
			}
	print "</user>\n";
}
else{
	$null="null";
	print "<user>\n";
				print "<record>\n";
				print "  <id_vendor>$null</id_vendor>\n";
				print "  <nama_user>$null</nama_user>\n";
				print "  <alamat_user>$null</alamat_user>\n";
				print "  <telepon_user>$null</telepon_user>\n";
				print "  <nomor_imei>$null</nomor_imei>\n";
				print "  <email_user>$null</email_user>\n";
				print "  <password>$null</password>\n";
				print "  <level_user>$null</level_user>\n";
				print "  <gambar>$null</gambar>\n";
				print "  <id_user>$null</id_user>\n";
				print "</record>\n";
	print "</user>\n";

}

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

function getJum($conn,$sql){
  $rs=$conn->query($sql);
  $jum= $rs->num_rows;
	$rs->free();
	return $jum;
}

function getData($conn,$sql){
	$rs=$conn->query($sql);
	$rs->data_seek(0);
	$arr = $rs->fetch_all(MYSQLI_ASSOC);

	$rs->free();
	return $arr;
}
?>
