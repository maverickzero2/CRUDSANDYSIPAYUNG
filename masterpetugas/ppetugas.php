<?php
$tanggal = WKT(date("Y-m-d"));
$pro = "simpan";
$gambar0 = "avatar.jpg";
$password = "Aktif";
$pic_vendor=$_SESSION["cid"];
//$PATH="ypathcss";
if(!isset($_SESSION["cid"])){
  die("<script>location.href='index.php'</script>");
}
?>
<link type="text/css" href="<?php echo "$PATH/base/"; ?>ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo "$PATH/"; ?>jquery-1.3.2.js"></script>
<script type="text/javascript" src="<?php echo "$PATH/"; ?>ui/ui.core.js"></script>
<script type="text/javascript" src="<?php echo "$PATH/"; ?>ui/ui.datepicker.js"></script>
<script type="text/javascript" src="<?php echo "$PATH/"; ?>ui/i18n/ui.datepicker-id.js"></script>

<script type="text/javascript">
$(document).ready(function () {
  $("#tanggal").datepicker({
    dateFormat: "dd MM yy",
    changeMonth: true,
    changeYear: true
  });
});
</script>

<script type="text/javascript">
function PRINT() {
  win = window.open('user/print.php', 'win', 'width=1000, height=400, menubar=0, scrollbars=1, resizable=0, location=0, toolbar=0, password=0');
}
</script>
<script language="JavaScript">
function buka(url) {
  window.open(url, 'window_baru', 'width=800,height=600,left=320,top=100,resizable=1,scrollbars=1');
}
function MM_validateForm() { //v4.0
  if (document.getElementById){
    var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
    for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=document.getElementById(args[i]);
      if (val) { nm=val.name; if ((val=val.value)!="") {
        if (test.indexOf('istelepon_user')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
        min=test.substring(8,p); max=test.substring(p+1);
        if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
      } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
    } if (errors) alert('The following error(s) occurred:\n'+errors);
    document.MM_returnValue = (errors == '');
  } }
  </script>

  <?php
  $sql = "select `id_user` from `$tbmasterpetugas` order by `id_user` desc";
  $q = mysqli_query($conn, $sql);
  $jum = mysqli_num_rows($q);
  $th = date("y");
  $bl = date("m") + 0;
  if ($bl < 10) {
    $bl = "0" . $bl;
  }

  $kd = "USR" . $th . $bl; //KEG1610001
  if ($jum > 0) {
    $d = mysqli_fetch_array($q);
    $idmax = $d["id_user"];

    $bul = substr($idmax, 5, 2);
    $tah = substr($idmax, 3, 2);
    if ($bul == $bl && $tah == $th) {
      $urut = substr($idmax, 7, 3) + 1;
      if ($urut < 10) {
        $idmax = "$kd" . "00" . $urut;
      } else if ($urut < 100) {
        $idmax = "$kd" . "0" . $urut;
      } else {
        $idmax = "$kd" . $urut;
      }
    }//==
    else {
      $idmax = "$kd" . "001";
    }
  }//jum>0
  else {
    $idmax = "$kd" . "001";
  }
  $id_user = $idmax;
  ?>
  <?php
  if ($_GET["pro"] == "ubah") {
    $id_user = $_GET["kode"];
    $sql = "select * from `$tbmasterpetugas` where `id_user`='$id_user'";
    $d = getField($conn, $sql);
    $id_user = $d["id_user"];
    $id_vendor = $d["id_vendor"];
    $nama_user = $d["nama_user"];
    $alamat_user = $d["alamat_user"];
    $telepon_user = $d["telepon_user"];
    $nomor_imei = $d["nomor_imei"];
    $email_user = $d["email_user"];
    $password = $d["password"];
    $level_user = $d["level_user"];
    $gambar = $d["gambar"];
    $gambar0 = $d["gambar"];
    $pro = "ubah";
  }
  ?>
  <div class="panel-group" id="accordion">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
          Input Petugas Lapangan</a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse in">
        <div class="panel-body">

  <form action="" method="post" enctype="multipart/form-data">
    <div class="box-body row">
      <div class="form-group col-sm-5" >
        <center>
          <?php
          echo"<a href='#' onclick='buka(\"user/zoom.php?id=$id_user\")'>
          <img src='$YPATH/$gambar0' width='77' height='80' />
          </a>
          ";
          ?>
        </center>
      </div>

      <div class="form-group col-sm-6" >
        <div class=form-group>
          <label for="id_user">ID User</label>
          <input disabled="disabled" class="form-control" value="<?php echo $id_user; ?>" />
        </div>
        <div class=form-group>
          <label for="id_vendor">Nama Vendor</label>
          <select class="form-control"  name="id_vendor" id="id_vendor">
            <?php
            $sql = "select * from `$tbmastervendor` where pic_vendor='$pic_vendor'";//" INNER JOIN `$tbmasterpetugas` ON $tbmastervendor.pic_vendor=$tbmasterpetugas.id_user"; // where `level`='AP2T'";
            $arr = getData($conn, $sql);
            foreach ($arr as $d) {
              $id_vendor0 = $d["id_vendor"];
              $nama_vendor = $d["nama_vendor"];
              echo"<option value='$id_vendor0' ";
              if ($id_vendor0 == $id_vendor) {
                echo"selected";
              }echo">$nama_vendor</option>";
            }
            ?>
          </select>
        </div>

        <div class=form-group>
          <label for="nama_user">Nama Petugas Lapangan</label>
          <input class="form-control" name="nama_user" type="text" id="nama_user" value="<?php echo $nama_user; ?>" />
        </div>
        <div class=form-group>
          <label for="alamat_user">Alamat Petugas Lapangan</label>
          <textarea class="form-control" name="alamat_user" type="text" id="alamat_user" size="25"><?php echo $alamat_user; ?></textarea>
      </div>
        <div class=form-group>
          <label for="telepon_user">Telepon Petugas Lapangan</label>
          <input class="form-control" name="telepon_user" type="number" id="telepon_user" value="<?php echo $telepon_user; ?>" size="25" />
        </div>
        <div class="form-group">
          <label for="nomor_imei">Nomor IMEI</label>
          <input class="form-control" name="nomor_imei" type="text" id="nomor_imei" value="<?php echo $nomor_imei; ?>" size="15" />
        </div>

        <label for="email_user">Email Petugas Lapangan</label>
        <div class="input-group">
          <span class="input-group-addon">@</span>
          <input class="form-control" name="email_user" type="email" required="required" id="email_user" value="<?php echo $email_user; ?>" size="25" />

        </div>
        <div class="form-group">
          <label for="password">Password</label>
          <div class="input-group">
            <span class="input-group-addon">#</span>
            <input class="form-control" name="password" type="password" required="required" id="password" value="<?php echo $password; ?>" size="25" />
          </div>
        </div>
        <label for="level_user">Level Petugas Lapangan</label>
        <select class="form-control" name="level_user" id="level_user">
          <option value="PL">PL</option>
        </select>
        <label for="gambar">Gambar</label>
        <input class="form-control" name="gambar" type="file" id="gambar" size="20" />
        => <a href='#' onclick='buka("user/zoom.php?id=<?php echo $id_user; ?>")'><?php echo $gambar0; ?></a></td>

        <div class="form-group" align="right">
          <button name="Simpan" type="submit" class="btn btn-primary"id="Simpan">Simpan</button>
          <!--<input name="Simpan" type="submit" id="Simpan" value="Simpan" />-->
          <input class="form-control" name="pro" type="hidden" id="pro" value="<?php echo $pro; ?>" />
          <input class="form-control" name="gambar0" type="hidden" id="gambar0" value="<?php echo $gambar0; ?>" />
          <input class="form-control" name="id_user" type="hidden" id="id_user" value="<?php echo $id_user; ?>" />
          <input class="form-control" name="id_user0" type="hidden" id="id_user0" value="<?php echo $id_user0; ?>" />
          <a href="?mnu=user"><button type="button" name="Batal" id="Batal" class="btn btn-danger">Batal</button></a>
        </div>
      </div>
    </div>
  </form>
  <br />

</div>
    </div>
  </div>

  <?php
  $sqlq="select distinct(nama_vendor) from `$tbmastervendor` order by `id_vendor` desc";
  $jumq=getJum($conn,$sqlq);
        if($jumq <1){
            echo"<h1>Maaf data belum tersedia...</h1>";
            }
    $arrq=getData($conn,$sqlq);
        foreach($arrq as $dq) {
                $id_vendor=$dq["id_vendor"];
                        $nama_vendor=$dq["nama_vendor"];
                                $id_vendorr=$dq["id_vendor"];

?>

  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordions" href="#collapse2">
        Data Vendor <?php echo $nama_vendor; ?></a>
      </h4>
    </div>
    <div id="collapse2" class="panel-collapse collapse">
      <div class="panel-body">
  Data user:
  | <a href="masterpetugas/pdf.php"><img src='ypathicon/pdf.png' alt='PDF'></a>
  | <a href="masterpetugas/xml.php"><img src='ypathicon/xml.png' alt='XML'></a>
  | <a href="masterpetugas/xls.php"><img src='ypathicon/xls.png' alt='XLS'></a>
  | <img src='ypathicon/print.png' alt='PRINT' OnClick="PRINT()"> |
  <br>

  <div class="row">
    <div class="table-responsive">
      <table id="tabeluser" class="table table-bordered table-striped dataTable">
        <thead>
          <tr>
            <th>No</td>
            <th>User</td>
            <th>Nama Vendor</td>
            <th>Alamat</td>
            <th>Nomor IMEI</td>
            <th>Gambar</td>
            <th>Aksi</td>
          </tr>
        </thead>
                      <?php
                      $sql = "select * from `$tbmasterpetugas` where `level_user`='PL' AND `id_vendor`='$id_vendor0' order by `id_user` desc";
                      $jum = getJum($conn, $sql);
                      if ($jum > 0) {
                        //--------------------------------------------------------------------------------------------
                        $batas = 10;
                        $page = $_GET['page'];
                        if (empty($page)) {
                          $posawal = 0;
                          $page = 1;
                        } else {
                          $posawal = ($page - 1) * $batas;
                        }

                        $sql2 = $sql . " LIMIT $posawal,$batas";
                        $no = $posawal + 1;
                        //--------------------------------------------------------------------------------------------
                        $arr = getData($conn, $sql2);
                        foreach ($arr as $d) {
                          $id_user = $d["id_user"];
                          $id_vendor = getVendor($conn, $d["id_vendor"]);
                          $nama_user = $d["nama_user"];
                          $alamat_user = $d["alamat_user"];
                          $telepon_user = $d["telepon_user"];
                          $nomor_imei = $d["nomor_imei"];
                          $email_user = $d["email_user"];
                          $password = $d["password"];
                          $level_user = $d["level_user"];
                          $gambar = $d["gambar"];
                          $gambar0 = $d["gambar"];
                          $color = "#dddddd";
                          if ($no % 2 == 0) {
                            $color = "#eeeeee";
                          }
                          echo"<tr bgcolor='$color'>
                          <td valign='top'>$no</td>
                          <td valign='top'><a href='mailto:$email_user'>$nama_user</a><br>$id_user</td>
                          <td valign='top'>$id_vendor</td>
                          <td valign='top'>$alamat_user</td>
                          <td valign='top'>$nomor_imei<br>password:$password<br>level: $level_user</td>
                          <td><div align='center'>";
                          echo"<a href='#' onclick='buka(\"user/zoom.php?id=$id_user\")'>
                          <img src='$YPATH/$gambar' width='80' height='80' /></a></div>";
                          echo"</td>
                          <td><div align='center'>
                          <a href='?mnu=ppetugas&pro=ubah&kode=$id_user'><img src='ypathicon/u.png' alt='ubah'></a>
                          <a href='?mnu=ppetugas&pro=hapus&kode=$id_user'><img src='ypathicon/h.png' alt='hapus'
                          onClick='return confirm(\"Apakah Anda benar-benar akan menghapus $nama pada data user ?..\")'></a></div></td>
                          </tr>";

                          $no++;
                        }//while
                      }//if
                      else {
                        echo"<tr><td colspan='6'><blink>Maaf, Data user belum tersedia...</blink></td></tr>";
                      }
                      ?>
                    </table>
                  </div>
                </div>
              </div>
                  </div>
                </div>
                <?php } ?>
              </div>
                <?php
                if (isset($_POST["Simpan"])) {
                  $pro = strip_tags($_POST["pro"]);
                  $id_user = strip_tags($_POST["id_user"]);
                  $id_user0 = strip_tags($_POST["id_user"]);
                  $id_vendor = ($_POST["id_vendor"]);
                  $nama_user = strip_tags($_POST["nama_user"]);
                  $alamat_user = strip_tags($_POST["alamat_user"]);
                  $telepon_user = strip_tags($_POST["telepon_user"]);
                  $nomor_imei = strip_tags($_POST["nomor_imei"]);
                  $email_user = strip_tags($_POST["email_user"]);
                  $password = strip_tags($_POST["password"]);
                  $level_user = strip_tags($_POST["level_user"]);

                  $gambar0 = strip_tags($_POST["gambar0"]);
                  if ($_FILES["gambar"] != "") {
                    @copy($_FILES["gambar"]["tmp_name"], "$YPATH/" . $_FILES["gambar"]["name"]);
                    $gambar = $_FILES["gambar"]["name"];
                  } else {
                    $gambar = $gambar0;
                  }
                  if (strlen($gambar) < 1) {
                    $gambar = $gambar0;
                  }

                  if ($pro == "simpan") {
                    $sql = " INSERT INTO `$tbmasterpetugas` (
                      `id_user` ,
                      `id_vendor` ,
                      `nama_user` ,
                      `alamat_user` ,
                      `telepon_user` ,
                      `nomor_imei` ,
                      `email_user` ,
                      `password` ,
                      `level_user` ,
                      `gambar`
                    ) VALUES (
                      '$id_user',
                      '$id_vendor',
                      '$nama_user',
                      '$alamat_user',
                      '$telepon_user',
                      '$nomor_imei',
                      '$email_user',
                      '$password',
                      '$level_user',
                      '$gambar'
                    )";

                    $simpan = process($conn, $sql);
                    if ($simpan) {
                      echo "<script>alert('Data $id_user berhasil disimpan !');document.location.href='?mnu=ppetugas';</script>";
                    } else {
                      echo"<script>alert('Data $id_user gagal disimpan...');document.location.href='?mnu=ppetugas';</script>";
                    }
                  } else {
                    $sql = "update `$tbmasterpetugas` set `id_vendor`='$id_vendor',`nama_user`='$nama_user',`alamat_user`='$alamat_user' ,`telepon_user`='$telepon_user', `nomor_imei`='$nomor_imei',`email_user`='$email_user',`password`='$password', `level_user`='$level_user',
                    `gambar`='$gambar'  where `id_user`='$id_user0'";
                    $ubah = process($conn, $sql);
                    if ($ubah) {
                      echo "<script>alert('Data $id_user berhasil diubah !');document.location.href='?mnu=ppetugas';</script>";
                    } else {
                      echo"<script>alert('Data $id_user gagal diubah...');document.location.href='?mnu=ppetugas';</script>";
                    }
                  }//else simpan
                }
                ?>

                <?php
                if ($_GET["pro"] == "hapus") {
                  $id_user = $_GET["kode"];
                  $sql = "delete from `$tbmasterpetugas` where `id_user`='$id_user'";
                  $hapus = process($conn, $sql);
                  if ($hapus) {
                    echo "<script>alert('Data $id_user berhasil dihapus !');document.location.href='?mnu=ppetugas';</script>";
                  } else {
                    echo"<script>alert('Data $id_user gagal dihapus...');document.location.href='?mnu=ppetugas';</script>";
                  }
                }
                ?>
