<?php
$tanggal = WKT(date("Y-m-d"));
$pro = "simpan";
$gambar0 = "avatar.jpg";
$password = "Aktif";
//$PATH="ypathcss";
if(!isset($_SESSION["cid"])){
  die("<script>location.href='index.php'</script>");
}
?>
<link type="text/css" href="<?php echo "$PATH/base/"; ?>ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo "$PATH/"; ?>jquery-1.3.2.js"></script>
<script type="text/javascript" src="<?php echo "$PATH/"; ?>ui/ui.core.js"></script>
<script type="text/javascript" src="<?php echo "$PATH/"; ?>ui/ui.datepicker.js"></script>
<script type="text/javascript" src="<?php echo "$PATH/"; ?>ui/i18n/ui.datepicker-id.js"></script>

<script type="text/javascript">
$(document).ready(function () {
  $("#tanggal").datepicker({
    dateFormat: "dd MM yy",
    changeMonth: true,
    changeYear: true
  });
});
</script>

<script type="text/javascript">
function PRINT() {
  win = window.open('user/print.php', 'win', 'width=1000, height=400, menubar=0, scrollbars=1, resizable=0, location=0, toolbar=0, password=0');
}
</script>
<script language="JavaScript">
function buka(url) {
  window.open(url, 'window_baru', 'width=800,height=600,left=320,top=100,resizable=1,scrollbars=1');
}
function MM_validateForm() { //v4.0
  if (document.getElementById){
    var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
    for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=document.getElementById(args[i]);
      if (val) { nm=val.name; if ((val=val.value)!="") {
        if (test.indexOf('istelepon_user')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
        min=test.substring(8,p); max=test.substring(p+1);
        if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
      } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
    } if (errors) alert('The following error(s) occurred:\n'+errors);
    document.MM_returnValue = (errors == '');
  } }
  </script>

  <?php

    $id_user = $_SESSION["cid"];
    $sql = "select * from `$tbmasterpetugas` where `id_user`='$id_user'";
    $d = getField($conn, $sql);
    $id_user = $d["id_user"];
    $id_vendor = $d["id_vendor"];
    $nama_user = $d["nama_user"];
    $alamat_user = $d["alamat_user"];
    $telepon_user = $d["telepon_user"];
    $nomor_imei = $d["nomor_imei"];
    $email_user = $d["email_user"];
    $password = $d["password"];
    $level_user = $d["level_user"];
    $gambar = $d["gambar"];
    $gambar0 = $d["gambar"];
    $pro = "ubah";

  ?>

  <form action="" method="post" enctype="multipart/form-data">
    <div class="box-body row">
      <div class="form-group col-sm-5" >
        <center>
          <?php
          echo"<a href='#' onclick='buka(\"user/zoom.php?id=$id_user\")'>
          <img src='$YPATH/$gambar0' width='77' height='80' />
          </a>
          ";
          ?>
        </center>
      </div>

      <div class="form-group col-sm-6" >
        <div class=form-group>
          <label for="id_user">ID User</label>
          <input disabled="disabled" class="form-control" value="<?php echo $id_user; ?>" />
        </div>
        <div class=form-group>
          <label for="id_vendor">Nama Vendor</label>
          <select disabled class="form-control" name="id_vendor" id="id_vendor">
            <?php
            $sql = "select id_vendor, nama_vendor from `$tbmastervendor`"; // where `level`='AP2T'";
            $arr = getData($conn, $sql);
            foreach ($arr as $d) {
              $id_vendor0 = $d["id_vendor"];
              $nama_vendor = $d["nama_vendor"];
              echo"<option value='$id_vendor0' ";
              if ($id_vendor0 == $id_vendor) {
                echo"selected";
              }echo">$nama_vendor</option>";
            }
            ?>
          </select>
        </div>
        <div class=form-group>
          <label for="nama_user">Nama User</label>
          <input class="form-control" name="nama_user" type="text" id="nama_user" value="<?php echo $nama_user; ?>" />
        </div>
        <div class=form-group>
          <label for="alamat_user">Alamat User</label>
          <textarea class="form-control" name="alamat_user" type="text" id="alamat_user" size="25"><?php echo $alamat_user; ?></textarea>
      </div>
        <div class=form-group>
          <label for="telepon_user">Telepon User</label>
          <input class="form-control" name="telepon_user" type="number" id="telepon_user" required="required" value="<?php echo $telepon_user; ?>" size="25" />
        </div>
        <div class="form-group">
          <label for="nomor_imei">Nomor IMEI</label>

          <input class="form-control" name="nomor_imei" type="text" id="nomor_imei" value="<?php echo $nomor_imei; ?>" size="15" />
        </div>

        <label for="email_user">Email User</label>
        <div class="input-group">
          <span class="input-group-addon">@</span>
          <input class="form-control" name="email_user" type="email" required="required" id="email_user" value="<?php echo $email_user; ?>" size="25" />

        </div>
        <div class="form-group">
          <label for="password">Password</label>
          <div class="input-group">
            <span class="input-group-addon">#</span>
            <input class="form-control" name="password" type="password" required="required" id="password" value="<?php echo $password; ?>" size="25" />
          </div>
        </div>
        <label for="level_user">Level User</label>
        <select class="form-control"  name="level_user" disabled id="level_user">
          <option value="PIC">PIC</option>
          <option value="PL">PL</option>
        </select>
        <label for="gambar">Gambar</label>
        <input class="form-control" name="gambar" type="file" id="gambar" size="20" />
        => <a href='#' onclick='buka("user/zoom.php?id=<?php echo $id_user; ?>")'><?php echo $gambar0; ?></a></td>

        <div class="form-group" align="right">
          <button name="Simpan" type="submit" class="btn btn-primary"id="Simpan">Update Profil</button>
          <input name="gambar0" type="hidden" id="gambar0" value="<?php echo $gambar0; ?>" />
                <a href="?mnu=profil"><button type="button" name="Batal" id="Batal" class="btn btn-danger">Batal</button></a>
        </div>
      </div>
    </div>
  </form>
                <?php
                if (isset($_POST["Simpan"])) {
                  $pro = strip_tags($_POST["pro"]);
                  $id_user = strip_tags($_SESSION["cid"]);
                  $id_vendor = ($_POST["id_vendor"]);
                  $nama_user = strip_tags($_POST["nama_user"]);
                  $alamat_user = strip_tags($_POST["alamat_user"]);
                  $telepon_user = strip_tags($_POST["telepon_user"]);
                  $nomor_imei = strip_tags($_POST["nomor_imei"]);
                  $email_user = strip_tags($_POST["email_user"]);
                  $password = strip_tags($_POST["password"]);
                  $level_user = strip_tags($_POST["level_user"]);

                  $gambar0 = strip_tags($_POST["gambar0"]);
                  if ($_FILES["gambar"] != "") {
                    @copy($_FILES["gambar"]["tmp_name"], "$YPATH/" . $_FILES["gambar"]["name"]);
                    $gambar = $_FILES["gambar"]["name"];
                  } else {
                    $gambar = $gambar0;
                  }
                  if (strlen($gambar) < 1) {
                    $gambar = $gambar0;
                  }

                    $sql = "update `$tbmasterpetugas` set `nama_user`='$nama_user',`alamat_user`='$alamat_user' ,`telepon_user`='$telepon_user', `nomor_imei`='$nomor_imei',`email_user`='$email_user',`password`='$password',
                    `gambar`='$gambar'  where `id_user`='$id_user'";
                    $ubah = process($conn, $sql);
                    if ($ubah) {
                      echo "<script>alert('Data $id_user berhasil diubah !');document.location.href='?mnu=profil';</script>";
                    } else {
                      echo"<script>alert('Data $id_user gagal diubah...');document.location.href='?mnu=profil';</script>";
                    }

                }
                ?>
