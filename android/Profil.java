package com.penginapan.penginapan;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.penginapan.penginapan.JSONParser;
import com.penginapan.penginapan.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Profil extends AppCompatActivity {
	String ip="";
	//String kode_pengelola;
	String kode_pengelola0="";

	EditText txtKode_pengelola;
	EditText txtNama_pengelola;
	EditText txtAlamat;
	EditText txtTelepon;
	EditText txtUsername;
	EditText txtPassword;
	EditText txtKeterangan;
	EditText txtStatus;

	Button btnProses;
	Button btnHapus;
String kode_pengelola,nama_pengelola,alamat,telepon,username,password,keterangan,status;
	String xml="",xmlbio="";
	private ProgressDialog pDialog;
	JSONParser jsonParser = new JSONParser();

	private static final String TAG_SUKSES = "sukses";
	private static final String TAG_record = "record";

	private static final String TAG_kode_pengelola = "kode_pengelola";
	private static final String TAG_nama_pengelola = "nama_pengelola";
	private static final String TAG_alamat = "alamat";
	private static final String TAG_telepon = "telepon";
	private static final String TAG_username = "username";
	private static final String TAG_password= "password";
	private static final String TAG_keterangan= "keterangan";
	private static final String TAG_status= "status";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profil);

		ip=jsonParser.getIP();
		//callMarquee();

		txtKode_pengelola = (EditText) findViewById(R.id.txtkode_pengelola);
		txtNama_pengelola= (EditText) findViewById(R.id.txtnama_pengelola);
		txtTelepon= (EditText) findViewById(R.id.txttelepon);
		txtAlamat= (EditText) findViewById(R.id.txtalamat);
		txtUsername= (EditText) findViewById(R.id.txtusername);
		txtPassword= (EditText) findViewById(R.id.txtpassword);
		txtKeterangan= (EditText) findViewById(R.id.txtketerangan);
		txtStatus= (EditText) findViewById(R.id.txtstatus);

		btnProses= (Button) findViewById(R.id.btnproses);
		btnHapus = (Button) findViewById(R.id.btnhapus);

		Intent i = getIntent();
		kode_pengelola0 = i.getStringExtra("xmlbio");
		Log.v("xmlbio",xmlbio);
		Toast.makeText(Profil.this,xmlbio+ " telah dipilih....", Toast.LENGTH_LONG).show();

		String[]ar=kode_pengelola0.split("#");
		if(ar.length<8){finish();}
		kode_pengelola=ar[0];
		nama_pengelola=ar[1];
		alamat=ar[2];
		telepon=ar[3];
		username=ar[4];
		password=ar[5];
		keterangan=ar[6];
		status=ar[7];

		txtKode_pengelola.setText(kode_pengelola0);
		txtNama_pengelola.setText(nama_pengelola);
		txtAlamat.setText(alamat);
		txtTelepon.setText(telepon);
		txtKode_pengelola.setText(kode_pengelola);
		txtUsername.setText(username);
		txtPassword.setText(password);
		txtKeterangan.setText(keterangan);
		txtStatus.setText(status);


			//new get().execute();
			//btnProses.setText("Update Data");
//			btnHapus.setVisibility(View.VISIBLE);
//
//			btnProses.setText("Kembali");
			//btnHapus.setVisibility(View.VISIBLE);


		btnProses.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				kode_pengelola= txtKode_pengelola.getText().toString();
				String lnama_pengelola= txtNama_pengelola.getText().toString();
				String ltelepon= txtTelepon.getText().toString();
				String lalamat= txtAlamat.getText().toString();
				String lkode_pengelola= txtKode_pengelola.getText().toString();
				String lusername= txtUsername.getText().toString();
				String lpassword= txtPassword.getText().toString();
				String lketerangan= txtKeterangan.getText().toString();
				String lstatus= txtStatus.getText().toString();

				if(kode_pengelola.length()<1){lengkapi("kode_pengelola");}
				else if(lnama_pengelola.length()<1){lengkapi("nama_pengelola");}
				else if(ltelepon.length()<1){lengkapi("telepon");}
				else if(lalamat.length()<1){lengkapi("alamat");}
				else if(lkode_pengelola.length()<1){lengkapi("Pengelola");}
				else if(lusername.length()<1){lengkapi("username");}
				else if(lpassword.length()<1){lengkapi("password");}
				else if(lketerangan.length()<1){lengkapi("keterangan");}
				else if(lstatus.length()<1){lengkapi("status");}
			//	else{
//					if(kode_pengelola0.length()>0){
						new update().execute();
//					}
//					else{
//						new save().execute();
//					}
				//}//else

			}});

		btnHapus.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {

				finish();
				//new del().execute();
			}});
	}

	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	class get extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Profil.this);
			pDialog.setMessage("Load data detail. Silahkan tunggu...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}
		protected String doInBackground(String... params) {
			int sukses;
			try {
				List<NameValuePair> params1 = new ArrayList<NameValuePair>();
				params1.add(new BasicNameValuePair("kode_pengelola", kode_pengelola0));

				String url=ip+"pengelola/pengelola_detail.php";
				Log.v("detail",url);
				JSONObject json = jsonParser.makeHttpRequest(url, "GET", params1);
				Log.d("detail", json.toString());
				sukses = json.getInt(TAG_SUKSES);
				if (sukses == 1) {
					JSONArray myObj = json.getJSONArray(TAG_record); // JSON Array
					final JSONObject myJSON = myObj.getJSONObject(0);
					runOnUiThread(new Runnable() {
						public void run() {
							try {
								txtKode_pengelola.setText(kode_pengelola0);
								txtNama_pengelola.setText(myJSON.getString(TAG_nama_pengelola));
								txtAlamat.setText(myJSON.getString(TAG_alamat));
								txtTelepon.setText(myJSON.getString(TAG_telepon));
								txtKode_pengelola.setText(myJSON.getString(TAG_kode_pengelola));
								txtUsername.setText(myJSON.getString(TAG_username));
								txtPassword.setText(myJSON.getString(TAG_password));
								txtKeterangan.setText(myJSON.getString(TAG_keterangan));
								txtStatus.setText(myJSON.getString(TAG_status));
							}
							catch (JSONException e) {e.printStackTrace();}
						}});
				}
				else{
					// jika id tidak ditemukan
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return null;
		}
		protected void onPostExecute(String file_url) {pDialog.dismiss();}
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	class save extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Profil.this);
			pDialog.setMessage("Menyimpan data ...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}
		protected String doInBackground(String... args) {
			kode_pengelola = txtKode_pengelola.getText().toString();
			String lnama_pengelola= txtNama_pengelola.getText().toString();
			String ltelepon= txtTelepon.getText().toString();
			String lalamat= txtAlamat.getText().toString();
			String lkode_pengelola= txtKode_pengelola.getText().toString();
			String lusername= txtUsername.getText().toString();
			String lpassword= txtPassword.getText().toString();
			String lketerangan= txtKeterangan.getText().toString();
			String lstatus= txtStatus.getText().toString();


			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("kode_pengelola0", kode_pengelola0));
			params.add(new BasicNameValuePair("kode_pengelola", kode_pengelola));
			params.add(new BasicNameValuePair("nama_pengelola", lnama_pengelola));
			params.add(new BasicNameValuePair("telepon", ltelepon));
			params.add(new BasicNameValuePair("alamat", lalamat));
			params.add(new BasicNameValuePair("kode_pengelola", lkode_pengelola));
			params.add(new BasicNameValuePair("username", lusername));

			params.add(new BasicNameValuePair("password", lpassword));
			params.add(new BasicNameValuePair("keterangan", lketerangan));
			params.add(new BasicNameValuePair("status", lstatus));

			String url=ip+"pengelola/pengelola_add.php";
			Log.v("add",url);
			JSONObject json = jsonParser.makeHttpRequest(url,"POST", params);
			Log.d("add", json.toString());
			try {
				int sukses = json.getInt(TAG_SUKSES);
				if (sukses == 1) {
					Intent i = getIntent();
					setResult(100, i);
					finish();
				} else {
					// gagal update data
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return null;
		}

		protected void onPostExecute(String file_url) {pDialog.dismiss();}
	}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	class update extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Profil.this);
			pDialog.setMessage("Mengubah data ...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}
		protected String doInBackground(String... args) {
			kode_pengelola = txtKode_pengelola.getText().toString();
			String lnama_pengelola= txtNama_pengelola.getText().toString();
			String ltelepon= txtTelepon.getText().toString();
			String lalamat= txtAlamat.getText().toString();
			String lkode_pengelola= txtKode_pengelola.getText().toString();
			String lusername= txtUsername.getText().toString();
			String lpassword= txtPassword.getText().toString();
			String lketerangan= txtKeterangan.getText().toString();
			String lstatus= txtStatus.getText().toString();

			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("kode_pengelola0", kode_pengelola0));
			params.add(new BasicNameValuePair("kode_pengelola", kode_pengelola));
			params.add(new BasicNameValuePair("nama_pengelola", lnama_pengelola));
			params.add(new BasicNameValuePair("telepon", ltelepon));
			params.add(new BasicNameValuePair("alamat", lalamat));
			params.add(new BasicNameValuePair("kode_pengelola", lkode_pengelola));
			params.add(new BasicNameValuePair("username", lusername));
			params.add(new BasicNameValuePair("password", lpassword));
			params.add(new BasicNameValuePair("keterangan", lketerangan));
			params.add(new BasicNameValuePair("status", lstatus));

			String url=ip+"android/update_profil.php.php";
			Log.v("update",url);
			JSONObject json = jsonParser.makeHttpRequest(url,"POST", params);
			Log.d("add", json.toString());
			try {
				int sukses = json.getInt(TAG_SUKSES);
				if (sukses == 1) {
					Intent i = getIntent();
					setResult(100, i);
					finish();
				} else {
					// gagal update data
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return null;
		}

		protected void onPostExecute(String file_url) {pDialog.dismiss();}
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	class del extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Profil.this);
			pDialog.setMessage("Menghapus data...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		protected String doInBackground(String... args) {
			int sukses;
			try {
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair("kode_pengelola", kode_pengelola0));

				String url=ip+"pengelola/pengelola_del.php";
				Log.v("delete",url);
				JSONObject json = jsonParser.makeHttpRequest(url, "GET", params);
				Log.d("delete", json.toString());
				sukses = json.getInt(TAG_SUKSES);
				if (sukses == 1) {
					Intent i = getIntent();
					setResult(100, i);
					finish();
				}
			}
			catch (JSONException e) {e.printStackTrace();}
			return null;
		}

		protected void onPostExecute(String file_url) {pDialog.dismiss();}
	}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	public void lengkapi(String item){
		new AlertDialog.Builder(this)
				.setTitle("Lengkapi Data")
				.setMessage("Silakan lengkapi data "+item +" !")
				.setNeutralButton("Tutup", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dlg, int sumthin) {
						finish();
					}}).show();
	}

//
//	void callMarquee(){
//		Calendar cal = Calendar.getInstance();
//		int jam = cal.get(Calendar.HOUR);
//		int menit= cal.get(Calendar.MINUTE);
//		int detik= cal.get(Calendar.SECOND);
//
//		int tgl= cal.get(Calendar.DATE);
//		int bln= cal.get(Calendar.MONTH);
//		int thn= cal.get(Calendar.YEAR);
//
//		String stgl=String.valueOf(tgl)+"-"+String.valueOf(bln)+"-"+String.valueOf(thn);
//		String sjam=String.valueOf(jam)+":"+String.valueOf(menit)+":"+String.valueOf(detik);
//
//		TextView  txtMarquee=(TextView)findViewById(R.id.txtMarquee);
//		txtMarquee.setSelected(true);
//		String kata="Selamat Datang sandysipayung Aplikasi Android  "+stgl+"/"+sjam+" #";
//		String kalimat=String.format("%1$s",TextUtils.htmlEncode(kata));
//		txtMarquee.setText(Html.fromHtml(kalimat+kalimat+kalimat));
//	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

}
