package com.penginapan.penginapan;

import android.content.Context;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class Menu_pengusaha extends AppCompatActivity {
	Button btnIklan, btnNew, btnProfil, btnAbout;
	String xmlbio="",$kode_pengelola="";
	
	String myLati="-6.196577";
	String myLongi="106.849161";
	String myPosisi="Lp2m Aray";
	String kode_pengelola="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_pengusaha);
        
        Intent io = this.getIntent();
        xmlbio=io.getStringExtra("xmlbio");
		Log.v("axmlbio",xmlbio);
		Toast.makeText(Menu_pengusaha.this,xmlbio+ " telah dipilih....", Toast.LENGTH_LONG).show();

        String[]ar=xmlbio.split("#");
        if(ar.length<7){finish();}
		//$kode_pengelola#$nama_pengelola#$alamat#$telepon#$username#$password#$keterangan#$status
		 kode_pengelola=ar[1];

        btnIklan=(Button) findViewById(R.id.btnIklan);
        btnIklan.setOnClickListener(new View.OnClickListener() {
        public void onClick(View arg0) {
          		Intent i = new Intent(Menu_pengusaha.this,Iklan_List.class);
			i.putExtra("pk", kode_pengelola);
     	          startActivity(i);
    }});


        btnNew=(Button) findViewById(R.id.btnNew);
        btnNew.setOnClickListener(new View.OnClickListener() {
        public void onClick(View arg0) {
          		Intent i = new Intent(Menu_pengusaha.this,Buat_iklan.class);
			i.putExtra("pk", "");
     	          startActivity(i);
    }});



        btnProfil=(Button)findViewById(R.id.btnProfil);
        btnProfil.setOnClickListener(new View.OnClickListener() {
        public void onClick(View arg0) {
        	//finish();
        	Intent i = new Intent(Menu_pengusaha.this,Profil.class);
          		i.putExtra("xmlbio", xmlbio);

			Toast.makeText(Menu_pengusaha.this,xmlbio+ " telah dipilih....", Toast.LENGTH_LONG).show();

		//	Log.v("sxmlbio",xmlbio);
     	          startActivity(i);
    }});



		LocationManager locationManager;
		String context = Context.LOCATION_SERVICE;
		locationManager = (LocationManager)getSystemService(context);

		Criteria criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		criteria.setAltitudeRequired(false);
		criteria.setBearingRequired(false);
		criteria.setCostAllowed(true);
		criteria.setPowerRequirement(Criteria.POWER_LOW);
		String provider = locationManager.getBestProvider(criteria, true);

		Location location = locationManager.getLastKnownLocation(provider);
		updateWithNewLocation(location);

		locationManager.requestLocationUpdates(provider, 2000, 10, locationListener);
	}

	private final LocationListener locationListener = new LocationListener() {
		public void onLocationChanged(Location location) {
			updateWithNewLocation(location);
		}

		public void onProviderDisabled(String provider){
			updateWithNewLocation(null);
		}

		public void onProviderEnabled(String provider){ }
		public void onStatusChanged(String provider, int status,
									Bundle extras){ }
	};

	private void updateWithNewLocation(Location location) {
		double latitude=Double.parseDouble(myLati);
		double longitude=Double.parseDouble(myLongi);
		String addressString = "No address found";

		if (location != null) {
			latitude = location.getLatitude();
			longitude = location.getLongitude();
			Geocoder gc = new Geocoder(this, Locale.getDefault());
			try {
				List<Address> addresses = gc.getFromLocation(latitude, longitude, 1);
				StringBuilder sb = new StringBuilder();
				if (addresses.size() > 0) {
					Address address = addresses.get(0);

					for (int i = 0; i < address.getMaxAddressLineIndex(); i++)
						sb.append(address.getAddressLine(i)).append("\n");

					sb.append(address.getLocality()).append("\n");
					sb.append(address.getPostalCode()).append("\n");
					sb.append(address.getCountryName());
				}
				addressString = sb.toString();
			} catch (IOException e) {}
		} else {
			myLati="-6.353370";
			myLongi="106.832349";
			addressString="Lp2m Aray Jkt";
		}

		myPosisi=addressString;
		myLati=String.valueOf(latitude);
		myLongi=String.valueOf(longitude);


		TextView txtMarquee=(TextView)findViewById(R.id.txtMarquee);
		txtMarquee.setSelected(true);
		String kata="Posisi Anda :"+myLati+"/"+myLongi+" "+myPosisi+"#";
		String kalimat=String.format("%1$s", TextUtils.htmlEncode(kata));
		txtMarquee.setText(Html.fromHtml(kalimat+kalimat+kalimat));
	}

	void callMarquees(){
		Calendar cal = Calendar.getInstance();
		int jam = cal.get(Calendar.HOUR);
		int menit= cal.get(Calendar.MINUTE);
		int detik= cal.get(Calendar.SECOND);

		int tgl= cal.get(Calendar.DATE);
		int bln= cal.get(Calendar.MONTH);
		int thn= cal.get(Calendar.YEAR);

		String stgl=String.valueOf(tgl)+"-"+String.valueOf(bln)+"-"+String.valueOf(thn);
		String sjam=String.valueOf(jam)+":"+String.valueOf(menit)+":"+String.valueOf(detik);

		TextView  txtMarquee=(TextView)findViewById(R.id.txtMarquee);
		txtMarquee.setSelected(true);
		String kata="Selamat Datang di Aplikasi Android ..Selamat Mengerjakan /"+stgl+"/"+sjam+" #";
		String kalimat=String.format("%1$s",TextUtils.htmlEncode(kata));
		txtMarquee.setText(Html.fromHtml(kalimat+kalimat+kalimat));

	}


	public void keluar(){
    	new AlertDialog.Builder(this)
		.setTitle("Menutup Aplikasi")
		.setMessage("Terimakasih... Anda Telah Menggunakan Aplikasi Ini")
		.setNeutralButton("Tutup", new OnClickListener() {
			public void onClick(DialogInterface dlg, int sumthin) {
			finish();
			}})
		.show();
    }
    public void keluarYN(){
    	AlertDialog.Builder ad=new AlertDialog.Builder(Menu_pengusaha.this);
            	ad.setTitle("Konfirmasi");
            	ad.setMessage("Apakah benar ingin keluar?");
            	
            	ad.setPositiveButton("OK",new OnClickListener(){
       			@Override
    			public void onClick(DialogInterface dialog, int which) {
    					keluar();
    				}});
            	
            	ad.setNegativeButton("No",new OnClickListener(){
        			public void onClick(DialogInterface arg0, int arg1) {
        			}});
            	
            	ad.show();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
        	keluarYN();
                return true;
        }
    return super.onKeyDown(keyCode, event);
} 
    
}
