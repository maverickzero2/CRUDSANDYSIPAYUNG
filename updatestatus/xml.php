<?php
header("Content-type: text/xml");

include "../konmysqli.php";
$sql = "select * from `$tbupdatestatus`";
if(getJum($conn,$sql)>0){
	print "<to>\n";
		$arr=getData($conn,$sql);
		foreach($arr as $d) {
				$id_updatestatus=$d["id_updatestatus"];
				$id_workorder=$d["id_workorder"];
				$id_user=$d["id_user"];
				$uraian=$d["uraian"];
				$jam_updatestatus=$d["jam_updatestatus"];
				$tanggal_updatestatus=$d["tanggal_updatestatus"];

				print "<record>\n";
				print "  <id_workorder>$id_workorder</id_workorder>\n";
				print "  <id_user>$id_user</id_user>\n";
				print "  <uraian>$uraian</uraian>\n";
				print "  <jam_updatestatus>$jam_updatestatus</jam_updatestatus>\n";
				print "  <tanggal_updatestatus>$tanggal_updatestatus</tanggal_updatestatus>\n";
				print "  <id_updatestatus>$id_updatestatus</id_updatestatus>\n";
				print "</record>\n";
			}
	print "</to>\n";
}
else{
	$null="null";
	print "<to>\n";
		print "<record>\n";
				print "  <id_workorder>$null</id_workorder>\n";
				print "  <id_user>$null</id_user>\n";
				print "  <uraian>$null</uraian>\n";
				print "  <jam_updatestatus>$jam_updatestatus</jam_updatestatus>\n";
				print "  <tanggal_updatestatus>$tanggal_updatestatus</tanggal_updatestatus>\n";
				print "  <id_updatestatus>$null</id_updatestatus>\n";
		print "</record>\n";
	print "</to>\n";
	}
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

function getJum($conn,$sql){
  $rs=$conn->query($sql);
  $jum= $rs->num_rows;
	$rs->free();
	return $jum;
}

function getData($conn,$sql){
	$rs=$conn->query($sql);
	$rs->data_seek(0);
	$arr = $rs->fetch_all(MYSQLI_ASSOC);

	$rs->free();
	return $arr;
}
?>
