<?php
header("Content-type: text/xml");

include "../konmysqli.php";
$sql = "select * from `$tbworkorder`";
if(getJum($conn,$sql)>0){
	print "<to>\n";
		$arr=getData($conn,$sql);
		foreach($arr as $d) {
				$id_workorder=$d["id_workorder"];
				$id_targetoperasi=$d["id_targetoperasi"];
                $id_vendor=$d["id_vendor"];
				$id_pic=$d["id_pic"];
				$id_pelaksana=$d["id_pelaksana"];
				$jam_workorder=$d["jam_workorder"];
				$tanggal_workorder=$d["tanggal_workorder"];
			    $keterangan=$d["keterangan"];
				$status=$d["status"];

				print "<record>\n";
				print "  <id_targetoperasi>$id_targetoperasi</id_targetoperasi>\n";
				print "  <id_vendor>$id_vendor</id_vendor>\n";
                print "  <id_pic>$id_pic</id_pic>\n";
				print "  <id_pelaksana>$id_pelaksana</id_pelaksana>\n";
				print "  <jam_workorder>$jam_workorder</jam_workorder>\n";
				print "  <tanggal_workorder>$tanggal_workorder</tanggal_workorder>\n";
				print "  <keterangan>$keterangan</keterangan>\n";
				print "  <status>$status</status>\n";
				print "  <id_workorder>$id_workorder</id_workorder>\n";
				print "</record>\n";
			}
	print "</to>\n";
}
else{
	$null="null";
	print "<to>\n";
		print "<record>\n";
				print "  <id_targetoperasi>$null</id_targetoperasi>\n";
                print "  <id_vendor>$null</id_vendor>\n";
				print "  <id_pic>$null</id_pic>\n";
				print "  <id_pelaksana>$null</id_pelaksana>\n";
				print "  <jam_workorder>$jam_workorder</jam_workorder>\n";
				print "  <tanggal_workorder>$tanggal_workorder</tanggal_workorder>\n";
				print "  <keterangan>$null</keterangan>\n";
				print "  <status>$null</status>\n";
				print "  <id_workorder>$null</id_workorder>\n";
		print "</record>\n";
	print "</to>\n";
	}
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

function getJum($conn,$sql){
  $rs=$conn->query($sql);
  $jum= $rs->num_rows;
	$rs->free();
	return $jum;
}

function getData($conn,$sql){
	$rs=$conn->query($sql);
	$rs->data_seek(0);
	$arr = $rs->fetch_all(MYSQLI_ASSOC);

	$rs->free();
	return $arr;
}
?>
