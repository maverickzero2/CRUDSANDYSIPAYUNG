<?php
header("Content-type: text/xml");

include "../konmysqli.php";
$sql = "select * from `$tbmastervendor`";
if(getJum($conn,$sql)>0){
	print "<to>\n";
		$arr=getData($conn,$sql);
		foreach($arr as $d) {
				$id_vendor=$d["id_vendor"];
				$nama_vendor=$d["nama_vendor"];
				$alamat_vendor=$d["alamat_vendor"];
				$telepon_vendor=$d["telepon_vendor"];
				$email_vendor=$d["email_vendor"];
				$pic_vendor=$d["pic_vendor"];

				print "<record>\n";
				print "  <nama_vendor>$nama_vendor</nama_vendor>\n";
				print "  <alamat_vendor>$alamat_vendor</alamat_vendor>\n";
				print "  <telepon_vendor>$telepon_vendor</telepon_vendor>\n";
				print "  <email_vendor>$email_vendor</email_vendor>\n";
				print "  <pic_vendor>$pic_vendor</pic_vendor>\n";
				print "  <id_vendor>$id_vendor</id_vendor>\n";
				print "</record>\n";
			}
	print "</to>\n";
}
else{
	$null="null";
	print "<to>\n";
		print "<record>\n";
				print "  <nama_vendor>$null</nama_vendor>\n";
				print "  <alamat_vendor>$null</alamat_vendor>\n";
				print "  <telepon_vendor>$null</telepon_vendor>\n";
				print "  <email_vendor>$null</email_vendor>\n";
				print "  <pic_vendor>$null</pic_vendor>\n";
				print "  <id_vendor>$null</id_vendor>\n";
		print "</record>\n";
	print "</to>\n";
	}
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

function getJum($conn,$sql){
  $rs=$conn->query($sql);
  $jum= $rs->num_rows;
	$rs->free();
	return $jum;
}

function getData($conn,$sql){
	$rs=$conn->query($sql);
	$rs->data_seek(0);
	$arr = $rs->fetch_all(MYSQLI_ASSOC);

	$rs->free();
	return $arr;
}
?>
