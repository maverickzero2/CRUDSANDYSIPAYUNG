<?php
$pro = "simpan";
$tanggal = WKT(date("Y-m-d"));

if(!isset($_SESSION["cid"])){
  die("<script>location.href='index.php'</script>");
}
?>
<link type="text/css" href="<?php echo "$PATH/base/"; ?>ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo "$PATH/"; ?>jquery-1.3.2.js"></script>
<script type="text/javascript" src="<?php echo "$PATH/"; ?>ui/ui.core.js"></script>
<script type="text/javascript" src="<?php echo "$PATH/"; ?>ui/ui.datepicker.js"></script>
<script type="text/javascript" src="<?php echo "$PATH/"; ?>ui/i18n/ui.datepicker-id.js"></script>

<script type="text/javascript">
$(document).ready(function () {
  $("#tanggal").datepicker({
    dateFormat: "dd MM yy",
    changeMonth: true,
    changeYear: true
  });
});
</script>

<script type="text/javascript">
function PRINT() {
  win = window.open('mastervendor/print.php', 'win', 'width=1000, height=400, menubar=0, scrollbars=1, resizable=0, location=0, toolbar=0, pic_vendor=0');
}
</script>
<script language="JavaScript">
function buka(url) {
  window.open(url, 'window_baru', 'width=800,height=600,left=320,top=100,resizable=1,scrollbars=1');
}
</script>

<?php
$sql = "select `id_vendor` from `$tbmastervendor` order by `id_vendor` desc";
$q = mysqli_query($conn, $sql);
$jum = mysqli_num_rows($q);
$th = date("y");
$bl = date("m") + 0;
if ($bl < 10) {
  $bl = "0" . $bl;
}

$kd = "VEN" . $th . $bl; //KEG1610001
if ($jum > 0) {
  $d = mysqli_fetch_array($q);
  $idmax = $d["id_vendor"];

  $bul = substr($idmax, 5, 2);
  $tah = substr($idmax, 3, 2);
  if ($bul == $bl && $tah == $th) {
    $urut = substr($idmax, 7, 3) + 1;
    if ($urut < 10) {
      $idmax = "$kd" . "00" . $urut;
    } else if ($urut < 100) {
      $idmax = "$kd" . "0" . $urut;
    } else {
      $idmax = "$kd" . $urut;
    }
  }//==
  else {
    $idmax = "$kd" . "001";
  }
}//jum>0
else {
  $idmax = "$kd" . "001";
}
$id_vendor = $idmax;
?>

<?php
if ($_GET["pro"] == "ubah") {
  $id_vendor = $_GET["kode"];
  $sql = "select * from `$tbmastervendor` where `id_vendor`='$id_vendor'";
  $d = getField($conn, $sql);
  $id_vendor = $d["id_vendor"];
  $id_vendor0 = $d["id_vendor"];
  $nama_vendor = $d["nama_vendor"];
  $alamat_vendor = $d["alamat_vendor"];
  $telepon_vendor = $d["telepon_vendor"];
  $email_vendor = $d["email_vendor"];
  $pic_vendor = $d["pic_vendor"];
  $pro = "ubah";
}
?>

<div class="panel-group" id="accordion">
  <div class="panel box box-success panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
          Input Vendor</a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse in">
        <div class="panel-body">

          <form action="" method="post" enctype="multipart/form-data">
            <div class="box-body row">
              <div class="col-sm-3">
              </div>
              <div class="form-group col-sm-6" >
                <div class="form-group">
                  <label for="id_vendor">ID Vendor</label>
                  <input disabled="disabled" class="form-control" value="<?php echo $id_vendor; ?>"/>
                </div>
                <div class="form-group">
                  <label for="nama_vendor">Nama Vendor</label>
                  <input class="form-control" name="nama_vendor" type="text" id="nama_vendor" required="required" value="<?php echo $nama_vendor; ?>" size="30" />
                </div>
                <div class="form-group">
                  <label for="alamat_vendor">Alamat Vendor</label>
                  <textarea class="form-control" name="alamat_vendor" type="text" id="alamat_vendor" size="25"><?php echo $alamat_vendor; ?></textarea>
                </div>
                <div class="form-group">
                  <label for="telepon_vendor">Telepon Vendor</label>
                  <input class="form-control" name="telepon_vendor" type="number" id="telepon_vendor" required="required" value="<?php echo $telepon_vendor; ?>" size="30" />
                </div>
                <div class="form-group">
                  <label for="email_vendor">Email Vendor</label>
                  <input class="form-control" name="email_vendor" type="email" id="email_vendor" value="<?php echo $email_vendor; ?>" size="25" />
                </div>
                <div class=form-group>
                  <label for="pic_vendor">PIC Vendor</label>
                  <select class="form-control" name="pic_vendor" id="pic_vendor">
                    <?php
                    $sql = "select id_user,nama_user from `$tbmasterpetugas` where `level_user`='PIC'";
                    $arr = getData($conn, $sql);
                    foreach ($arr as $d) {
                      $id_user0 = $d["id_user"];
                      $nama_user = $d["nama_user"];
                      echo"<option value='$id_user0' ";
                      if ($id_user0 == $id_user) {
                        echo"selected";
                      }echo">$nama_user</option>";
                    }
                    ?>
                  </select>
                </div><br/>
                <div class="form-group" align="right">
                  <button type="submit" name="Simpan"id="Simpan" class="btn btn-primary">Simpan</button>
                  <input name="pro" type="hidden" id="pro" value="<?php echo $pro; ?>" />
                  <input name="id_vendor" type="hidden" id="id_vendor" value="<?php echo $id_vendor; ?>" />
                  <input name="id_vendor0" type="hidden" id="id_vendor0" value="<?php echo $id_vendor0; ?>" />
                  <a href="?mnu=mastervendor"><button type="button" name="Batal" id="Batal" class="btn btn-danger">Batal</button></a>
                </div>
              </div>
              <div class="col-sm-3">
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>



      <div class="panel box box-primary panel-default">
        <div class="panel-heading">
          <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordions" href="#collapse2<?php  $id_vendor; ?>">
              Data Vendor <?php echo $nama_vendor; ?></a>
            </h4>
          </div>
          <div id="collapse2<?php  $id_vendor; ?>" class="panel-collapse collapse">
            <div class="panel-body">
              Data mastervendor:
              | <a href="mastervendor/pdf.php"><img src='ypathicon/pdf.png' alt='PDF'></a>
              | <a href="mastervendor/xls.php"><img src='ypathicon/xls.png' alt='XLS'></a>
              | <a href="mastervendor/xml.php"><img src='ypathicon/xml.png' alt='XML'></a>
              | <img src='ypathicon/print.png' alt='PRINT' OnClick="PRINT()"> |
              <br>

              <div class="row">
                <div class="table-responsive">
                  <table id="tabelvendor" class="table table-bordered table-striped dataTable">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>ID Vendor</th>
                        <th>Nama Vendor</th>
                        <th>Alamat Vendor</th>
                        <th>Telepon Vendor</th>
                        <th>Email Vendor</th>
                        <th>PIC</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <?php
                    $sql = "select * from `$tbmastervendor` order by `id_vendor` desc";
                    $jum = getJum($conn, $sql);
                    if ($jum > 0) {
                      //--------------------------------------------------------------------------------------------
                      $batas = 10;
                      $page = $_GET['page'];
                      if (empty($page)) {
                        $posawal = 0;
                        $page = 1;
                      } else {
                        $posawal = ($page - 1) * $batas;
                      }

                      $sql2 = $sql . " LIMIT $posawal,$batas";
                      $no = $posawal + 1;
                      //--------------------------------------------------------------------------------------------
                      $arr = getData($conn, $sql2);
                      foreach ($arr as $d) {
                        $id_vendor = $d["id_vendor"];
                        $nama_vendor =  $d["nama_vendor"];
                        $alamat_vendor =  $d["alamat_vendor"];
                        $telepon_vendor = $d["telepon_vendor"];
                        $email_vendor = $d["email_vendor"];
                        $pic_vendor = getUser($conn, $d["pic_vendor"]);
                        $color = "#dddddd";
                        if ($no % 2 == 0) {
                          $color = "#eeeeee";
                        }
                        echo"<tr bgcolor='$color'>
                        <td>$no</td>
                        <td>$id_vendor</td>
                        <td>$nama_vendor</td>
                        <td>$alamat_vendor</td>
                        <td>$telepon_vendor</td>
                        <td>$email_vendor</td>
                        <td align='center'>$pic_vendor</td>
                        <td align='center'>
                        <a href='?mnu=mastervendor&pro=ubah&kode=$id_vendor'><img src='ypathicon/u.png' alt='ubah'></a>
                        <a href='?mnu=mastervendor&pro=hapus&kode=$id_vendor'><img src='ypathicon/h.png' alt='hapus'
                        onClick='return confirm(\"Apakah Anda benar-benar akan menghapus $nama_vendor pada data mastervendor ?..\")'></a></td>
                        </tr>";

                        $no++;
                      }//while
                    }//if
                    else {
                      echo"<tr><td colspan='7'><blink>Maaf, Data mastervendor belum tersedia...</blink></td></tr>";
                    }
                    ?>
                  </table>
                </div>
              </div>
              <?php
              //Langkah 3: Hitung total data dan page
              $jmldata = $jum;
              if ($jmldata > 0) {
                if ($batas < 1) {
                  $batas = 1;
                }
                $jmlhal = ceil($jmldata / $batas);
                echo "<div class=paging>";
                if ($page > 1) {
                  $prev = $page - 1;
                  echo "<span class=prevnext><a href='$_SERVER[PHP_SELF]?page=$prev&mnu=vendor'>« Prev</a></span> ";
                } else {
                  echo "<span class=disabled>« Prev</span> ";
                }

                // Tampilkan link page 1,2,3 ...
                for ($i = 1; $i <= $jmlhal; $i++)
                if ($i != $page) {
                  echo "<a href='$_SERVER[PHP_SELF]?page=$i&mnu=vendor'>$i</a> ";
                } else {
                  echo " <span class=current>$i</span> ";
                }

                // Link kepage berikutnya (Next)
                if ($page < $jmlhal) {
                  $next = $page + 1;
                  echo "<span class=prevnext><a href='$_SERVER[PHP_SELF]?page=$next&mnu=vendor'>Next »</a></span>";
                } else {
                  echo "<span class=disabled>Next »</span>";
                }
                echo "</div>";
              }//if jmldata

              $jmldata = $jum;
              echo "<p align=center>Total Data <b>$jmldata</b> Item</p>";
              ?>
            </div>
          </div>
        </div>

      </div>
      <?php
      if (isset($_POST["Simpan"])) {
        $pro = strip_tags($_POST["pro"]);
        $id_vendor = strip_tags($_POST["id_vendor"]);
        $id_vendor0 = strip_tags($_POST["id_vendor0"]);
        $nama_vendor = strip_tags($_POST["nama_vendor"]);
        $alamat_vendor = strip_tags($_POST["alamat_vendor"]);
        $telepon_vendor = strip_tags($_POST["telepon_vendor"]);
        $email_vendor = strip_tags($_POST["email_vendor"]);
        $pic_vendor = strip_tags($_POST["pic_vendor"]);

        if ($pro == "simpan") {
          $sql = " INSERT INTO `$tbmastervendor` (
            `id_vendor` ,
            `nama_vendor` ,
            `alamat_vendor` ,
            `telepon_vendor` ,
            `email_vendor` ,
            `pic_vendor`
          ) VALUES (
            '$id_vendor',
            '$nama_vendor',
            '$alamat_vendor',
            '$telepon_vendor',
            '$email_vendor',
            '$pic_vendor'
          )";

          $simpan = process($conn, $sql);
          if ($simpan) {
            echo "<script>alert('Data $id_vendor berhasil disimpan !');document.location.href='?mnu=mastervendor';</script>";
          } else {
            echo"<script>alert('Data $id_vendor gagal disimpan...');document.location.href='?mnu=mastervendor';</script>";
          }
        } else {
          $sql = "update `$tbmastervendor` set
          `nama_vendor`='$nama_vendor',
          `alamat_vendor`='$alamat_vendor' ,
          `telepon_vendor`='$telepon_vendor',
          `pic_vendor`='$pic_vendor',
          `email_vendor`='$email_vendor'
          where `id_vendor`='$id_vendor0'";
          $ubah = process($conn, $sql);
          if ($ubah) {
            echo "<script>alert('Data $id_vendor berhasil diubah !');document.location.href='?mnu=mastervendor';</script>";
          } else {
            echo"<script>alert('Data $id_vendor gagal diubah...');document.location.href='?mnu=mastervendor';</script>";
          }
        }//else simpan
      }
      ?>
      <?php
      if ($_GET["pro"] == "hapus") {
        $id_vendor = $_GET["kode"];
        $sql = "delete from `$tbmastervendor` where `id_vendor`='$id_vendor'";
        $hapus = process($conn, $sql);
        if ($hapus) {
          echo "<script>alert('Data vendor $id_vendor berhasil dihapus !');document.location.href='?mnu=mastervendor';</script>";
        } else {
          echo"<script>alert('Data vendor $id_vendor gagal dihapus...');document.location.href='?mnu=mastervendor';</script>";
        }
      }
      ?>
